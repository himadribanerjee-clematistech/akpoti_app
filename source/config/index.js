import { StyleSheet, Dimensions } from 'react-native';
import * as theme from './theme';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        flexDirection: 'column',
        fontFamily:"'Wallpoet', cursive"
    },
    signinHeader: {
        width: Width, 
        height: ( Height * 12/100 ), 
        backgroundColor: '#FFF', 
        borderColor: '#ddd', 
        shadowColor: '#000', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.8, 
        shadowRadius: 3, 
        elevation: 5,
    },
    signinHeaderContainer: {
        flex: 1, 
        flexDirection: 'row'
    },
    signinHeaderContainerLeft: {
        flex: 0.20, 
        justifyContent: 'center', 
        alignItems: 'center',
        paddingTop: 40
    },
    signinHeaderContainerRight: {
        flex: 0.70
    },
   
    signinHeaderContainerRightText: {
        top: ( Height * 7/100 ), 
        fontSize: 20, 
        left: ( Width * 10/100 ), 
    },












    
    categoriesHeader: {
        width: Width, 
        height: ( Height * 10/100 ), 
        backgroundColor: '#FFF', 
        borderColor: '#ddd', 
        shadowColor: '#000', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.8, 
        shadowRadius: 3, 
        elevation: 5,
    },
    categoriesHeaderContainerLeft: {
        flex: 0.20, 
        justifyContent: 'center', 
        alignItems: 'center',
        paddingTop: 20
    },
    categoriesHeaderContainerMiddle: {
        flex: 0.70,
    },
   categoriesHeaderContainerMiddleText: {
       top: ( Height * 4.0/100 ), 
        fontSize: 20, 
        left: ( Width * 2/100 ), 
         borderWidth: 1,
          borderColor: 'white'
    },
   itemsHeaderContainerMiddleText: {
         top: ( Height * 2.0/100 ), 
        fontSize: 20, 
        left: ( Width * 2/100 ), 
         borderWidth: 1,
          borderColor: 'white'
    },
    productfilterHeaderContainerMiddleText: {
       top: ( Height * 4.0/100 ), 
        fontSize: 20, 
        left: ( Width * 1/100 ), 
         borderWidth: 1,
          borderColor: 'white'
    },
    categoriesHeaderContainerRight: {
        flex: 0.10,
        paddingTop: 35
    },
     productfilterHeaderContainerMiddle: {
        flex: 0.50,
    },
    productfilterHeaderContainerRight: {
        flex: 0.30,
       paddingTop: 35
    },
     itemSearchHeaderContainerMiddleText: {
        top: ( Height * 2.0/100 ), 
        fontSize: 20, 
        left: ( Width * 2/100 ), 
    },
   productListHeaderContainerMiddleText: {
       top: ( Height * 4.5/100 ), 
        fontSize: 20, 
        left: ( Width * 2/100 ), 
         borderWidth: 1,
          borderColor: 'white'
    },
    productListHeaderContainerMiddle: {
        flex: 0.70,
    },
     productListHeaderContainerRighttwo:{
         flex: 0.10,  
         top: ( Height * 4.5/100 ),   
    },
    productListHeaderContainerLeft: {
        flex: 0.10, 
        justifyContent: 'center', 
        alignItems: 'center',
        paddingTop: 20
    },
    productListHeaderContainerRight:{
         flex: 0.10,  
         top: ( Height * 4.5/100 ),   
    },
    paginationContainer: {
        backgroundColor:'red'
    },
     paginationDot: {
        width: 180,
        height: 180,
        borderRadius: 4,
        marginHorizontal: 8
    }
});

export { styles }