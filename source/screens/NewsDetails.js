import React from 'react';
import { SafeAreaView, View, Text, Dimensions, ActivityIndicator, Image, YellowBox } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { HeaderBackButton } from 'react-navigation-stack';
import { Footer, FooterTab, Button, Container, Content, Card, CardItem, Body, } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
const regex = /(<([^>]+)>)/ig;

export default class NewsDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      serverData: '',
      slider1ActiveSlide: 1,
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',

    }
    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {

    this.setState({
      serverData: this.props.navigation.getParam('item', null),
      loading: false,
    });

  }

  _renderProductImage({ item, index }) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', top: 0, borderRadius: 10, backgroundColor: '#eff0f1', shadowColor: '#000', height: 220, justifyContent: 'flex-start', alignItems: 'center' }} >
        <Image style={{ width: '100%', height: '100%', resizeMode: 'contain', opacity: 1 }} source={{ uri: serverURL + 'admin/uploads/news/' + item }} />
        <View>

        </View>
      </View>
    );
  }



  render() {
    //console.log(this.state.serverData); 
    const serverData = this.state.serverData;
    const { slider1ActiveSlide } = this.state;

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 0.10 }} >
              <View style={style.styles.categoriesHeader}>
                <View style={style.styles.signinHeaderContainer}>
                  <View style={style.styles.categoriesHeaderContainerLeft}>
                    <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                  </View>
                  <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>News Details</Text></View>
                  <View style={style.styles.productListHeaderContainerRight}></View>
                  <View style={style.styles.productListHeaderContainerRighttwo}>


                  </View>
                </View>
              </View>
            </View>
            {
              (this.state.serverError) ? (
                <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
              ) :
                (!this.state.connectState) ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                  </View>
                ) :
                  this.state.loading ? (
                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                  ) :

                    <View style={{ flex: 0.90 }} >
                      <Content>
                        <Card style={{ flex: 0 }}>

                          <CardItem>
                            <Body>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <Text style={{ fontWeight: 'bold', textAlign: 'left', fontSize: 20, color: '#148db3', paddingTop: 10 }}>{serverData.title}</Text>
                              </View>
                              <View style={{ flex: 1, flexDirection: 'row', paddingTop: 10 }}>
                                <View style={{ width: 30 }}><Icon name="ios-calendar" /></View>
                                <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', paddingTop: 5 }} >{serverData.date}</Text></View>
                              </View>
                            </Body>
                          </CardItem>
                          <CardItem>
                            <Body>

                              <View style={{ flex: 1 }}>
                                <Carousel
                                  data={serverData.image}
                                  loop={true}
                                  hasParallaxImages={true}
                                  renderItem={this._renderProductImage}
                                  sliderWidth={BannerWidth - 30}
                                  itemWidth={BannerWidth - 30}
                                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                />
                                <Pagination
                                  dotsLength={serverData.image.length}
                                  dotColor={'#000'}
                                  dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                  inactiveDotColor={'#fff'}
                                  containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                  activeDotIndex={slider1ActiveSlide}
                                  inactiveDotOpacity={0.9}
                                  inactiveDotScale={0.9}
                                  carouselRef={this._slider1Ref}
                                  tappableDots={!!this._slider1Ref}
                                />
                              </View>

                              <Text style={{ textAlign: 'left', fontSize: 15, color: '#666', paddingTop: 10 }}>
                                {
                                  serverData.description = serverData.description.replace(regex, '')
                                }
                                {serverData.description}
                              </Text>
                            </Body>
                          </CardItem>

                        </Card>
                      </Content>
                    </View>
            }


            <View style={{ flex: 0.08 }} >
              <Footer style={{ height: '100%' }} >
                <FooterTab style={{ backgroundColor: '#000' }}>
                  <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                    <Icon name="ios-home" style={{ color: 'grey' }} />
                    <Text style={{ color: 'grey' }}>Home</Text>
                  </Button>
                  <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                    <Icon name="ios-person" style={{ color: 'grey' }} />
                    <Text style={{ color: 'grey' }}>Myakpoti</Text>
                  </Button>
                  <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                    <Icon name="ios-list" style={{ color: 'grey' }} />
                    <Text style={{ color: 'grey' }}>Categories</Text>
                  </Button>
                  <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                    <Icon name="ios-cog" style={{ color: 'grey' }} />
                    <Text style={{ color: 'grey' }}>Services</Text>
                  </Button>
                  <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                    <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                    <Text style={{ color: 'grey' }}>Messenger</Text>
                  </Button>
                </FooterTab>
              </Footer>
            </View>
          </View>
        </Container>
      </SafeAreaView>
    );
  }

}
