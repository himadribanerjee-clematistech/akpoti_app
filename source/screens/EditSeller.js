import React from 'react';
import { YellowBox, View, Text, StatusBar, Dimensions, TouchableOpacity, TextInput, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


export default class EditSeller extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showNameText: false,
            nameText: null,
            showCityText: false,
            cityText: null,
            showStateText: false,
            stateText: null,
            showContactNameText: false,
            contactNameText: null,
            showContactNumberText: false,
            contactNumberText: null,
            showAddressText: false,
            addressText: null,

            sname: null,
            scity: null,
            sstate: null,
            scontactname: null,
            scontactnum: null,
            saddress: null,

            serverError: false,
            connectState: true,
            showLoginButton: false,
            errorMsg: 'Something went wrong. Please try later.',
        }

        this.CheckConnectivity();

    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };

    componentDidMount = () => {
        this._doAuthenticateMe().done();
    }

    _doAuthenticateMe = async () => {
        try {
            const savedToken = await AsyncStorage.getItem('@token');

            fetch(serverURL + 'api/login/doAuthenticateMe', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': savedToken,
                },
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);

                    if (responseJson[0] == 200) {
                        var data = responseJson[1].result;
                        var seller = data.seller_info;

                        this.setState({
                            sname: seller.business_name,
                            saddress: seller.address,
                            scity: seller.city,
                            sstate: seller.state,
                            scontactname: seller.contact_name,
                            scontactnum: seller.contact_number,
                        });
                    }
                    else {
                        this.logout();
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (e) {
            console.log(e);
        }
    }

    logout = async () => {
        const keys = ['@token', '@total_info']
        try {
            //alert('Pressed!');            
            await AsyncStorage.multiRemove(keys);
            this.props.navigation.replace('Home');
            global_isLoggedIn = false;
            global_LoggedInCustomerName = '';
            global_LoggedInCustomerId = '';
            global_LoggedInSellerId = '';
            global_is_seller = '';
            global_is_agent = '';
            global_profileImage = serverURL + 'assets/images/user.png';
        }
        catch (error) {
            console.error();
        }
    }

    _update = async () => {
        let savedToken = await AsyncStorage.getItem('@token');
        let customer_id = await AsyncStorage.getItem('@customer_id');

        let BName = this.state.sname;
        let BCity = this.state.scity;
        let BState = this.state.sstate;
        let BCName = this.state.scontactname;
        let BCNum = this.state.scontactnum;
        let BAddr = this.state.saddress;

        if (BName == null) {
            this.setState({
                showNameText: true,
                nameText: 'Bussiness Name Should Not Be Blank',
            })
        }
        else if (BCity == null) {
            this.setState({
                showNameText: false,
                showCityText: true,
                cityText: 'Bussiness City Should Not Be Blank'
            })
        }
        else if (BState == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: true,
                stateText: 'Bussiness State Should Not Be Blank'
            })
        }
        else if (BCName == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: true,
                contactNameText: 'Bussiness Contact Name Should Not Be Blank'
            })
        }
        else if (BCNum == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: false,
                showContactNumberText: true,
                contactNumberText: 'Bussiness Contact Number Should Not Be Blank'
            })
        }
        else if (BAddr == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: false,
                showContactNumberText: false,
                showAddressText: true,
                addressText: 'Bussiness Address Should Not Be Blank'
            })
        }
        else {
            //console.log(BName, BCity, BState, BCName, BCNum, BAddr, serverURL + 'api/sellerUpdate');

            fetch(serverURL + 'api/sellerUpdate', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': savedToken,
                    'credentials': 'same-origin'
                },
                body: JSON.stringify({
                    customer_id: customer_id,
                    business_name: BName,
                    address: BAddr,
                    state: BState,
                    city: BCity,
                    contact_name: BCName,
                    contact_number: BCNum,
                })
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);
                    if (responseJson[0] == 200) {
                        if (responseJson[1].msg !== '') {
                            Alert.alert('Success Message !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                        else {
                            Alert.alert('Success Message with Error !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                    }
                    else {
                        Alert.alert('Error Message !!', responseJson[1].msg);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }


    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#F7F9F9' }}>
                {
                    (!this.state.connectState) ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                        </View>
                    ) : null
                }
                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <View style={{ width: Width, height: (Height * 12 / 100), backgroundColor: '#FFF', borderColor: '#ddd', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 3, elevation: 5, }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <Icon name="ios-arrow-back" style={{ fontSize: 30, color: '#000', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.9, justifyContent: 'center', alignItems: 'center', paddingTop: 40, }}>
                            <Text style={{ fontSize: 20, }}> Edit Seller Profile</Text>
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, paddingTop: 15 }}>
                            <TextInput
                                style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                returnKeyType='done'
                                autoCorrect={false}
                                onChangeText={sname => this.setState({ sname })}
                                placeholder="Bussiness Name"
                                returnKeyType='next'
                                value={this.state.sname}
                            />
                        </View>
                        {
                            this.state.showNameText
                                ?
                                <View style={{ marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.nameText} </Text>
                                </View>
                                :
                                null
                        }

                        <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, paddingTop: 15 }}>
                            <TextInput
                                style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                returnKeyType='done'
                                autoCorrect={false}
                                onChangeText={scity => this.setState({ scity })}
                                placeholder="Bussiness City"
                                returnKeyType='next'
                                value={this.state.scity}
                            />
                        </View>
                        {
                            this.state.showCityText
                                ?
                                <View style={{ marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.cityText} </Text>
                                </View>
                                :
                                null
                        }

                        <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, paddingTop: 15 }}>
                            <TextInput
                                style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                returnKeyType='done'
                                autoCorrect={false}
                                onChangeText={sstate => this.setState({ sstate })}
                                placeholder="Bussiness State"
                                returnKeyType='next'
                                value={this.state.sstate}
                            />
                        </View>
                        {
                            this.state.showStateText
                                ?
                                <View style={{ marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.stateText} </Text>
                                </View>
                                :
                                null
                        }

                        <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, paddingTop: 15 }}>
                            <TextInput
                                style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                returnKeyType='done'
                                autoCorrect={false}
                                onChangeText={scontactname => this.setState({ scontactname })}
                                placeholder="Bussiness Contact Name"
                                returnKeyType='next'
                                value={this.state.scontactname}
                            />
                        </View>
                        {
                            this.state.showContactNameText
                                ?
                                <View style={{ marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.contactNameText} </Text>
                                </View>
                                :
                                null
                        }

                        <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, paddingTop: 15 }}>
                            <TextInput
                                style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                returnKeyType='done'
                                autoCorrect={false}
                                onChangeText={scontactnum => this.setState({ scontactnum })}
                                placeholder="Bussiness Contact Number"
                                returnKeyType='next'
                                value={this.state.scontactnum}
                                keyboardType='number-pad'
                            />
                        </View>
                        {
                            this.state.showContactNumberText
                                ?
                                <View style={{ marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.contactNumberText} </Text>
                                </View>
                                :
                                null
                        }

                        <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, paddingTop: 25 }}>
                            <TextInput
                                style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                returnKeyType='done'
                                autoCorrect={false}
                                onChangeText={saddress => this.setState({ saddress })}
                                placeholder="Bussiness Address"
                                returnKeyType='next'
                                value={this.state.saddress}
                                multiline
                            />
                        </View>

                        {
                            this.state.showAddressText
                                ?
                                <View style={{ marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.addressText} </Text>
                                </View>
                                :
                                null
                        }

                        <View style={{ width: (Width * 90 / 100), paddingTop: 20 }}>
                            <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', justifyContent: 'center', alignItems: 'center' }} onPress={() => this._update()}>
                                <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}> Update Profile </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View >

            </View >
        )
    }
}