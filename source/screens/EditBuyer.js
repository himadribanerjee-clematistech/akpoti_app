import React from 'react';
import { View, Text, StatusBar, Dimensions, TouchableOpacity, Alert, YellowBox, TextInput } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Content, Form, Item, Label, Input } from 'native-base';
import NetInfo from "@react-native-community/netinfo";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

YellowBox.ignoreWarnings(["Warning:"]);

export default class EditBuyer extends React.Component {
    constructor(props) {
        super(props);

        this._doAuthenticateMe().done();
        this.state = {
            name: null,
            showNameText: false,
            nameText: '',
            city: null,
            showCityText: false,
            cityText: '',
            state: null,
            showStateText: false,
            address: null,
            showAddrText: false,

            shwAddr: null,

            waitMsg: false,
            waitMsgTxt: null,

            lat: null,
            long: null,
            location_key: null,
            location_city: null,

            shwButt: false,

            serverError: false,
            connectState: true,
            showLoginButton: false,
            errorMsg: 'Something went wrong. Please try later.',
        }
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };

    _doAuthenticateMe = async () => {
        try {
            const savedToken = await AsyncStorage.getItem('@token');

            fetch(serverURL + 'api/login/doAuthenticateMe', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': savedToken,
                },
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);

                    if (responseJson[0] == 200) {
                        var data = responseJson[1].result;
                        var buyer = data.buyer_info;

                        AsyncStorage.setItem('@customer_id', data.id);

                        this.setState({
                            name: data.name,
                            address: buyer.address,
                            city: buyer.city,
                            state: buyer.state,
                            shwAddr: buyer.address
                        });
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (e) {
            console.log(e);
        }
    }

    _getLocation = async (lat, long) => {
        try {
            const savedToken = await AsyncStorage.getItem('@token');

            fetch(serverURL + 'api/weather/getlocationkey', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': savedToken,
                },
                body: JSON.stringify({
                    latitude: lat,
                    longitude: long,
                }),
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    if (responseJson[0] == 200) {
                        this.setState({
                            waitMsg: true,
                            waitMsgTxt: 'You will get weather update of ' + responseJson[1].locations.location_city,
                            location_key: responseJson[1].locations.location_key,
                            location_city: responseJson[1].locations.location_city,
                            shwButt: true,
                        });
                    }
                    else {
                        console.log(responseJson);
                        this.setState({ shwButt: false, });
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (e) {
            console.log(e);
        }
    }

    _updateBuyer = async () => {
        try {
            const savedToken = await AsyncStorage.getItem('@token');

            let Name = this.state.name;
            let Addr = this.state.address;
            let City = this.state.city;
            let State = this.state.state;

            if (Name == '') {
                this.setState({
                    showNameText: true,
                    nameText: 'Name is Required',
                });
            }
            else if (City == '') {
                this.setState({
                    showNameText: false,
                    showCityText: true,
                    cityText: 'City Field is Required',
                });
            }
            else if (State == '') {
                this.setState({
                    showNameText: false,
                    showCityText: false,
                    showStateText: true,
                    stateText: 'State Field is Required',
                });
            }
            else if (Addr == '') {
                this.setState({
                    showNameText: false,
                    showCityText: false,
                    showStateText: false,
                    waitMsg: true,
                    waitMsgTxt: 'Address is Required',
                });
            }
            else {
                this.setState({
                    showNameText: false,
                    showCityText: false,
                    showStateText: false,
                    waitMsg: false,
                });

                fetch(serverURL + 'api/buyerUpdate', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': savedToken,
                    },
                    body: JSON.stringify({
                        address: Addr,
                        state: State,
                        city: City,
                        lat: this.state.lat,
                        long: this.state.long,
                        location_city: this.state.location_city,
                        location_key: this.state.location_key,
                    }),
                })
                    .then(response => {
                        const status = response.status;
                        const data = response.json();

                        return Promise.all([status, data]);
                    })
                    .then((responseJson) => {
                        //console.log(responseJson);
                        if (responseJson[0] == 200) {
                            Alert.alert('Success Message !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                        else {
                            Alert.alert('Error Message !!', responseJson[1].msg);
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#F7F9F9' }}>
                {
                    (!this.state.connectState) ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                        </View>
                    ) : null
                }
                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <View style={{ width: Width, height: (Height * 12 / 100), backgroundColor: '#FFF', borderColor: '#ddd', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 3, elevation: 5, }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <Icon name="ios-arrow-back" style={{ fontSize: 30, color: '#000', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.5, justifyContent: 'flex-start', alignItems: 'flex-start', paddingTop: 50, }}>
                            <Text style={{ fontSize: 20, }}> Edit Profile</Text>
                        </View>
                        <View style={{ flex: 0.4, justifyContent: 'center', alignItems: 'center', height: 30, top: 45, right: 10 }}>
                            <TouchableOpacity onPress={() => this._updateBuyer()} style={{ justifyContent: 'center', alignItems: 'center', }}>

                                <Text style={{ color: '#ffa500', textAlign: 'center', fontSize: 18, borderColor: '#ff8308', borderWidth: 1, paddingLeft: 10, paddingRight: 10, paddingTop: 5, paddingBottom: 5 }}>Update Profile</Text>

                            </TouchableOpacity>
                        </View>
                    </View>
                </View>


                <View style={{ flex: 1, top: (Height * 3.5 / 100), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, }}>
                        <TextInput
                            style={{ height: 45, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                            onChangeText={name => this.setState({ name })}
                            placeholder="Name"
                            returnKeyType='next'
                            value={this.state.name}
                            autoCorrect={false}
                        />
                    </View>
                    {
                        this.state.showNameText
                            ?
                            <View style={{ marginBottom: 10, marginTop: 5 }}>
                                <Text style={{ textAlign: 'left', color: 'red', left: 15 }} > {this.state.nameText} </Text>
                            </View>
                            :
                            null
                    }

                    <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, }}>
                        <TextInput
                            style={{ height: 45, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                            placeholder="City"
                            returnKeyType='next'
                            autoCorrect={false}
                            ref={"txtCity"}
                            value={this.state.city}
                            onChangeText={city => this.setState({ city })}
                        />
                    </View>
                    {
                        this.state.showCityText
                            ?
                            <View style={{ marginBottom: 10, marginTop: 5 }}>
                                <Text style={{ textAlign: 'left', color: 'red', left: 15 }} > {this.state.cityText} </Text>
                            </View>
                            :
                            null
                    }

                    <View style={{ width: (Width * 90 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, }}>
                        <TextInput
                            style={{ height: 45, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                            placeholder="State"
                            returnKeyType='next'
                            autoCorrect={false}
                            ref={"txtState"}
                            value={this.state.state}
                            onChangeText={state => this.setState({ state })}
                        />
                    </View>
                    {
                        this.state.showStateText
                            ?
                            <View style={{ marginBottom: 10, marginTop: 5 }}>
                                <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.stateText} </Text>
                            </View>
                            :
                            null
                    }

                    <View style={{ paddingTop: 10, flex: 1, justifyContent: 'center', alignItems: 'center', }}>

                        {
                            (this.state.address !== null)
                                ?
                                <Text>Previous Address: <Text> {this.state.shwAddr} </Text></Text>
                                :
                                null
                        }

                        <GooglePlacesAutocomplete
                            placeholder='Address'
                            minLength={2}
                            autoFocus={false}
                            returnKeyType={'search'}
                            listViewDisplayed='false'
                            fetchDetails={true}
                            renderDescription={row =>
                                row.description || row.formatted_address || row.name
                            }
                            onPress={(data, details = null) => {
                                //console.log(details);
                                this.setState({
                                    address: details.formatted_address,
                                    lat: JSON.stringify(details.geometry.location.lat),
                                    long: JSON.stringify(details.geometry.location.lng),
                                    error: null,
                                });

                                this._getLocation(this.state.lat, this.state.long);
                            }}

                            query={{
                                key: API_KEY,
                                language: 'en',
                                types: '(cities)' // default: 'geocode'
                            }}

                            styles={{
                                textInputContainer: {
                                    backgroundColor: 'transparent',
                                    borderTopColor: 'transparent',
                                    width: (Width * 95 / 100),
                                    borderBottomColor: '#e68c04',
                                    borderBottomWidth: 1,
                                    marginTop: 10,
                                },
                                textInput: {
                                    height: 38,
                                    backgroundColor: 'transparent',
                                    fontSize: 18
                                },
                                description: {
                                    fontWeight: 'bold'
                                },
                                predefinedPlacesDescription: {
                                    color: '#1faadb'
                                }
                            }}

                            debounce={200}
                            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                        />

                        {
                            this.state.waitMsg
                                ?
                                <View style={{ marginBottom: 10, marginTop: 20, position: 'absolute', }}>
                                    <Text style={{ textAlign: 'left', color: '#29863d', fontSize: 16 }} > {this.state.waitMsgTxt} </Text>
                                </View>
                                :
                                null
                        }

                    </View>




                </View>



            </View>
        )
    }
}