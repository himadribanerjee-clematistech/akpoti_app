import React from 'react';
import { SafeAreaView, View, Text, Dimensions, TouchableOpacity, StyleSheet, StatusBar, TextInput, ActivityIndicator, Image, FlatList } from 'react-native';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Button, Container, Header, Title, Badge, Content, List, Body, Right, Thumbnail, Left, ListItem, Tab, Tabs } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

//const serverURL = 'http://agro.clematistech.com/';
const BannerWidth = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;
const BannerHeight = 260;
const regex = /(<([^>]+)>)/ig;

export default class MyMessageList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      serverError: false,
      connectState: true,
      showLoginButton: false,
      is_seen: '',
      selectedtab: 0,
      serverData: [],
      type: 'all',
      forceReload: false,
      errorMsg: 'Something went wrong. Please try later.'

    }

    this.CheckConnectivity();

  }

  componentDidMount() {

    this.getMessageList();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };


  getMessageList = async () => {
    if (global_isLoggedIn) {
      let url = serverURL + "api/messageList";
      let savedToken = await AsyncStorage.getItem('@token');

      var data = {
        type: this.state.type,
        is_seen: this.state.is_seen
      };
      console.log(url, data)
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': savedToken,
          'credentials': 'same-origin'
        },
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          //console.log(this.state)
          var data = res.result;

          if (res.status === 200) {

            this.setState({
              serverData: res.response_data,
              loading: false,
              serverError: false,

            });
          } else {
            this.setState({
              serverError: true,
              errorMsg: res.msg

            });
          }

        })
        .catch(error => {
          this.setState({
            serverError: true,
          });
          console.log(error)

        })
    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }

  }


  goBack() {
    this.setState({
      serverData: [],
      type: 'all',
      loading: true,
      forceReload: true,
      serverError: false,
    }, () => {
      this.props.navigation.goBack(null)
    });
  }

  async reloadSection(position) {

    if (position == 0) {

      this.setState({ selectedtab: position, type: 'all' }, () => {
        //alert(this.state.type);
        this.getMessageList();
      });
    }
    if (position == 1) {
      this.setState({ selectedtab: position, type: 'rfq' }, () => {
        //alert(this.state.type);
        this.getMessageList();
      });
    }
    if (position == 2) {
      this.setState({ selectedtab: position, type: 'enquiry' }, () => {
        //alert(this.state.type);
        this.getMessageList();
      });
    }


  }

  loadAgain() {
    this.setState({ forceReload: false })
    this.getMessageList();
  }



  render() {


    const serverData = Object.values(this.state.serverData);
    let chatCount = serverData.length;
    //console.log(chatCount);
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 0.10 }} >
              <View style={style.styles.categoriesHeader}>
                <View style={style.styles.signinHeaderContainer}>
                  <View style={style.styles.categoriesHeaderContainerLeft}>
                    {/* <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} /> */}
                    <HeaderBackButton onPress={() => this.goBack()} />
                  </View>
                  <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Message Center</Text></View>
                  <View style={{ flex: 0.10, top: (Height * 5.5 / 100) }}>
                    <Text style={{ textAlign: 'left', paddingRight: 5, color: '#04ab30' }}> </Text>
                  </View>

                  <View style={style.styles.productListHeaderContainerRighttwo}>
                  </View>
                </View>
              </View>
            </View>
            {
              (this.state.serverError) ? (
                <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
              ) :
                (!this.state.connectState) ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                  </View>
                ) :
                  (this.state.showLoginButton) ? (
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                      <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                      <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                        <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                          <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                        </Button>
                      </View>
                    </View>
                  )
                    :
                    this.state.loading ? (
                      <ActivityIndicator size="large" />
                    ) :
                      <View style={{ flex: 1 }} >
                        <Tabs onChangeTab={(obj) => this.reloadSection(obj.i)}>
                          <Tab heading="ALL">
                            <Content>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <List>
                                  {
                                    (serverData || []).map((value) => {

                                      if (value.type == 'rfq') {
                                        var profileImg = serverURL + 'assets/images/user.png';
                                        if (value.buyer_profile_image != null) {
                                          profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                        }

                                        var newArrivalColor = '#888';
                                        var headerColor = '#000';
                                        var dateColor = '#888';
                                        if (value.total_message == 0) {
                                          newArrivalColor = 'green';

                                        }

                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                          reltype: value.type,
                                          relpic: profileImg,
                                          relname: value.buyer_name,
                                          relid: value.rfq_id
                                        })} >
                                          <Left>
                                            <Thumbnail rounded source={{ uri: profileImg }} />
                                          </Left>
                                          <Body>
                                            <Text numberOfLines={1} style={{ color: headerColor, fontSize: 16 }}>{value.item_name}({value.species_name})</Text>
                                            <Text note numberOfLines={2} style={{ paddingTop: 5, paddingBottom: 5, color: newArrivalColor }}>{value.rfq_message}</Text>
                                            <Text style={{ color: dateColor }}>{value.created_date}</Text>
                                          </Body>

                                        </ListItem>

                                      }

                                      if (value.type == 'service_rfq') {
                                        var profileImg = serverURL + 'assets/images/user.png';
                                        if (value.buyer_profile_image != null) {
                                          profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                        }

                                        var newArrivalColor = '#888';
                                        var headerColor = '#000';
                                        var dateColor = '#888';
                                        if (value.total_message == 0) {
                                          newArrivalColor = 'green';

                                        }

                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                          reltype: value.type,
                                          relpic: profileImg,
                                          relname: value.buyer_name,
                                          relid: value.rfq_id
                                        })} >
                                          <Left>
                                            <Thumbnail rounded source={{ uri: profileImg }} />
                                          </Left>
                                          <Body>
                                            <Text numberOfLines={1} style={{ color: headerColor, fontSize: 16 }}>{value.item_name}({value.category_name})</Text>
                                            <Text note numberOfLines={2} style={{ paddingTop: 5, paddingBottom: 5, color: newArrivalColor }}>{value.rfq_message}</Text>
                                            <Text style={{ color: dateColor }}>{value.created_date}</Text>
                                          </Body>

                                        </ListItem>

                                      }

                                      if (value.type == 'enquiry' || value.type == 'service_enquiry') {
                                        var profileImg = serverURL + 'assets/images/user.png';
                                        if (value.buyer_profile_image != null) {
                                          profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                        }

                                        var userName = '';
                                        if (value.login_customer_id == value.enq_from_id) {
                                          if (value.seller_profile_image != null) {
                                            profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.seller_profile_image;

                                          } else {
                                            profileImg = serverURL + 'assets/images/user.png';
                                          }

                                          userName = value.seller_name;
                                        } else if (value.login_customer_id == value.enq_to_id) {
                                          if (value.buyer_profile_image != null) {
                                            profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;

                                          } else {
                                            profileImg = serverURL + 'assets/images/user.png';
                                          }
                                          userName = value.buyer_name;
                                        }

                                        var newArrivalColor = '#888';
                                        var headerColor = '#000';
                                        var dateColor = '#888';
                                        if (value.total_message == 0) {
                                          newArrivalColor = 'green';

                                        }

                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                          reltype: value.type,
                                          relpic: profileImg,
                                          relname: userName,
                                          relid: value.enq_id,
                                          relitemid: value.enq_seller_item_id
                                        })} >
                                          <Left>
                                            <Thumbnail rounded source={{ uri: profileImg }} />
                                          </Left>
                                          <Body>
                                            <Text numberOfLines={1} style={{ color: headerColor, fontSize: 16 }}>{userName}</Text>
                                            <Text note numberOfLines={2} style={{ paddingTop: 5, paddingBottom: 5, color: newArrivalColor }}>{value.enq_message}</Text>
                                            <Text style={{ color: dateColor }}>{value.enq_date}</Text>
                                          </Body>

                                        </ListItem>

                                      }

                                    })

                                  }

                                </List>
                                {
                                  (chatCount == 0) ? (<View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}><Text style={{ fontSize: 24, color: '#a94442' }}>No Data Found!!</Text></View>) : null
                                }
                              </View>
                            </Content>
                          </Tab>
                          <Tab heading="RFQs">
                            <Content>
                              <View>
                                <List>
                                  {
                                    (serverData || []).map((value) => {

                                      if (value.type == 'rfq') {
                                        var profileImg = serverURL + 'assets/images/user.png';
                                        if (value.buyer_profile_image != null) {
                                          profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                        }

                                        var newArrivalColor = '#888';
                                        var headerColor = '#000';
                                        var dateColor = '#888';
                                        if (value.total_message == 0) {
                                          newArrivalColor = 'green';

                                        }

                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                          reltype: value.type,
                                          relpic: profileImg,
                                          relname: value.buyer_name,
                                          relid: value.rfq_id
                                        })} >
                                          <Left>
                                            <Thumbnail rounded source={{ uri: profileImg }} />
                                          </Left>
                                          <Body>
                                            <Text numberOfLines={1} style={{ color: headerColor, fontSize: 16 }}>{value.item_name}({value.species_name})</Text>
                                            <Text note numberOfLines={2} style={{ paddingTop: 5, paddingBottom: 5, color: newArrivalColor }}>{value.rfq_message}</Text>
                                            <Text style={{ color: dateColor }}>{value.created_date}</Text>
                                          </Body>

                                        </ListItem>

                                      }

                                      if (value.type == 'service_rfq') {
                                        var profileImg = serverURL + 'assets/images/user.png';
                                        if (value.buyer_profile_image != null) {
                                          profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                        }

                                        var newArrivalColor = '#888';
                                        var headerColor = '#000';
                                        var dateColor = '#888';
                                        if (value.total_message == 0) {
                                          newArrivalColor = 'green';

                                        }

                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                          reltype: value.type,
                                          relpic: profileImg,
                                          relname: value.buyer_name,
                                          relid: value.rfq_id
                                        })}>
                                          <Left>
                                            <Thumbnail rounded source={{ uri: profileImg }} />
                                          </Left>
                                          <Body>
                                            <Text numberOfLines={1} style={{ color: headerColor, fontSize: 16 }}>{value.item_name}({value.category_name})</Text>
                                            <Text note numberOfLines={2} style={{ paddingTop: 5, paddingBottom: 5, color: newArrivalColor }}>{value.rfq_message}</Text>
                                            <Text style={{ color: dateColor }}>{value.created_date}</Text>
                                          </Body>

                                        </ListItem>

                                      }


                                    })
                                  }

                                </List>
                                {
                                  (chatCount == 0) ? (<View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}><Text style={{ fontSize: 24, color: '#a94442' }}>No Data Found!!</Text></View>) : null
                                }
                              </View>
                            </Content>
                          </Tab>
                          <Tab heading="ENQUIRY">
                            <Content>
                              <View>
                                <List>
                                  {
                                    (serverData || []).map((value) => {

                                      if (value.type == 'enquiry' || value.type == 'service_enquiry') {
                                        var profileImg = serverURL + 'assets/images/user.png';
                                        if (value.buyer_profile_image != null) {
                                          profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                        }

                                        var userName = '';
                                        if (value.login_customer_id == value.enq_from_id) {
                                          if (value.seller_profile_image != null) {
                                            profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.seller_profile_image;

                                          } else {
                                            profileImg = serverURL + 'assets/images/user.png';
                                          }

                                          userName = value.seller_name;
                                        } else if (value.login_customer_id == value.enq_to_id) {
                                          if (value.buyer_profile_image != null) {
                                            profileImg = serverURL + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;

                                          } else {
                                            profileImg = serverURL + 'assets/images/user.png';
                                          }
                                          userName = value.buyer_name;
                                        }

                                        var newArrivalColor = '#888';
                                        var headerColor = '#000';
                                        var dateColor = '#888';
                                        if (value.total_message == 0) {
                                          newArrivalColor = 'green';

                                        }

                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                          reltype: value.type,
                                          relpic: profileImg,
                                          relname: userName,
                                          relid: value.enq_id,
                                          relitemid: value.enq_seller_item_id
                                        })} >
                                          <Left>
                                            <Thumbnail rounded source={{ uri: profileImg }} />
                                          </Left>
                                          <Body>
                                            <Text numberOfLines={1} style={{ color: headerColor, fontSize: 16 }}>{userName}</Text>
                                            <Text note numberOfLines={2} style={{ paddingTop: 5, paddingBottom: 5, color: newArrivalColor }}>{value.enq_message}</Text>
                                            <Text style={{ color: dateColor }}>{value.enq_date}</Text>
                                          </Body>

                                        </ListItem>

                                      }

                                    })
                                  }

                                </List>
                                {
                                  (chatCount == 0) ? (<View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                                    <Text style={{ fontSize: 24, color: '#a94442' }}>No Data Found!!</Text>
                                  </View>) : null
                                }
                              </View>
                            </Content>
                          </Tab>
                        </Tabs>
                      </View>
            }


          </View>
        </Container>
      </SafeAreaView>
    );
  }

}