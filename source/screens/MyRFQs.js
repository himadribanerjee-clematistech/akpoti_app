import React from 'react';
import { YellowBox, SafeAreaView, View, Text, Dimensions, ActivityIndicator } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Button, Container, Content, List, Body, Thumbnail, Left, ListItem } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

//const serverURL = 'http://agro.clematistech.com/';
const BannerWidth = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;
const BannerHeight = 260;
const regex = /(<([^>]+)>)/ig;

export default class MyRFQs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      serverData: [],
      pageno: 1,
      unread: 0,
      count: 0,
      serverError: false,
      connectState: true,
      showLoginButton: false,
      errorMsg: 'Something went wrong. Please try later.'

    }

    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentWillUnmount() {
    // Remove the event listener before removing the screen from the stack
    this.focusListener.remove();
  }

  componentDidMount() {
    const { navigation } = this.props;
    //Adding an event listner on focus
    //So whenever the screen will have focus it will set the state to zero
    this.focusListener = navigation.addListener('didFocus', () => {
      console.log('page reloads');
      this.forceUpdateHandler();
    });

  }

  forceUpdateHandler() {
    this.setState({
      loading: true,
      serverData: [],
      pageno: 1,
      unread: 0,
      count: 0,
      serverError: false,
      connectState: true,
      showLoginButton: false,
      errorMsg: 'Network connection occured. Please try later.'

    }, () => { this.getRFQListData() })
  };

  loadPagination() {
    return (
      //Footer View with Load More button        
      (this.state.pageno > 0) ?

        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
          <Button style={{ padding: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.loadMorePaginationData()}>
            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Load More</Text>
          </Button>

        </View>
        : null
    );
  }

  loadMorePaginationData() {
    this.getRFQListData();
    //console.log('AA');
  }


  getRFQListData = async () => {
    if (global_isLoggedIn) {
      let url = serverURL + "api/rfq/getRFQ";
      let savedToken = await AsyncStorage.getItem('@token');

      var data = {
        pageno: this.state.pageno,
      };
      //console.log(savedToken,url,data)
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          Authorization: savedToken,
          'credentials': 'same-origin'
        },
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          //console.log(res)
          var data = res.result;

          if (res.status === 200) {

            if (this.state.serverData && this.state.serverData.length) {
              var serverDataPagination = this.state.serverData.concat(res.response_data);

              this.setState({
                serverData: serverDataPagination,
                pageno: res.next,
                loading: false,
                count: res.response_data.length,
                serverError: false,
              });
            } else {
              this.setState({
                serverData: res.response_data,
                count: res.count,
                pageno: res.next,
                loading: false,
                serverError: false,
              });
            }

          } else {
            this.setState({
              serverError: true,
              errorMsg: res.msg
            });
          }

        })
        .catch(error => {
          console.log(error)
        })
    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true
      });
    }
  }

  extendRfqExpiry = async (rfq_id) => {
    this.setState(state => {
      const list = state.serverData.map((item, j) => {
        if (item.rfq_id === rfq_id) {
          item.is_expiry_date = 2;
          return item;
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });

    let url = serverURL + "api/rfq/extendsRfqExpiry";
    let savedToken = await AsyncStorage.getItem('@token');

    var data = {
      rfq_id: rfq_id,
    };
    //console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res)  
        if (res.status == 200) {
          Alert.alert(
            'Success !',
            res.msg + ' New expiry date: ' + res.expiry_date, [
            { text: 'OK', onPress: () => console.log('..'), style: 'cancel' },],
            { cancelable: false })
        } else {
          Alert.alert(
            'Success !',
            res.msg, [
            { text: 'OK', onPress: () => console.log('..'), style: 'cancel' },],
            { cancelable: false })
          this.setState({
            isSubmited: false,
          })
        }


      })
      .catch(error => {
        console.log("aa", JSON.stringify(error))
      })

  }




  render() {

    const serverData = Object.values(this.state.serverData);

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 0.10 }} >
              <View style={style.styles.categoriesHeader}>
                <View style={style.styles.signinHeaderContainer}>
                  <View style={style.styles.categoriesHeaderContainerLeft}>
                    <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                  </View>
                  <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 18, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Request For Quotations</Text></View>
                  <View style={{ flex: 0.20, top: (Height * 5.5 / 100) }}>
                    {
                      (this.state.unread > 0) ?
                        <Text style={{ textAlign: 'left', paddingRight: 5, color: '#04ab30' }}> {this.state.count} RFQs</Text>
                        :
                        <Text style={{ textAlign: 'left', paddingRight: 5, color: '#04ab30' }}> </Text>
                    }
                  </View>

                  <View style={style.styles.productListHeaderContainerRighttwo}>
                  </View>
                </View>
              </View>
            </View>
            {
              (this.state.serverError) ? (
                <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
              ) :
                (!this.state.connectState) ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                  </View>
                ) :
                  (this.state.showLoginButton) ? (
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                      <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                      <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                        <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                          <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                        </Button>
                      </View>
                    </View>
                  ) :
                    (!this.state.loading && this.state.serverData.length == 0) ?
                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 24, color: '#a94442' }}>No Data Found!!</Text>
                    </View>
                      :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :

                        <View style={{ flex: 0.80 }} >
                          <Content>
                            <View>
                              <List>
                                {
                                  (serverData || []).map((y) => {
                                    let profileImg = serverURL + 'assets/images/user.png';
                                    if (y.buyer_profile_image != null) {
                                      profileImg = serverURL + 'admin/uploads/bussiness_logo/' + y.buyer_profile_image;
                                    }
                                    let lightcolor = '#888'
                                    let deepcolor = '#000'

                                    let displayLi = '';
                                    if (y.category_name) {
                                      displayLi += '' + y.category_name + '';
                                    }
                                    if (y.item_name) {
                                      displayLi += ' >' + y.item_name + '';
                                    }
                                    if (y.species_name) {
                                      displayLi += ' >' + y.species_name + '';
                                    }
                                    let other_requerment = ''
                                    if (y.other_requerment != '') {
                                      other_requerment = 'Other requirement:' + y.other_requerment;
                                    }

                                    return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                      reltype: y.type,
                                      relpic: profileImg,
                                      relname: y.buyer_name,
                                      relid: y.rfq_id
                                    })}>
                                      <Left>
                                        <Thumbnail rounded source={{ uri: profileImg }} />
                                      </Left>
                                      <Body>
                                        <Text style={{ color: deepcolor }}>{y.buyer_name}</Text>
                                        <Text note numberOfLines={1} style={{ paddingTop: 5, paddingBottom: 2, color: lightcolor }}>{displayLi}</Text>
                                        <Text note numberOfLines={1} style={{ paddingTop: 2, paddingBottom: 2, color: lightcolor }}>Quantity: {y.quantity} {y.uom_name}</Text>
                                        <Text note numberOfLines={3} style={{ paddingTop: 5, paddingBottom: 1, color: deepcolor }}>{y.rfq_message} {other_requerment}</Text>
                                        {
                                          (y.images || []).map((img) => {
                                            return <Thumbnail square source={{ uri: serverURL + 'admin/uploads/rfq/' + img.image }} />
                                          })
                                        }
                                        <Text style={{ color: lightcolor, paddingTop: 5 }}>{y.created_date}</Text>
                                        {
                                          (y.is_expiry_date == 1) ? <Button info style={{ width: '35%', marginTop: 10, justifyContent: 'center' }} onPress={() => this.markasreadextendRfqExpiry(y.rfq_id)}>
                                            <Text style={{ color: '#fff' }}>Extend RFQ</Text>
                                          </Button> : (y.is_expiry_date == 0) ? <Button light style={{ width: '35%', marginTop: 10, justifyContent: 'center' }}>
                                            <Text style={{ color: '#888' }}>Extend RFQ</Text>
                                          </Button> : <Text></Text>
                                        }



                                      </Body>
                                    </ListItem>

                                  })
                                }

                              </List>
                              {
                                this.loadPagination()
                              }
                            </View>
                          </Content>
                        </View>
            }

            {
              this.state.loading ? (
                <Text style={{ color: 'white' }}></Text>
              ) :
                <View style={{ flex: 0.10, marginBottom: 0, paddingBottom: 0 }} >
                  <Footer style={{ height: '100%' }} >
                    <FooterTab style={{ backgroundColor: '#000' }}>
                      <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                        <Icon name="ios-home" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Home</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                        <Icon name="ios-person" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Myakpoti</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                        <Icon name="ios-list" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Categories</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                        <Icon name="ios-cog" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Services</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                        <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Messenger</Text>
                      </Button>
                    </FooterTab>
                  </Footer>
                </View>
            }
          </View>
        </Container>
      </SafeAreaView>
    );
  }

}
