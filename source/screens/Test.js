import React from 'react'
import { View, Text, Image, Button } from 'react-native'
import ImagePicker from 'react-native-image-picker'
const savedToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl9pZCI6IjA4Yjk5ZjJlODllNjZkYmYzNDMzMWEwYmM4ZWNjZjY3In0.i_Lsf7UjZbIb-U6--JJVw2roBy1fHrwiStnah8Zlk7g';
const createFormData = (photo, body) => {
  const data = new FormData();
 
  photo.forEach((item, i) => {
    data.append("image[]", {
      uri:  Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
      type:  item.type,
      name: item.filename || `filename${i}.jpg`,
    });
  });
 /* data.append("photo", {
    name: photo.fileName,
    type: photo.type,
    uri:
      Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
  });
*/
  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};
export default class App extends React.Component {
  state = {
    photo: null,
    selectedImages:[],
  }
  
  handleUploadPhoto = () => {


    var url = 'http://agro.clematistech.com/api/seller/testdata'; // File upload web service path

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    console.log('OPENED', xhr.status);
    xhr.responseType = "blob"; 
    xhr.onprogress = function () {
        console.log('LOADING', xhr.status);
    };
  
    xhr.onload = function () {
        console.log('DONE', xhr.responseText);
    };
    
    xhr.setRequestHeader('authorization',savedToken);
    xhr.send(createFormData(this.state.selectedImages, { userId: "123", userId1: "123"}));

    
    
  };
  
  handleChoosePhoto = () => {
    const options = {
      noData: true,
      multiple:true
    }
    ImagePicker.launchImageLibrary(options, response => {
    
      if (response.uri) {
        this.setState({ photo: response });
      
      }

      if (response.uri) {
        var selectedImg = [];
        this.state.selectedImages.map((y) => {                                
          selectedImg.push(y);            
      })
      selectedImg.push(response); 
        this.setState({ selectedImages: selectedImg })

        //console.log(this.state.selectedImages)
      }
    })
  }

  render() {
    const { photo } = this.state
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

        <View style={{ flex: .5, alignItems: 'center',flexDirection:'column', justifyContent: 'center' }}>
          <View style={{flex: 1, paddingTop:20, paddingBottom:10 }}>
          {                         

this.state.selectedImages.map((y) => {                             
 return  <View style={{flex:0.5,paddingLeft:5}}>
   <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
 </View>
 <Image
  source={{ uri: y.uri }}
  style={{ width: 100, height: 100 }}
/></View>            
})
}
            </View>
          </View>
         <View style={{ flex: .5, alignItems: 'center', justifyContent: 'center' }}> 
      

               
        <Button title="Choose Photo" onPress={this.handleChoosePhoto} />
       
        </View>
        <View style={{flex:0.5,paddingLeft:5}}>
        <Button title="send" onPress={this.handleUploadPhoto} />
        </View>
      </View>
      
       
    )
  }
}