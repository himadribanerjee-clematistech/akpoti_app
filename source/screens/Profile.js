import React from 'react';
import { YellowBox, View, Text, StatusBar, Dimensions, TouchableOpacity, Platform, Alert, Image, Modal, TextInput, ScrollView, PermissionsAndroid, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, Content, Item, Form, Label, Input } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import { NavigationEvents } from 'react-navigation';
import NetInfo from "@react-native-community/netinfo";

import RNFetchBlob from 'rn-fetch-blob';

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.CheckConnectivity();
        //this._callOnLoad();
        this._loadInitialState();

        this.state = {
            noteCount: false,
            count: null,
            showLoader: false,
            showEmailModal: false,
            showPassChangeModal: false,
            showSellerModal: false,
            showProPicModal: false,

            info: false,
            verifyEmailModal: false,
            showVEmailOTP: true,
            vEmailOTP: null,

            showMailModal: false,
            showPassModal: false,
            showOTP: false,
            userOTP: '',

            showNameText: false,
            nameText: 'Name is Required',
            showAddressText: false,
            addressText: 'Address is Required',
            showCityText: false,
            cityText: 'City is Required',
            showStateText: false,
            stateText: 'State is Required',
            showContactNameText: true,
            contactNameText: 'Contact Name is Required',
            showContactNumberText: true,
            contactNumberText: 'Contact Number is Required',
            showImageText: true,
            imageText: 'Image is Required',

            showMailText: false,
            userMail: '',

            iconOld: 'eye-off',
            icon: 'eye-off',
            iconCheck: 'eye-off',

            is_seller: false,
            is_agent: false,

            shwPic: '',
            shwName: null,
            shwMob: null,
            shwMail: null,
            shwMobVerified: '',
            shwMailVerified: '',
            shwAddr: null,
            shwCity: null,
            shwState: null,

            bname: '',
            bcity: '',
            bstate: '',
            bcontactname: '',
            bcontact: '',
            baddress: '',
            bimages: null,

            showOldPass: true,
            showPass: true,
            showConfPass: true,

            showOTPText: false,
            showPassText: false,
            showOldPassText: false,
            showCheckPassText: false,

            userOldPass: '',
            userPass: '',
            userPassCheck: '',

            photo: null,
            shwImg: true,
            shwImgButt: false,
            avatarSource: null,

            sellerBox: true,
            sellerDoc: true,
            agentDoc: true,

            shwSellerVw: false,
            shwsellerName: null,
            shwsellerAddr: null,
            shwsellerCity: null,
            shwsellerState: null,
            shwsellerCName: null,
            shwsellerCNum: null,
            shwsellerDoc: [],

            shwAgentVw: false,
            shwAgentImg: [],

            existingImages: [],

            shwImgFull: true,

            errorMsg: null,
            showLoginButton: false,
            connectState: true,

        }
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };

    _callOnLoad = () => {
        this._doAuthenticateMe().done();
        this._getNotificationCount().done();
    }

    _loadInitialState = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                if (global_isLoggedIn) {
                    this.setState({
                        errorMsg: '',
                        showLoginButton: false
                    });
                    this._callOnLoad();
                }
                else if (tokenSaved != null) {
                    this.setState({
                        errorMsg: '',
                        showLoginButton: false
                    });
                    this._callOnLoad();
                }
                else {
                    this.setState({
                        errorMsg: 'Please login to view this section.',
                        showLoginButton: true
                    });
                }

                console.log(tokenSaved);
            } else {
                this.setState({
                    connectState: false,
                });
            }
        });


    }

    componentDidMount() {
        this._loadInitialState().done();
    }

    _logout = () => {
        Alert.alert(
            '',
            'Are You Want To Logged Out ?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Yes',
                    onPress: () => this.logout()
                },
            ],
            { cancelable: true },
        );
    }

    logout = async () => {
        const keys = ['@token', '@total_info']
        try {
            //alert('Pressed!');            
            await AsyncStorage.multiRemove(keys);
            this.props.navigation.replace('Home');
            global_isLoggedIn = false;
            global_LoggedInCustomerName = '';
            global_LoggedInCustomerId = '';
            global_LoggedInSellerId = '';
            global_is_seller = '';
            global_is_agent = '';
            global_profileImage = 'http://agro.clematistech.com/assets/images/user.png';
        }
        catch (error) {
            console.error();
        }
    }

    _doAuthenticateMe = async () => {
        try {
            const savedToken = await AsyncStorage.getItem('@token');

            fetch(serverURL + 'api/login/doAuthenticateMe', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': savedToken,
                },
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);

                    if (responseJson[0] == 200) {
                        var data = responseJson[1].result;
                        var buyer = data.buyer_info;
                        var seller = data.seller_info;

                        AsyncStorage.setItem('@customer_id', data.id);

                        global_isLoggedIn = true;
                        global_LoggedInCustomerId = responseJson[1].result.id;
                        global_is_seller = responseJson[1].result.is_seller;
                        global_is_agent = responseJson[1].result.is_agent;

                        if (typeof (responseJson[1].result.seller_info.id) != 'undefined') {
                            global_LoggedInSellerId = responseJson[1].result.seller_info.id;
                        }

                        if (responseJson[1].result.buyer_info.profile_image) {
                            var profileImg = serverURL + 'admin/uploads/bussiness_logo/' + responseJson[1].result.buyer_info.profile_image;
                            global_profileImage = profileImg;

                        }
                        global_LoggedInCustomerName = responseJson[1].result.name;

                        this.setState({

                            name: data.name,
                            mob: data.phone,
                            mail: data.email,
                            address: buyer.address,
                            city: buyer.city,
                            state: buyer.state,

                            shwPic: buyer.profile_image,
                            shwName: data.name,
                            shwMob: data.phone,
                            shwMail: data.email,

                            is_seller: data.is_seller,
                            is_agent: seller.agent_YN,

                            shwMobVerified: data.phone_verify,
                            shwMailVerified: data.email_verify,

                            shwAddr: buyer.address,
                            shwCity: buyer.city,
                            shwState: buyer.state,

                            shwsellerName: seller.business_name,
                            shwsellerAddr: seller.address,
                            shwsellerCity: seller.city,
                            shwsellerState: seller.state,
                            shwsellerCName: seller.contact_name,
                            shwsellerCNum: seller.contact_number,
                        });


                        if (this.state.is_seller == 1) {
                            let extImg = [];
                            seller.images.map((y) => {
                                extImg.push(y);
                            });

                            this.setState({ existingImages: extImg, shwSellerVw: true });
                        }
                        else {
                            this.setState({ shwSellerVw: false });
                        }

                        if (this.state.is_agent == 1) {
                            let extAgntImg = [];
                            data.agent_doc.map((y) => {
                                extAgntImg.push(y);
                            });

                            this.setState({
                                shwAgentVw: true,
                                shwAgentImg: extAgntImg
                            });
                        }
                        else {
                            this.setState({ shwAgentVw: false });
                        }
                    }
                    else {
                        this.logout();
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (e) {
            console.log(e);
        }
    }

    _getNotificationCount = async () => {
        try {
            const savedToken = await AsyncStorage.getItem('@token');

            fetch(serverURL + 'api/main/getNotificationUnReadCount', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': savedToken,
                },
                body: JSON.stringify({ pageno: 1 })
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);

                    if (responseJson[0] == 200) {
                        var newNotification = parseInt(responseJson[1].unread);

                        if (newNotification > 0) {
                            this.setState({ noteCount: true, count: newNotification });
                        }
                    }
                    else {
                        console.log(responseJson);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (e) {
            console.log(e);
        }
    }

    _openEdit() {
        this.props.navigation.navigate('EditBuyer');
    }

    _openSeller = () => {
        this.props.navigation.navigate('UpgradeSeller');
    }

    _openAgent = () => {
        this.props.navigation.navigate('UpgradeAgent');
    }

    _openMailChange = () => {
        this.setState({ showMailModal: true });
    }

    _openEmailVerify = () => {
        this.setState({ verifyEmailModal: true });
    }

    _openPassChange = () => {
        this.setState({ showPassModal: true });
    }

    _sendOTP = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');

        fetch(serverURL + 'api/emailOTPSend', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': tokenSaved,
            },
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status == 200) {
                    this.setState({ showOTP: true });
                }
                else {
                    alert(responseJson.msg);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _updateMail = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');

        let Mail = this.state.userMail;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (Mail == '') {
            this.setState({
                showMailText: true,
                emailMessage: 'E-mail is Required',
            });
        }
        else if (reg.test(Mail) === false) {
            this.setState({
                showMailText: true,
                emailMessage: 'E-mail Format will be like example@example.com',
            });
        }
        else {
            this.setState({
                showMailText: false,
            });

            fetch(serverURL + 'api/emailupdate', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': tokenSaved,
                },
                body: JSON.stringify({
                    otp: this.state.userOTP,
                    email: this.state.userMail,
                }),
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    if (responseJson[0] == 200) {
                        this._doAuthenticateMe();
                        this.setState({
                            showMailText: false,
                            showMailModal: false,
                        });
                    }
                    else {
                        if ((responseJson[1].field) == 'email') {
                            this.setState({
                                showMailText: true,
                                emailMessage: responseJson[1].msg,
                            });
                        }
                        else {
                            alert(responseJson[1].msg);
                        }
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    _sendPOTP = async () => {
        var TokenSaved = await AsyncStorage.getItem('@token');

        fetch(serverURL + 'api/changePasswordOTPSend', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': TokenSaved,
            },
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status == 200) {
                    this.setState({ showOTP: true });
                }
                else {
                    alert(responseJson.msg);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _showOldPassword = () => {
        this.setState(prevState => ({
            iconOld: prevState.iconOld === 'eye' ? 'eye-off' : 'eye',
            showOldPass: !prevState.showOldPass
        }))
    }

    _showPassword = () => {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            showPass: !prevState.showPass
        }))
    }

    _showConfPassword = () => {
        this.setState(prevState => ({
            iconCheck: prevState.iconCheck === 'eye' ? 'eye-off' : 'eye',
            showConfPass: !prevState.showConfPass
        }))
    }

    _checkPass = () => {
        var Pass = this.state.userPass;
        var checkPass = this.state.userPassCheck;

        if (Pass === checkPass) {
            this.setState({
                showCheckPassText: false,
            });
        }
        else {
            this.setState({
                showCheckPassText: true,
            });
        }
    }

    _updateMail = async () => {
        var otp = this.state.userOTP;
        var old = this.state.userOldPass;
        var pass = this.state.userPass;
        var con_pass = this.state.userPassCheck;

        var TokenSaved = await AsyncStorage.getItem('@token');

        if (otp == '') {
            this.setState({
                showOTPText: true,
            })
        }
        else if (old == '') {
            this.setState({
                showOTPText: false,
                showOldPassText: true,
            })
        }
        else if (pass == '') {
            this.setState({
                showOTPText: false,
                showOldPassText: false,
                showPassText: true,
            })
        }
        else {
            this.setState({
                showPassText: false,
            });

            fetch(serverURL + 'api/changePassword', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': TokenSaved,
                },
                body: JSON.stringify({
                    old_password: old,
                    new_password: pass,
                    confirm_password: con_pass,
                    otp: otp,
                }),
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    if (responseJson[0] == 200) {
                        Alert.alert('Congratulations!!', responseJson[1].msg);

                        this.setState({
                            showOTP: false,
                            showPassModal: false,
                        })

                        setTimeout(() => {
                            this.props.navigation.navigate('Home');
                        }, 1000);
                    }
                    else {
                        Alert.alert('Some Error!', responseJson[1].msg);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });

        }
    }

    _openProPicChange = () => {
        this.setState({ showProPicModal: true });
    }

    _saveProPic = async () => {
        try {

            const options = {
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                    quality: 0.06
                },
            };

            ImagePicker.launchImageLibrary(options, (response) => {
                console.log('Response Object = ', response);

                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                }
                else {

                    //var i = Math.floor(Math.log(response.fileSize) / Math.log(1024));
                    //console.log(i)
                    if (response.fileSize > 2000000) {
                        Alert.alert('Error!', 'Image Size Greater Than 2 MB');
                        this.setState({ showProPicModal: false });
                        this._doAuthenticateMe().done();
                    }
                    else {
                        const source = { uri: response.uri };
                        this.setState({
                            shwImg: false,
                            avatarSource: source,
                            shwImgButt: true,
                            photo: response,
                        });
                    }
                }
            });

        } catch (err) {
            console.warn(err);
        }
    }

    _fetchLatLong = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                title: 'Location Permission',
                message: 'Akpoti Need To Access Your Location, Please Accept To Serve You Better',
                buttonNegative: 'Decline',
                buttonPositive: 'Accept',
            },
            );

            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.getLongLat = Geolocation.getCurrentPosition(
                    (position) => {
                        this.setState({
                            lat: JSON.stringify(position.coords.latitude),
                            long: JSON.stringify(position.coords.longitude),
                            error: null,
                        });

                        console.log(this.state.lat, this.state.long);
                    },
                    (error) => {
                        var code = error.code;
                        if (code == 2) {
                            alert('Please Enable Your GPS in Your Device');
                        }
                        else {
                            alert('Error Message : ' + error.message);
                        }
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, }
                );
            }
            else {
                alert('You Have to give permission to invoolv to serve you better!');
            }
        }
        catch (e) {
            console.log(e);
        }
    }

    _changeProPic = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');
        const photoData = this.state.photo;

        RNFetchBlob.fetch('POST', serverURL + 'api/updateBuyerImage', {
            Authorization: tokenSaved,
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        }, [
            {
                name: 'image',
                filename: photoData.fileName,
                type: photoData.type,
                tmp_name: Platform.OS === "android" ? photoData.uri : photoData.uri.replace("file://", ""),
                size: photoData.fileSize,
                data: photoData.data
            }]
        )
            .then(response => {
                //console.log(response);
                const status = response.respInfo.status;
                const data = response.json();

                return Promise.all([status, data]);
            })
            .then((responseJson) => {
                //console.log(responseJson);

                if (responseJson[0] == 200) {
                    Alert.alert('Success Message !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.setState({ showProPicModal: false, }) }], { cancelable: false });
                    this._doAuthenticateMe();
                }
                else {
                    Alert.alert('Error Message !!', responseJson[1].msg);
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    _resendVEmailOTP = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');

        this.setState({ info: true, });

        fetch(serverURL + 'api/verifyemailOTPSend', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': tokenSaved,
            },
            body: JSON.stringify({
                email: this.state.shwMail,
            }),
        })
            .then(response => {
                const status = response.status;
                const data = response.json();

                return Promise.all([status, data]);
            })
            .then((responseJson) => { })
            .catch((error) => {
                console.error(error);
            });
    }

    _openEmailVerify = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');

        fetch(serverURL + 'api/verifyemailOTPSend', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': tokenSaved,
            },
            body: JSON.stringify({
                email: eMail,
            }),
        })
            .then(response => {
                const status = response.status;
                const data = response.json();

                return Promise.all([status, data]);
            })
            .then((responseJson) => {
                //console.log(responseJson);
                if (responseJson[0] == 200) {
                    this.setState({ verifyEmailModal: true });
                }
                else {
                    alert(responseJson[1].msg);
                    this.setState({ verifyEmailModal: false });
                }
            })
            .catch((error) => {
                console.error(error);
            });

        //this.setState({ verifyEmailModal: true });
    }

    _verifyMail = async () => {
        var tokenSaved = await AsyncStorage.getItem('@token');

        this.setState({ info: false, showLoader: true });

        let OtP = this.state.vEmailOTP;
        let eMail = this.state.shwMail

        if (this.state.vEmailOTP == null) {
            this.setState({
                showOTPText: true,
                otpMessage: 'OTP Field is Required',
                showLoader: false
            });
        }
        else {

            this.setState({
                showMailText: false,
            });
            //console.log(this.state.vEmailOTP);

            fetch(serverURL + 'api/verifyemailupdate', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': tokenSaved,
                },
                body: JSON.stringify({
                    otp: OtP,
                    email: eMail,
                }),
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);
                    if (responseJson[0] == 200) {
                        this.setState({
                            showLoader: false,
                            verifyEmailModal: false,
                        });
                        this._doAuthenticateMe();
                    }
                    else {
                        alert(responseJson[1].msg);
                        this.setState({
                            showLoader: false,
                            verifyEmailModal: true,
                        });
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    _deleteDocument = async (image_id) => {
        let savedToken = await AsyncStorage.getItem('@token');

        fetch(serverURL + 'api/sellerImageRemove', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': savedToken,
                'credentials': 'same-origin'
            },
            body: JSON.stringify({
                seller_image_id: image_id,
            }),
        })
            .then(response => {
                const status = response.status;
                const data = response.json();

                return Promise.all([status, data]);
            })
            .then((responseJson) => {
                //console.log(responseJson);
                if (responseJson[0] == 200) {
                    Alert.alert(
                        'Success !',
                        'Image Successfully Deleted',
                        [{ text: 'OK', onPress: () => this._doAuthenticateMe(), style: 'cancel' },],
                        { cancelable: false }
                    );
                }
                else {
                    alert(responseJson[1].msg);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _addSellerDoc = () => {
        this.props.navigation.navigate('SellerNewDoc');
    }

    render() {

        const { avatarSource } = this.state;

        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#F7F9F9' }}>

                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <NavigationEvents onDidFocus={() => this._loadInitialState()} />

                <View style={{ width: Width, height: (Height * 12 / 100), backgroundColor: '#FFF', borderColor: '#ddd', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 3, elevation: 5, }}>


                    {
                        (this.state.noteCount)
                            ?
                            <View style={{ position: 'absolute', zIndex: 1, top: (Height * 6.3 / 100), left: (Width * 6 / 100) }}>
                                <Text style={{ fontSize: 16, backgroundColor: '#ffa500', color: '#FFF', padding: 2, borderRadius: 50 }} > {this.state.count} </Text>
                            </View>
                            :
                            null
                    }


                    {
                        (this.state.showLoginButton)
                            ?
                            (
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ top: (Height * 3 / 100), fontSize: 20, }}> Profile </Text>
                                </View>
                            )
                            :
                            (
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 0.20, justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('My Notifications')}>
                                            <Icon name="ios-notifications-outline" style={{ fontSize: 40, color: '#000', }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 0.65, }}>
                                        <Text style={{ top: (Height * 7.5 / 100), fontSize: 16, textAlign: 'right' }}> {this.state.shwName} </Text>
                                    </View>
                                    <View style={{ flex: 0.15, justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                                        <Icon name="ios-power" style={{ fontSize: 30, color: '#e68c04', }} onPress={() => this._logout()} />
                                    </View>
                                </View>
                            )
                    }
                </View>

                {
                    (!this.state.connectState) ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                        </View>
                    ) :
                        (this.state.showLoginButton)
                            ?
                            (
                                <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                                    <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                                    <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                                        <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                                        </Button>
                                    </View>
                                </View>
                            )
                            :

                            (
                                <ScrollView>

                                    {
                                        (this.state.is_seller == 0)
                                            ?
                                            <View style={{ flexDirection: 'row-reverse', top: (Height * 2 / 100), right: (Width * 2 / 100) }}>
                                                <TouchableOpacity onPress={() => this._openSeller()}>
                                                    <View style={{ paddingTop: 7, paddingBottom: 7, paddingLeft: 13, paddingRight: 13, borderColor: '#ff8308', borderWidth: 1 }}>
                                                        <Text style={{ color: '#e68c04', fontSize: 15, }}>
                                                            Upgrade To Seller
                                                    </Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                            :
                                            (this.state.is_seller == 1 && this.state.is_agent != 1)
                                                ?
                                                <View style={{ flexDirection: 'row-reverse', top: (Height * 2 / 100), right: (Width * 2 / 100) }}>
                                                    <TouchableOpacity onPress={() => this._openAgent()}>
                                                        <View style={{ paddingTop: 7, paddingBottom: 7, paddingLeft: 13, paddingRight: 13, borderColor: '#ff8308', borderWidth: 1 }}>
                                                            <Text style={{ color: '#e68c04', fontSize: 15, }}>
                                                                Upgrade To Agent
                                                        </Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                null
                                    }

                                    {
                                        (this.state.is_agent == 1)
                                            ?
                                            <View style={{ flexDirection: 'row-reverse', top: (Height * 2 / 100), right: (Width * 2 / 100), }}>
                                                <View style={{ marginRight: 10 }}>
                                                    <TouchableOpacity>
                                                        <View style={{ paddingTop: 7, paddingBottom: 7, paddingLeft: 13, paddingRight: 13, borderColor: '#ff8308', borderWidth: 1 }}>
                                                            <Text style={{ color: '#e68c04', fontSize: 15, }}>
                                                                My Items
                                                        </Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>

                                                <View style={{ marginRight: 10 }}>
                                                    <TouchableOpacity>
                                                        <View style={{ paddingTop: 7, paddingBottom: 7, paddingLeft: 13, paddingRight: 13, borderColor: '#ff8308', borderWidth: 1 }}>
                                                            <Text style={{ color: '#e68c04', fontSize: 15, }}>
                                                                My Services
                                                        </Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>

                                            :
                                            null
                                    }


                                    <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, marginTop: 30, borderRadius: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15, paddingBottom: 15 }}>
                                            <View style={{ flex: 0.30, alignItems: 'center', }}>
                                                <View style={{ padding: 10, borderColor: '#e68c04', borderWidth: 1, borderRadius: 100, }}>
                                                    {
                                                        (this.state.shwPic == null)
                                                            ?
                                                            <Image
                                                                source={require('../assets/images/user.png')}
                                                                style={{ width: 78, height: 78, }}
                                                            />
                                                            :
                                                            <Image
                                                                style={{ width: 80, height: 80, borderRadius: 100 }}
                                                                source={{ uri: serverURL + 'admin/uploads/bussiness_logo/' + this.state.shwPic }}
                                                            />
                                                    }

                                                </View>

                                                <View style={{ position: 'absolute', zIndex: 1, top: (Height * 9 / 100), left: (Width * 17 / 100) }}>
                                                    <Icon name="ios-camera" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => this._openProPicChange()} />
                                                </View>
                                            </View>
                                            <View style={{ flex: 0.70, }}>
                                                <Text style={{ fontSize: 20, paddingLeft: 5, }}>{this.state.shwName}</Text>
                                                <View style={{ flexDirection: 'row', paddingTop: 5, }}>
                                                    <View style={{ paddingTop: 5, paddingLeft: 5, }}>
                                                        <Text>Mobile: <Text> {this.state.shwMob} </Text> <Text style={{ color: '#e68c04' }}>[ {(this.state.shwMobVerified == '1') ? 'Verified' : 'Verify Mobile'} ]</Text> </Text>
                                                        <Text>E-mail:
                                                        <Text> {this.state.shwMail} </Text>
                                                            {
                                                                (this.state.shwMailVerified == '1')
                                                                    ?
                                                                    <Text style={{ color: '#e68c04' }}>[ Verified ]</Text>
                                                                    :
                                                                    <Text style={{ color: '#e68c04' }} onPress={() => this._openEmailVerify()}>[ Verify E-mail ]</Text>
                                                            }
                                                        </Text>

                                                        <Text style={{ top: 10 }}>
                                                            <Text style={{ color: '#e68c04', }} onPress={() => this._openMailChange()}>Change E-mail Address</Text>
                                                        </Text>
                                                        <Text style={{ top: 10 }}>
                                                            <Text onPress={() => this._openPassChange()} style={{ color: '#e68c04', }}>Change Password</Text>
                                                        </Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4 }}>
                                        <View style={{ padding: 20 }}>
                                            <View style={{ flexDirection: 'row', paddingBottom: 20, }}>
                                                <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>Name:</Text>
                                                <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}>{this.state.shwName}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', paddingBottom: 20, }}>
                                                <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>Address:</Text>
                                                <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwAddr} </Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', paddingBottom: 20, }}>
                                                <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>City:</Text>
                                                <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwCity} </Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', paddingBottom: 10, }}>
                                                <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>State:</Text>
                                                <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwState} </Text>
                                            </View>

                                            <View style={{ flexDirection: 'row-reverse', }}>
                                                <TouchableOpacity onPress={() => this._openEdit()}>
                                                    <Text style={{ color: '#e68c04', fontSize: 12, }}>
                                                        [ Edit ]
                                                </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>

                                    { /* Show Seller Information */}

                                    {
                                        (this.state.shwSellerVw)
                                            ?
                                            <View>
                                                <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4 }}>
                                                    <View style={{ paddingTop: 10, paddingLeft: 10, borderBottomColor: '#e68c04', borderBottomWidth: 1 }}>
                                                        <Text style={{ fontSize: 16, padding: 10 }}>Seller Information</Text>
                                                    </View>
                                                    <View style={{ padding: 20 }}>
                                                        <View style={{ flexDirection: 'row', paddingBottom: 20, }}>
                                                            <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>Business Name:</Text>
                                                            <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}>{this.state.shwsellerName}</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', paddingBottom: 20, }}>
                                                            <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>Address:</Text>
                                                            <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwsellerAddr} </Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', paddingBottom: 20, }}>
                                                            <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>City:</Text>
                                                            <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwsellerCity} </Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', paddingBottom: 10, }}>
                                                            <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>State:</Text>
                                                            <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwsellerState} </Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', paddingBottom: 10, }}>
                                                            <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>Contact Name:</Text>
                                                            <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwsellerCName} </Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', paddingBottom: 10, }}>
                                                            <Text style={{ fontSize: 15, flex: 0.30, color: '#999' }}>Contact Number:</Text>
                                                            <Text style={{ fontSize: 15, flex: 0.70, textAlign: 'right' }}> {this.state.shwsellerCNum} </Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row-reverse', }}>
                                                            <TouchableOpacity onPress={() => this.props.navigation.navigate('EditSeller')}>
                                                                <Text style={{ color: '#e68c04', fontSize: 12, }}>
                                                                    [ Edit ]
                                                            </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </View>

                                                <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4 }}>
                                                    <View style={{ paddingTop: 10, paddingLeft: 10, borderBottomColor: '#e68c04', borderBottomWidth: 1 }}>
                                                        <Text style={{ fontSize: 16, padding: 10 }}>Seller Document</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row-reverse', paddingTop: 5, paddingLeft: 10 }}>
                                                        <TouchableOpacity onPress={() => this._addSellerDoc()}>
                                                            <Text style={{ color: '#e68c04', fontSize: 12, }}>
                                                                [ Add New ]
                                                        </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{ padding: 20 }}>
                                                        <ScrollView horizontal >
                                                            <View style={{ flexDirection: 'row', }}>
                                                                {
                                                                    this.state.existingImages.map((y, index) => {
                                                                        return <View style={{ paddingLeft: 5 }}>
                                                                            <Image
                                                                                source={{ uri: serverURL + 'admin/uploads/seller/' + y.seller_image }}
                                                                                style={{ width: 150, height: 180, borderColor: '#ddd', borderWidth: 1 }}
                                                                            />
                                                                            <View style={{ position: 'absolute', zIndex: 1, top: -2, left: 2 }}>
                                                                                <Icon
                                                                                    name="ios-close-circle"
                                                                                    style={{ fontSize: 30, color: 'orange', }} onPress={() => Alert.alert('Warning !', 'This Image Will Be Permanently Deleted!', [{ text: 'Accept', onPress: () => this._deleteDocument(y.id) }, { text: 'Deny', onPress: () => console.log('Cancel Pressed'), style: 'cancel', }], { cancelable: true })} />
                                                                            </View>
                                                                        </View>
                                                                    })
                                                                }
                                                            </View>
                                                        </ScrollView>
                                                    </View>
                                                </View>
                                            </View>
                                            :
                                            null
                                    }


                                    { /* Show Seller Information Ends */}

                                    { /* Show Agent Document */}

                                    {
                                        (this.state.shwAgentVw)
                                            ?
                                            <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4 }}>
                                                <View style={{ paddingTop: 10, paddingLeft: 10, borderBottomColor: '#e68c04', borderBottomWidth: 1 }}>
                                                    <Text style={{ fontSize: 16, padding: 10 }}>Agent Document</Text>
                                                </View>
                                                <View style={{ padding: 20 }}>
                                                    <ScrollView horizontal >
                                                        <View style={{ flexDirection: 'row', }}>
                                                            {
                                                                this.state.shwAgentImg.map((y, index) => {
                                                                    return <View style={{ paddingLeft: 5 }} onPress={() => alert('Hello')}>
                                                                        <Image
                                                                            source={{ uri: serverURL + 'admin/uploads/agent_docs/' + y.document }}
                                                                            style={{ width: 150, height: 180, borderColor: '#ddd', borderWidth: 1 }}

                                                                        />
                                                                    </View>
                                                                })
                                                            }
                                                        </View>
                                                    </ScrollView>
                                                </View>
                                            </View>
                                            :
                                            null
                                    }


                                    { /* Show Agent Document Ends */}


                                </ScrollView>
                            )
                }



                {/* Edit Email Modal Starts  */}

                {
                    this.state.showMailModal
                        ?
                        <View>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.showMailModal}
                                onRequestClose={() => { this.setState({ showMailModal: false }); }}
                            >
                                <View style={{ zIndex: 1, backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute', width: Width, height: Height, }}>
                                    <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4, top: (Height * 5 / 100) }}>

                                        <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                            <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.setState({ showMailModal: false }); }} />
                                        </View>

                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, }}>
                                            <Text style={{ color: '#000', fontSize: 20, lineHeight: 30, }}>Update E-mail</Text>
                                        </View>

                                        <View style={{ padding: 20, borderTopColor: '#e68c04', borderTopWidth: 0.2, }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.10, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Icon type="FontAwesome" name="info-circle" style={{ color: '#4989c1', fontSize: 20 }} />
                                                </View>
                                                <View style={{ flex: 0.90 }}>
                                                    <Text style={{ fontSize: 15 }}>
                                                        Please make sure that your new email address works.
                                                                    </Text>
                                                </View>
                                            </View>
                                        </View>

                                        <View style={{ padding: 10, }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.30, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 15 }}>
                                                        Registered Email :
                                                                    </Text>
                                                </View>
                                                <View style={{ flex: 0.70 }}>
                                                    <Text style={{ fontSize: 16, letterSpacing: 2 }}>
                                                        {this.state.shwMail}
                                                    </Text>
                                                </View>
                                            </View>

                                            <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 5 }} onPress={() => this._sendOTP()}>
                                                <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>  Request OTP Verification Code </Text>
                                            </TouchableOpacity>
                                        </View>

                                        {
                                            this.state.showOTP
                                                ?
                                                <View style={{ padding: 5, }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Content>
                                                            <Form>
                                                                <Item floatingLabel style={{ width: (Width * 80 / 100) }}>
                                                                    <Label>Verification Code</Label>
                                                                    <Input
                                                                        style={{ fontSize: 20 }}
                                                                        keyboardType='phone-pad'
                                                                        autoCorrect={false}
                                                                        returnKeyType='done'
                                                                        maxLength={4}
                                                                        onSubmitEditing={this._checkOTP}
                                                                        onChangeText={userOTP => this.setState({ userOTP })}
                                                                    />
                                                                </Item>

                                                                <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                                                    <Label>New Email</Label>
                                                                    <Input
                                                                        style={{ fontSize: 20 }}
                                                                        keyboardType='email-address'
                                                                        returnKeyType='done'
                                                                        autoCorrect={false}
                                                                        ref={"txtMail"}
                                                                        onChangeText={userMail => this.setState({ userMail })}
                                                                    />
                                                                </Item>

                                                                {
                                                                    this.state.showMailText
                                                                        ?
                                                                        <View>
                                                                            <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > {this.state.emailMessage} </Text>
                                                                        </View>
                                                                        :
                                                                        null
                                                                }
                                                            </Form>
                                                        </Content>
                                                    </View>

                                                    <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15 }} onPress={() => this._updateMail()}>
                                                        <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>  Update </Text>
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                null
                                        }

                                        <View style={{ padding: 15, }}>
                                            <View style={{ flexDirection: 'column', backgroundColor: '#e8ceae', padding: 10 }}>
                                                <Text style={{ fontSize: 15, padding: 5 }}>
                                                    OTP verification code not received
                                                                </Text>
                                                <Text style={{ fontSize: 14, padding: 5 }}>
                                                    1. Your Mobile OTP verification code may take up to 10 minutes to arrive (depending on your email service provider), please do not repeat clicking.
                                                                </Text>
                                                <Text style={{ fontSize: 14, padding: 5 }}>
                                                    2. Please check if your mailbox works or if it goes to trash/spam folder or your mail inbox is full.
                                                                </Text>
                                                <Text style={{ fontSize: 14, padding: 5 }}>
                                                    3. Network anomalies may cause loss of messages, please re-submit request or try again later with different browsers or with browser cookies cleared.
                                                                </Text>
                                                <Text style={{ fontSize: 14, padding: 5 }}>
                                                    4. Check with your email operator to see if verification code email has been blocked.
                                                                </Text>
                                            </View>
                                        </View>

                                    </View>
                                </View>
                            </Modal>
                        </View>

                        :
                        null
                }

                {/* Edit Email Modal Ends  */}

                {/* Edit Password Modal Starts  */}

                {
                    this.state.showPassModal
                        ?
                        <View>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.showPassModal}
                                onRequestClose={() => { this.setState({ showPassModal: false }); }}
                            >
                                <View style={{ zIndex: 1, backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute', width: Width, height: Height, }}>
                                    <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4, top: (Height * 7 / 100) }}>

                                        <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                            <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.setState({ showPassModal: false }); }} />
                                        </View>

                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, }}>
                                            <Text style={{ color: '#000', fontSize: 20, lineHeight: 30, }}>Update Password</Text>
                                        </View>

                                        <View style={{ padding: 20, borderTopColor: '#e68c04', borderTopWidth: 0.2, }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.10, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Icon type="FontAwesome" name="info-circle" style={{ color: '#4989c1', fontSize: 20 }} />
                                                </View>
                                                <View style={{ flex: 0.90 }}>
                                                    <Text style={{ fontSize: 15 }}>
                                                        Please make sure that you will receive a 4 digit OTP code in your registered mobile to change Password.
                                                                    </Text>
                                                </View>
                                            </View>
                                        </View>

                                        <View style={{ padding: 10, }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.30, left: (Width * 2 / 100) }}>
                                                    <Text style={{ fontSize: 15, color: '#5f5b5b' }}>
                                                        Registered Email :
                                                                    </Text>
                                                </View>
                                                <View style={{ flex: 0.70 }}>
                                                    <Text style={{ fontSize: 16, letterSpacing: 2, left: (Width * 2 / 100), color: '#5f5b5b' }}>
                                                        {this.state.shwMail}
                                                    </Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.30, left: (Width * 2 / 100) }}>
                                                    <Text style={{ fontSize: 15, color: '#5f5b5b' }}>
                                                        Registered Mobile :
                                                                    </Text>
                                                </View>
                                                <View style={{ flex: 0.70 }}>
                                                    <Text style={{ fontSize: 16, letterSpacing: 2, left: (Width * 2 / 100), color: '#5f5b5b' }}>
                                                        {this.state.shwMob}
                                                    </Text>
                                                </View>
                                            </View>

                                            <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15 }} onPress={() => this._sendPOTP()}>
                                                <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>  Request OTP Verification Code </Text>
                                            </TouchableOpacity>
                                        </View>

                                        {
                                            this.state.showOTP
                                                ?
                                                <View style={{ padding: 5, }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Content>
                                                            <Form>
                                                                <Item floatingLabel style={{ width: (Width * 80 / 100) }}>
                                                                    <Label>Verification Code</Label>
                                                                    <Input
                                                                        style={{ fontSize: 20 }}
                                                                        keyboardType='phone-pad'
                                                                        autoCorrect={false}
                                                                        returnKeyType='done'
                                                                        maxLength={4}
                                                                        onSubmitEditing={this._checkOTP}
                                                                        onChangeText={userOTP => this.setState({ userOTP })}
                                                                    />
                                                                </Item>

                                                                {
                                                                    this.state.showOTPText
                                                                        ?
                                                                        <View>
                                                                            <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > OTP is Required </Text>
                                                                        </View>
                                                                        :
                                                                        null
                                                                }

                                                                <Item floatingLabel style={{ width: (Width * 80 / 100) }}>
                                                                    <Label>Old Password</Label>
                                                                    <Input
                                                                        style={{ fontSize: 20 }}
                                                                        returnKeyType='done'
                                                                        autoCorrect={false}
                                                                        secureTextEntry={this.state.showOldPass}
                                                                        onChangeText={userOldPass => this.setState({ userOldPass })}
                                                                    />
                                                                    <Icon active name={this.state.iconOld} onPress={this._showOldPassword} />
                                                                </Item>

                                                                {
                                                                    this.state.showOldPassText
                                                                        ?
                                                                        <View>
                                                                            <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > Old Password is Required </Text>
                                                                        </View>
                                                                        :
                                                                        null
                                                                }

                                                                <Item floatingLabel style={{ width: (Width * 80 / 100) }}>
                                                                    <Label>New Password</Label>
                                                                    <Input
                                                                        style={{ fontSize: 20 }}
                                                                        returnKeyType='next'
                                                                        autoCorrect={false}
                                                                        ref={"txtPass"}
                                                                        secureTextEntry={this.state.showPass}
                                                                        onKeyPress={() => this._checkPass()}
                                                                        onChangeText={userPass => this.setState({ userPass })}
                                                                    />
                                                                    <Icon active name={this.state.icon} onPress={this._showPassword} />
                                                                </Item>

                                                                {
                                                                    this.state.showPassText
                                                                        ?
                                                                        <View>
                                                                            <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > Password is Required </Text>
                                                                        </View>
                                                                        :
                                                                        null
                                                                }

                                                                <Item floatingLabel style={{ width: (Width * 80 / 100) }}>
                                                                    <Label>Confirm Password</Label>
                                                                    <Input
                                                                        style={{ fontSize: 20 }}
                                                                        returnKeyType='done'
                                                                        autoCorrect={false}
                                                                        secureTextEntry={this.state.showConfPass}
                                                                        ref={"txtPassConf"}
                                                                        onKeyPress={() => this._checkPass()}
                                                                        onChangeText={userPassCheck => this.setState({ userPassCheck })}
                                                                    />
                                                                    <Icon active name={this.state.iconCheck} onPress={this._showConfPassword} />
                                                                </Item>

                                                                {
                                                                    this.state.showCheckPassText
                                                                        ?
                                                                        <View>
                                                                            <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > New Password and Confirm Password Mismatched!!</Text>
                                                                        </View>
                                                                        :
                                                                        null
                                                                }
                                                            </Form>
                                                        </Content>
                                                    </View>

                                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                        <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15, width: (Width * 40 / 100), }} onPress={() => this._updateMail()}>
                                                            <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>  Update </Text>
                                                        </TouchableOpacity>
                                                    </View>

                                                </View>
                                                :
                                                null
                                        }

                                    </View>
                                </View>
                            </Modal>
                        </View>

                        :
                        null
                }

                {/* Edit Password Modal Ends  */}

                {/* Edit Profile Pic Modal Starts  */}

                {
                    this.state.showProPicModal
                        ?
                        <View>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.showProPicModal}
                                onRequestClose={() => { this.setState({ showProPicModal: false }); }}
                            >
                                <View style={{ zIndex: 1, backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute', width: Width, height: Height, }}>
                                    <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4, top: (Height * 4 / 100) }}>

                                        <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                            <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.setState({ showProPicModal: false }); }} />
                                        </View>

                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, }}>
                                            <Text style={{ color: '#000', fontSize: 18, lineHeight: 30, }}>Update Profile Picture</Text>
                                        </View>

                                        <View style={{ padding: 20, borderTopColor: '#e68c04', borderTopWidth: 0.2, }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.10, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Icon type="FontAwesome" name="info-circle" style={{ color: '#4989c1', fontSize: 19 }} />
                                                </View>
                                                <View style={{ flex: 0.90 }}>
                                                    <Text style={{ fontSize: 15 }}>
                                                        Your Photo will be displayed within 24 hours.
                                                                    </Text>
                                                </View>
                                            </View>
                                        </View>

                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ padding: 10, borderColor: '#e68c04', borderWidth: 1, borderRadius: 100, }}>

                                                {
                                                    (this.state.shwImg)
                                                        ?
                                                        (this.state.shwPic == null)
                                                            ?
                                                            <Image
                                                                source={require('../assets/images/user.png')}
                                                                style={{ width: 150, height: 150, borderRadius: 100 }}
                                                            />
                                                            :
                                                            <Image
                                                                style={{ width: 150, height: 150, borderRadius: 100 }}
                                                                source={{ uri: serverURL + 'admin/uploads/bussiness_logo/' + this.state.shwPic }}
                                                            />
                                                        :
                                                        null
                                                }

                                                {
                                                    avatarSource && (
                                                        <Image
                                                            source={this.state.avatarSource}
                                                            style={{ width: 150, height: 150, borderRadius: 100 }}
                                                        />
                                                    )
                                                }

                                            </View>

                                        </View>

                                        <View style={{ paddingTop: 10, paddingBottom: 20, justifyContent: 'center', alignItems: 'center' }}>
                                            <Button iconLeft transparent style={{ width: 150, height: 25, }} onPress={() => this._saveProPic()} >
                                                <Icon name='ios-attach' style={{ color: '#000', }} />
                                                <Text style={{ color: '#000', fontSize: 14, }}> Upload New Picture </Text>
                                            </Button>

                                            <Text style={{ fontSize: 10, color: '#E74C3C', paddingLeft: (Width * 2 / 100), paddingTop: 5 }}>[ Only jpg/png Allowed. File Should Be Less Than 3 MB. ]</Text>

                                            {
                                                (this.state.shwImgButt)
                                                    ?
                                                    <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15, width: (Width * 90 / 100) }} onPress={() => this._changeProPic()}>
                                                        <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>  Save As Profile Picture </Text>
                                                    </TouchableOpacity>

                                                    :
                                                    null
                                            }

                                        </View>

                                        <View style={{ padding: 15, paddingTop: 20 }}>

                                            <View style={{ flexDirection: 'column', padding: 15, borderWidth: 1, borderColor: '#8a8a8a', }}>

                                                <ScrollView>
                                                    <View>
                                                        <Text style={{ fontSize: 18, backgroundColor: '#FFF' }}>    Uploading Rules  :   </Text>
                                                        <Text style={{ fontSize: 16, padding: 5 }}>
                                                            1. Please upload a photo that matches the gender, age and status details in your personal profile.
                                                                    </Text>
                                                        <Text style={{ fontSize: 16, padding: 5 }}>
                                                            2. Use a photo is appropriate for a businesssetting. Group photos are not allowed.
                                                                    </Text>
                                                        <Text style={{ fontSize: 16, padding: 5 }}>
                                                            3. Photos violating the rules stated in the Terms of User will be removed without notice.
                                                                    </Text>
                                                    </View>
                                                </ScrollView>

                                            </View>
                                        </View>

                                    </View>
                                </View>
                            </Modal>
                        </View>

                        :
                        null
                }

                {/* Edit Profile Pic Modal Ends  */}

                {/* Verify Email Modal Starts  */}

                {
                    this.state.verifyEmailModal
                        ?
                        <View>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.verifyEmailModal}
                                onRequestClose={() => { this.setState({ verifyEmailModal: false }); }}
                            >
                                <View style={{ zIndex: 1, backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute', width: Width, height: Height, }}>
                                    <View style={{ backgroundColor: 'rgb(255, 255, 255)', width: (Width * 95 / 100), margin: 10, borderRadius: 4, top: (Height * 10 / 100) }}>

                                        <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                            <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.setState({ verifyEmailModal: false }); }} />
                                        </View>

                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, borderBottomColor: '#e68c04', borderBottomWidth: 0.2, }}>
                                            <Text style={{ color: '#000', fontSize: 20, lineHeight: 30, }}>Verify  E-mail</Text>
                                        </View>

                                        {
                                            (this.state.info)
                                                ?
                                                <View style={{ padding: 20, }}>
                                                    <View style={{ flexDirection: 'row', }}>
                                                        <View style={{ flex: 0.10, justifyContent: 'center', alignItems: 'center' }}>
                                                            <Icon type="FontAwesome" name="info-circle" style={{ color: '#5cb85c', fontSize: 20 }} />
                                                        </View>
                                                        <View style={{ flex: 0.90 }}>
                                                            <Text style={{ fontSize: 15 }}>
                                                                4 digit otp has been sent to your registered email address. Please paste the otp below to complete verification process.
                                                                            </Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                :
                                                null
                                        }


                                        <View style={{ padding: 10, }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 0.30, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 15 }}>
                                                        Registered Email :
                                                                    </Text>
                                                </View>
                                                <View style={{ flex: 0.70 }}>
                                                    <Text style={{ fontSize: 16, color: '#e68c04' }}>
                                                        {this.state.shwMail}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>

                                        {
                                            this.state.showVEmailOTP
                                                ?
                                                <View style={{ padding: 10, }}>
                                                    <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                                        <Label>Enter OTP From Your E-mail</Label>
                                                        <Input
                                                            style={{ fontSize: 20 }}
                                                            keyboardType='phone-pad'
                                                            autoCorrect={false}
                                                            returnKeyType='done'
                                                            maxLength={4}
                                                            onSubmitEditing={this._checkVEmailOTP}
                                                            onChangeText={vEmailOTP => this.setState({ vEmailOTP })}
                                                        />
                                                    </Item>

                                                    <Text style={{ paddingTop: 10, textAlign: 'right', color: '#ffa500', right: 25, }} onPress={this._resendVEmailOTP}>Resend OTP</Text>

                                                    {
                                                        this.state.showOTPText
                                                            ?
                                                            <View>
                                                                <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > {this.state.otpMessage} </Text>
                                                            </View>
                                                            :
                                                            null
                                                    }

                                                    {
                                                        this.state.showLoader
                                                            ?
                                                            <View style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15 }}>
                                                                <ActivityIndicator size="small" color="#FFF" />
                                                            </View>
                                                            :
                                                            <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15 }} onPress={() => this._verifyMail()}>
                                                                <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>  Verify </Text>
                                                            </TouchableOpacity>
                                                    }

                                                </View>
                                                :
                                                null
                                        }



                                    </View>
                                </View>
                            </Modal>
                        </View>

                        :
                        null
                }

                {/* Verify Email Modal Ends  */}

            </View>
        )
    }
}