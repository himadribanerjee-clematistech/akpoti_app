import React from 'react';
import { YellowBox, SafeAreaView, View, Text, Dimensions, TouchableOpacity, ActivityIndicator, Image, FlatList, Alert } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Button, Container, Content, Card, CardItem, Body, Right, Left, } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
const regex = /(<([^>]+)>)/ig;

export default class MyItems extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      serverData: [],
      pageno: 1,
      tot_record: 0,
      slider1ActiveSlide: 1,
      active: false,
      lastRefresh: Date(Date.now()).toString(),
      serverError: false,
      connectState: true,
      showLoginButton: false,
      errorMsg: 'Something went wrong. Please try later.',
      profileType: 'seller',

    }
    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentWillUnmount() {
    // Remove the event listener before removing the screen from the stack
    this.focusListener.remove();
  }

  componentDidMount() {
    const { navigation } = this.props;
    //Adding an event listner on focus
    //So whenever the screen will have focus it will set the state to zero
    this.focusListener = navigation.addListener('didFocus', () => {
      //console.log('page reloads');
      if (global_isLoggedIn) {
        if (global_is_seller == 0) {
          this.setState({
            profileType: 'buyer',
            errorMsg: 'Upgrade to seller to view this section',
            loading: false

          });
        } else {
          this.forceUpdateHandler();
        }
      } else {
        this.setState({
          errorMsg: 'Please login to view this section.',
          showLoginButton: true

        });
      }

    });

  }

  forceUpdateHandler() {
    this.setState({
      loading: true,
      serverData: [],
      pageno: 1,
      tot_record: 0,
      slider1ActiveSlide: 1,
      active: false,
      lastRefresh: Date(Date.now()).toString(),
      serverError: false,
      showLoginButton: false,
      errorMsg: 'Network connection occured. Please try later.',
      profileType: 'seller',

    }, () => { this.getMyItemsData() })
  };

  loadPagination() {
    return (
      //Footer View with Load More button

      (this.state.pageno > 0) ?

        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
          <Button style={{ padding: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.loadMorePaginationData()}>
            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Load More</Text>
          </Button>

        </View>
        : null
    );
  }

  loadMorePaginationData() {
    this.getMyItemsData();
    //console.log('AA');
  }


  getMyItemsData = async () => {
    if (global_isLoggedIn) {
      const savedToken = await AsyncStorage.getItem('@token');

      const url = serverURL + "api/seller/getSellerDetails";


      var data = {
        pageno: this.state.pageno,
      };
      //console.log(url,data)
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': savedToken,
          'credentials': 'same-origin'
        },
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          //console.log(res);
          var data = res.result;
          //alert(res.status)
          if (res.status === 200) {

            if (this.state.serverData && this.state.serverData.length) {
              var serverDataPagination = this.state.serverData.concat(res.result.seller_item);

              this.setState({
                serverData: serverDataPagination,
                loading: false,
                pageno: res.result.next,
                tot_record: res.result.tot_record,
                serverError: false,

              });
            } else {
              this.setState({
                serverData: res.result.seller_item,
                loading: false,
                pageno: res.result.next,
                tot_record: res.result.tot_record,
                serverError: false,

              });
            }

          } else {
            this.setState({
              serverError: true,
              errorMsg: res.msg

            });
          }
          //console.log(this.state.serverData);    
        })
        .catch(error => {
          // console.log("aa",JSON.stringify(error))
        })
    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }
  }

  _renderProductImage({ item, index }) {
    return (

      <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#eff0f1', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center' }} >
        <Image style={{ width: '100%', height: '100%', resizeMode: 'contain', borderRadius: 10, opacity: 1, backgroundColor: '#eff0f1', }} source={{ uri: 'http://agro.clematistech.com/admin/uploads/selleritem/' + item.image }} />
        <View>

        </View>
      </View>

    );
  }

  deleteSellerItem = async (itemId) => {

    let savedToken = await AsyncStorage.getItem('@token');
    let url = serverURL + "api/seller/deleteSellerItem";
    var data = {
      id: itemId,
      customer_id: global_LoggedInCustomerId,
      seller_id: global_LoggedInSellerId
    };
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res);
        var data = res.result;
        //alert(res.status)
        if (res.status === 200) {
          Alert.alert(
            'Success !',
            'Seller Item  successfully deleted', [
            { text: 'OK', onPress: () => this.forceUpdateHandler(), style: 'cancel' },],
            { cancelable: false })

        } else {
          this.setState({
            serverError: true,
            errorMsg: res.msg

          });
        }

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }




  render() {

    const serverData = Object.values(this.state.serverData);
    const { slider1ActiveSlide } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 0.10 }} >
              <View style={style.styles.categoriesHeader}>
                <View style={style.styles.signinHeaderContainer}>
                  <View style={style.styles.categoriesHeaderContainerLeft}>
                    <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                  </View>
                  <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>My Items</Text></View>
                  <View style={style.styles.productListHeaderContainerRight}></View>
                  <View style={style.styles.productListHeaderContainerRighttwo}>


                  </View>
                </View>
              </View>
            </View>
            {
              (this.state.serverError) ? (
                <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
              ) :
                (!this.state.connectState) ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                  </View>
                ) :
                  (this.state.showLoginButton) ? (
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                      <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                      <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                        <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                          <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                        </Button>
                      </View>
                    </View>

                  ) :
                    (!this.state.loading && this.state.serverData.length == 0 && this.state.profileType == 'buyer') ? (
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#eb5234', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                          <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('UpgradeSeller')}>
                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Upgrade To Seller</Text>
                          </Button>
                        </View>
                      </View>) :
                      (!this.state.loading && this.state.serverData.length == 0 && this.state.profileType != 'buyer') ?
                        (<View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}>
                          <Text style={{ fontSize: 24, color: '#a94442' }}>No Data Found!!</Text>

                          <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 30 }}>
                            <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('AddSellerItem')}>
                              <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Add New Item</Text>
                            </Button>
                          </View>

                        </View>
                        ) :
                        this.state.loading ? (
                          <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                        ) :

                          <View style={{ flex: 0.80 }} >
                            <Content>
                              <Card>
                                <CardItem header bordered>
                                  <Left>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddSellerItem')} >
                                      <Text style={{ backgroundColor: '#0ba12e', color: '#fff', fontSize: 15, fontWeight: 'bold', borderWidth: 1, borderColor: '#ddd', paddingTop: 5, paddingBottom: 5, paddingLeft: 5, paddingRight: 5 }}>Add New Item</Text>
                                    </TouchableOpacity>
                                  </Left>
                                  <Right><Text>Total {this.state.tot_record} item found.</Text></Right>
                                </CardItem>
                              </Card>
                              <FlatList
                                data={serverData}
                                renderItem={({ item }) =>
                                  <Card style={{ flex: 0 }}>

                                    <CardItem>
                                      <Body>
                                        <View style={{ flex: 1, flexDirection: 'column' }}>
                                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { item: item })} >
                                            <Text numberOfLines={1} style={{ color: '#ff8308', textAlign: 'center', fontSize: 17, fontWeight: 'bold', paddingTop: 8 }}>{`${item.item_name}`}<Text style={{ color: '#7bb244' }}> in </Text>{`${item.species_name}`}</Text>
                                          </TouchableOpacity>
                                        </View>
                                      </Body>
                                    </CardItem>
                                    <CardItem>
                                      <Body>
                                        {
                                          (item.seller_item_images.length > 0) ?

                                            <Carousel
                                              ref={(c) => { this._carousel = c; }}
                                              data={item.seller_item_images}
                                              loop={true}
                                              hasParallaxImages={true}
                                              renderItem={this._renderProductImage.bind(this)}
                                              sliderWidth={BannerWidth - 25}
                                              itemWidth={BannerWidth - 25}
                                              onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                            />
                                            :
                                            (item.item_image != null) ?
                                              <Image source={{ uri: serverURL + 'admin/uploads/item/' + item.item_image }} style={{ height: 200, width: 320, flex: 1 }} />
                                              :
                                              <Image source={{ uri: serverURL + 'admin/uploads/category/' + item.category_image }} style={{ height: 200, width: 320, flex: 1 }} />
                                        }
                                        <Text numberOfLines={3} style={{ textAlign: 'left', fontSize: 15, color: '#666', paddingTop: 10, flex: 1 }}>
                                          {`${item.item_description}`}
                                        </Text>
                                      </Body>
                                    </CardItem>
                                    <CardItem footer bordered>
                                      <Left>
                                        <Button transparent style={{ width: 60 }} onPress={() => this.props.navigation.navigate('ProductDetails', { productDetailsId: item.id })}>
                                          <Icon active name="ios-eye" style={{ fontSize: 25, color: '#2691bf' }} />
                                          <Text style={{ paddingLeft: 4, fontSize: 15, color: '#2691bf' }}>View</Text>
                                        </Button>
                                      </Left>
                                      <Body>
                                        <Button transparent style={{ width: 60, marginLeft: 10 }} onPress={() => this.props.navigation.navigate('EditSellerItem', { productDetailsId: item.id })}>
                                          <Icon active name="ios-create" style={{ fontSize: 25, color: '#26bf4f' }} />
                                          <Text style={{ paddingLeft: 1, fontSize: 15, color: '#26bf4f' }}>Edit</Text>
                                        </Button>
                                      </Body>
                                      <Right>
                                        <Button transparent style={{ width: 60 }} onPress={() => Alert.alert(
                                          'Oops !',
                                          'This item will be deleted!', [
                                          { text: 'OK', onPress: () => this.deleteSellerItem(item.id), style: 'cancel' },],
                                          { cancelable: true })}>
                                          <Icon active name="ios-trash" style={{ fontSize: 25, color: '#ff0000' }} />
                                          <Text style={{ paddingLeft: 1, fontSize: 15, color: '#ff0000' }}>Delete</Text>
                                        </Button>
                                      </Right>
                                    </CardItem>

                                  </Card>
                                }
                                keyExtractor={item => item.id}
                              />
                              {
                                this.loadPagination()
                              }
                            </Content>
                          </View>
            }

            {
              this.state.loading ? (
                <Text style={{ color: 'white' }}></Text>
              ) :
                <View style={{ flex: 0.10, marginBottom: 0, paddingBottom: 0 }} >
                  <Footer style={{ height: '100%' }} >
                    <FooterTab style={{ backgroundColor: '#000' }}>
                      <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                        <Icon name="ios-home" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Home</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                        <Icon name="ios-person" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Myakpoti</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                        <Icon name="ios-list" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Categories</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                        <Icon name="ios-cog" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Services</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                        <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Messenger</Text>
                      </Button>
                    </FooterTab>
                  </Footer>
                </View>
            }
          </View>
        </Container>
      </SafeAreaView>
    );
  }

}
