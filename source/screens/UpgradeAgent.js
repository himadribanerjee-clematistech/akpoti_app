import React from 'react';
import { YellowBox, View, Text, StatusBar, Dimensions, TouchableOpacity, Alert, KeyboardAvoidingView, Image, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Content, Form, Button } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;
const createFormData = (photo) => {
    const data = new FormData();

    photo.forEach((item, i) => {
        data.append("document[]", {
            uri: Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
            type: item.type,
            name: item.fileName || `filename${i}.jpg`,
            size: item.fileSize
        });
    });

    return data;
};

export default class UpgradeAgent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showImageText: false,
            imageText: null,
            serverError: false,
            connectState: true,
            errorMsg: 'Something went wrong. Please try later.',
            selectedImages: [],
        }
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };


    handleChoosePhoto = () => {
        const options = {
            noData: true,
            quality: 0.3
        }

        ImagePicker.launchImageLibrary(options, response => {
            var fileSizeInMB = parseInt(Math.floor(Math.log(response.fileSize) / Math.log(1024)));
            if (fileSizeInMB > 2) {
                Alert.alert(
                    'Oops !',
                    'Image size can\'t be greater than 2 MB!', [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                    { cancelable: false }
                )
            }
            else {
                if (response.uri) {
                    var selectedImg = [];
                    this.state.selectedImages.map((y) => {
                        selectedImg.push(y);
                    });

                    selectedImg.push(response);
                    this.setState({ selectedImages: selectedImg })
                }
            }
        })
    }

    delImg(ind) {
        var selectedImg = [];
        this.state.selectedImages.map((y, index) => {
            if (index == ind) {
                // Skip img
            } else {
                selectedImg.push(y);
            }

        })
        this.setState({ selectedImages: selectedImg })
    }

    _apply = async () => {
        let savedToken = await AsyncStorage.getItem('@token');

        let BDocument = this.state.selectedImages;

        if (BDocument == []) {
            this.setState({
                showImageText: true,
                imageText: 'Document Should Not Be Blank'
            })
        }
        else {
            fetch(serverURL + 'api/agentRequest', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': savedToken,
                    'credentials': 'same-origin'
                },
                timeout: 0,
                compress: true,
                body: createFormData(this.state.selectedImages)
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);
                    if (responseJson[0] == 200) {
                        if (responseJson[1].msg !== '') {
                            Alert.alert('Success Message !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                        else {
                            Alert.alert('Success Message with Error !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                    }
                    else {
                        Alert.alert('Error Message !!', responseJson[1].msg);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }


    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#F7F9F9' }}>
                {
                    (!this.state.connectState) ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                        </View>
                    ) : null
                }
                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <View style={{ width: Width, height: (Height * 12 / 100), backgroundColor: '#FFF', borderColor: '#ddd', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 3, elevation: 5, }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <Icon name="ios-arrow-back" style={{ fontSize: 30, color: '#000', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.9, justifyContent: 'center', alignItems: 'center', paddingTop: 40, }}>
                            <Text style={{ fontSize: 20, }}> Upgrade To Agent</Text>
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingBottom: 10, }}>
                    <KeyboardAvoidingView style={{ flex: 1, top: (Height * 2 / 100), justifyContent: 'center', alignItems: 'center', marginBottom: 20, }} behavior="padding" enabled>
                        <Content>
                            <Form style={{ width: (Width * 95 / 100), }}>

                                <View style={{ width: (Width * 50 / 100), paddingTop: 15, flexDirection: 'row', }} >
                                    <Button iconLeft transparent style={{ width: 230, height: 30, }} onPress={() => this.handleChoosePhoto()}>
                                        <Icon name='ios-attach' style={{ color: '#000' }} />
                                        <Text style={{ color: '#000', textAlign: 'center', fontSize: 14, }}> Choose Document To Be A Seller  </Text>
                                    </Button>
                                </View>

                                <View style={{ width: (Width * 50 / 100), paddingTop: 15, flexDirection: 'row', paddingLeft: 10 }} >
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 12, color: '#E74C3C', paddingLeft: (Width * 2 / 100), }}>[ Only jpg/png allowed ]</Text>
                                    </View>
                                </View>

                                <ScrollView horizontal >
                                    <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10 }}>
                                        {
                                            this.state.selectedImages.map((y, index) => {
                                                return <View style={{ paddingLeft: 5 }}>
                                                    <Image
                                                        source={{ uri: y.uri }}
                                                        style={{ width: 150, height: 150 }}
                                                    />
                                                    <View style={{ position: 'absolute', zIndex: 1, top: -10, left: 2 }}>
                                                        <Icon name="ios-close-circle" style={{ fontSize: 30, color: 'orange', }} onPress={() => this.delImg(index)} />
                                                    </View>
                                                </View>
                                            })
                                        }
                                    </View>
                                </ScrollView>


                                {
                                    this.state.showImageText
                                        ?
                                        <View style={{ marginBottom: 10, marginTop: 5 }}>
                                            <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.imageText} </Text>
                                        </View>
                                        :
                                        null
                                }

                                <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15 }} onPress={() => this._apply()}>
                                    <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}> Apply For Agent </Text>
                                </TouchableOpacity>

                            </Form>
                        </Content>
                    </KeyboardAvoidingView>
                </View>

            </View>
        )
    }
}