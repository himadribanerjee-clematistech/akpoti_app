import React from 'react';
import { View, Text, StatusBar, Dimensions, TouchableOpacity, ActivityIndicator, YellowBox, Image } from 'react-native';
import { Content, Form, Item, Input, Label } from 'native-base';
import { Icon } from 'native-base';
import * as style from '../config';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showLoader: false,
            showContact: true,
            showOTP: false,
            showForm: false,
            showPass: true,
            showConfPass: true,
            showRegister: false,

            showNoText: false,
            numberMessage: '',
            emailMessage: '',
            showRegMsg: false,
            showName: '',

            showNameText: false,
            showMailText: false,
            showPassText: false,
            showCheckPassText: false,

            icon: 'eye-off',
            iconCheck: 'eye-off',

            userNumber: '',
            userOTP: '',
            passWord: '',
            userName: '',
            userMail: '',
            userPass: '',
            userPassCheck: '',
            connectState: true,
        }
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };

    back = () => {
        this.props.navigation.navigate('Home');
    }

    _sendOTP = () => {
        this.setState({ showLoader: true });

        fetch(serverURL + 'api/login/doSendOTP', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                phone: this.state.userNumber
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status == 200) {
                    this.setState({
                        showNoText: false,
                    });

                    this.setState({ showLoader: false });
                    this.setState({ showOTP: true });
                }
                else {
                    this.setState({ showLoader: false });
                    this.setState({
                        showNoText: true,
                        numberMessage: responseJson.message
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _checkOTP = () => {
        this.setState({ showLoader: true });

        fetch(serverURL + 'api/login/doMobileOTPCheck', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                phone: this.state.userNumber,
                OTP: this.state.userOTP
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status == 200) {
                    //AsyncStorage.setItem('mobileNo', this.state.userNumber);
                    this.setState({ showLoader: false });
                    this.setState({ showOTP: false });
                    this.setState({ showContact: false });
                    this.setState({ showForm: true });
                }
                else {
                    this.setState({ showLoader: false });
                    this.setState({ showContact: true });
                    console.log(JSON.stringify(responseJson));
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _showPassword = () => {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            showPass: !prevState.showPass
        }))
    }

    _showConfPassword = () => {
        this.setState(prevState => ({
            iconCheck: prevState.iconCheck === 'eye' ? 'eye-off' : 'eye',
            showConfPass: !prevState.showConfPass
        }))
    }

    _checkPass = () => {
        var Pass = this.state.userPass;
        var checkPass = this.state.userPassCheck;
        if (this.state.userPass != '' && this.state.userPassCheck != '') {
            if (Pass === checkPass) {
                this.setState({
                    showCheckPassText: false,
                    showRegister: true
                });
            }
            else {
                this.setState({
                    showCheckPassText: true,
                    showRegister: false
                });
            }
        }

    }

    _register = () => {
        this.setState({ showLoader: true });

        let Name = this.state.userName;
        let Mail = this.state.userMail;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (Name == '') {
            this.setState({
                showLoader: false,
                showNameText: true,
                showRegister: true
            });
        }
        else if (Mail == '') {
            this.setState({
                showNameText: false,
                showLoader: false,
                showMailText: true,
                showRegister: true,
                emailMessage: 'E-mail is Required',
            });
        }
        else if (reg.test(Mail) === false) {
            this.setState({
                showLoader: false,
                showMailText: true,
                showRegister: true,
                emailMessage: 'E-mail Format will be like example@example.com',
            });
        }
        else {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        showNameText: false,
                        showMailText: false,
                    });

                    fetch(serverURL + 'api/login/doregister', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            phone: this.state.userNumber,
                            name: this.state.userName,
                            email: this.state.userMail,
                            password: this.state.userPass,
                            confirmpassword: this.state.userPassCheck
                        }),
                    })
                        .then(response => {
                            const status = response.status;
                            const data = response.json();

                            return Promise.all([status, data]);
                        })
                        .then((responseJson) => {
                            if (responseJson[0] == 200) {
                                this.setState({
                                    showLoader: false,
                                    showMailText: false,
                                    showRegister: false,
                                    showForm: false,
                                    showRegMsg: true,
                                    showName: this.state.userName,
                                });

                                setTimeout(() => {
                                    this.props.navigation.navigate('Signin');
                                }, 1000);
                            }
                            else {
                                if ((responseJson[1].field) == 'email') {
                                    this.setState({
                                        showLoader: false,
                                        showMailText: true,
                                        showRegister: true,
                                        emailMessage: responseJson[1].msg,
                                    });
                                }
                                else {
                                    alert(responseJson[1].msg);
                                }
                            }
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                } else {
                    Alert.alert(
                        'Oops !',
                        'Seems like you don\'t have an active internet connection!', [
                        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                        { cancelable: false })
                }
            });

        }
    }

    render() {
        return (
            <View style={style.styles.container}>

                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <View style={style.styles.signinHeader}>
                    <View style={style.styles.signinHeaderContainer}>
                        <View style={style.styles.signinHeaderContainerLeft}>
                            <TouchableOpacity onPress={this.back}>
                                <Icon name="ios-close" style={{ fontSize: 50, color: '#000', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={style.styles.signinHeaderContainerRight}>
                            <Text style={style.styles.signinHeaderContainerRightText}> Register Yourself </Text>
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, top: (Height * 2 / 100), }}>

                    {
                        (!this.state.connectState) ? (
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                            </View>
                        ) :
                            this.state.showLoader
                                ?
                                (
                                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                                )
                                :
                                null
                    }


                    <Content>
                        <Form style={{ height: Height, }}>

                            {
                                this.state.showContact
                                    ?
                                    <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                        <Label>Contact Number</Label>
                                        <Input
                                            style={{ fontSize: 20 }}
                                            keyboardType='phone-pad'
                                            autoCorrect={false}
                                            returnKeyType='done'
                                            onSubmitEditing={this._sendOTP}
                                            onChangeText={userNumber => this.setState({ userNumber })}
                                        />
                                    </Item>
                                    :
                                    null
                            }

                            {
                                this.state.showNoText
                                    ?
                                    <View>
                                        <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > {this.state.numberMessage} !!</Text>
                                    </View>
                                    :
                                    null
                            }

                            {
                                this.state.showOTP
                                    ?
                                    <View>
                                        <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                            <Label>OTP</Label>
                                            <Input
                                                style={{ fontSize: 20 }}
                                                keyboardType='phone-pad'
                                                autoCorrect={false}
                                                returnKeyType='done'
                                                maxLength={4}
                                                onSubmitEditing={this._checkOTP}
                                                onChangeText={userOTP => this.setState({ userOTP })}
                                            />
                                        </Item>

                                        <Text style={{ paddingTop: 10, textAlign: 'right', color: '#ffa500', right: 25 }} onPress={this._sendOTP}>Resend OTP</Text>
                                    </View>
                                    :
                                    null
                            }

                            {
                                this.state.showForm
                                    ?
                                    <View>
                                        <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                            <Label>Full Name</Label>
                                            <Input
                                                style={{ fontSize: 20 }}
                                                returnKeyType='next'
                                                autoCorrect={false}
                                                ref={"txtName"}
                                                //onSubmitEditing={() => this.refs.txtMail.focus()}
                                                onChangeText={userName => this.setState({ userName })}
                                            />
                                        </Item>

                                        {
                                            this.state.showNameText
                                                ?
                                                <View>
                                                    <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > Name is Required !!</Text>
                                                </View>
                                                :
                                                null
                                        }

                                        <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                            <Label>E-mail Address</Label>
                                            <Input
                                                style={{ fontSize: 20 }}
                                                keyboardType='email-address'
                                                returnKeyType='next'
                                                autoCorrect={false}
                                                ref={"txtMail"}
                                                //onSubmitEditing={() => this.refs.txtPass.focus()}
                                                onChangeText={userMail => this.setState({ userMail })}
                                            />
                                        </Item>

                                        {
                                            this.state.showMailText
                                                ?
                                                <View>
                                                    <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > {this.state.emailMessage} </Text>
                                                </View>
                                                :
                                                null
                                        }

                                        <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                            <Label>New Password</Label>
                                            <Input
                                                style={{ fontSize: 20 }}
                                                returnKeyType='next'
                                                autoCorrect={false}
                                                ref={"txtPass"}
                                                secureTextEntry={this.state.showPass}
                                                //onSubmitEditing={() => this.refs.txtPassConf.focus()}
                                                onChangeText={userPass => this.setState({ userPass }, () => this._checkPass())}
                                            />
                                            <Icon active name={this.state.icon} onPress={this._showPassword} />
                                        </Item>

                                        {
                                            this.state.showPassText
                                                ?
                                                <View>
                                                    <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > Password is Required </Text>
                                                </View>
                                                :
                                                null
                                        }

                                        <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                            <Label>Confirm Password</Label>
                                            <Input
                                                style={{ fontSize: 20 }}
                                                returnKeyType='done'
                                                autoCorrect={false}
                                                secureTextEntry={this.state.showConfPass}
                                                ref={"txtPassConf"}
                                                //onSubmitEditing={() => this._checkPass() }
                                                onChangeText={userPassCheck => this.setState({ userPassCheck }, () => this._checkPass())}
                                            />
                                            <Icon active name={this.state.iconCheck} onPress={this._showConfPassword} />
                                        </Item>

                                        {
                                            this.state.showCheckPassText
                                                ?
                                                <View>
                                                    <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > New Password and Confirm Password Mismatched!!</Text>
                                                </View>
                                                :
                                                null
                                        }

                                    </View>
                                    :
                                    null
                            }

                            {
                                this.state.showRegister
                                    ?
                                    <View style={{ justifyContent: 'center', alignItems: 'center', top: 40 }}>
                                        <TouchableOpacity onPress={this._register} style={{ height: 40, width: (Width * 90 / 100), borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, }}>
                                            <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>Register</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    null
                            }

                            {
                                this.state.showRegMsg
                                    ?
                                    <View style={{ justifyContent: 'center', alignItems: 'center', top: 40, backgroundColor: '#5cb85c', height: (Height * 20 / 100), margin: (Width * 5 / 100), borderRadius: 10 }}>
                                        <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>Hello <Text style={{ fontSize: 22, }}>{this.state.showName}</Text> . </Text>

                                        <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>Thank You For Joining with Our Venture! </Text>

                                        <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>Welcome To Akpoti Family. Hope You Will Love Akpoti.</Text>
                                    </View>
                                    :
                                    null
                            }

                        </Form>

                    </Content>
                </View>


            </View>
        )
    }
}