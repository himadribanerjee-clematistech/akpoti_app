import React from 'react';
import { YellowBox, View, Text, StyleSheet, ActivityIndicator, Alert } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Footer, FooterTab, Container, Content, Button, Input, Textarea, CheckBox, } from 'native-base';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

export default class ContactSupplier extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      from_id: null,
      to_id: null,
      seller_item_id: null,
      quantity: null,
      message: null,
      uom_id: null,
      type: 'item',

      isSubmited: false,
      checkboxRecommend: false,
      checkboxTerms: false,
      validForm: true,

      serverError: false,
      connectState: true,
      showLoginButton: false,
      errorMsg: 'Something went wrong. Please try later.',
    }

    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {
    //alert(this.props.navigation.getParam('sellerCustomerName', 'Not specified'));
    if (global_isLoggedIn) {
      this.setState({
        errorMsg: '',
        showLoginButton: false

      });
    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }
    this.setState({
      to_id: this.props.navigation.getParam('sellerCustomerId', null),
      //from_id: AsyncStorage.getItem('@customer_id'),
      seller_item_id: this.props.navigation.getParam('sellerItemId', null),
      uom_id: this.props.navigation.getParam('uomId', null),

    });
  }

  toggleRecommend() {
    this.setState({ checkboxRecommend: !this.state.checkboxRecommend });
  }

  toggleTerms() {
    this.setState({ checkboxTerms: !this.state.checkboxTerms });
  }

  submitForm = async () => {
    if (global_isLoggedIn) {

      const savedToken = await AsyncStorage.getItem('@token');
      const from_id = await AsyncStorage.getItem('@customer_id');

      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          if (from_id === null) {
            Alert.alert(
              'Oops !',
              'Customer id is missing. Please Log In', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false });
          }
          else if (this.state.to_id === null) {
            Alert.alert(
              'Oops !',
              'Seller id is missing', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
          }
          else if (this.state.message === null) {
            Alert.alert(
              'Oops !',
              'Message is missing', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
          }
          else if (!this.state.checkboxTerms) {
            Alert.alert(
              'Oops !',
              'Please agree with Terms & Conditions', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
          }
          else if (this.state.quantity == null) {
            Alert.alert(
              'Oops !',
              'Please Enter Valid Quantity Amount', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
          }
          else {
            // section for post data

            fetch(serverURL + 'api/enquiry/addEnquiry', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': savedToken,
              },
              body: JSON.stringify({
                to_id: this.state.to_id,
                from_id: from_id,
                seller_item_id: this.state.seller_item_id,
                message: this.state.message,
                quantity: this.state.quantity
              }),
            })
              .then(response => {
                const status = response.status;
                const data = response.json();

                return Promise.all([status, data]);
              })
              .then((responseJson) => {
                console.log(responseJson);
                if (responseJson[0] == 200) {
                  Alert.alert('Success Message!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack(null) }], { cancelable: false });
                }
                else {
                  Alert.alert('Error Message!', responseJson[1].msg);
                }
              })
              .catch((error) => {
                console.error(error);
              });

          }
        } else {
          Alert.alert(
            'Oops !',
            'Seems like you don\'t have an active internet connection!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
        }
      });



    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }



  }




  render() {
    return (
      <Container>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={style.styles.productListHeaderContainerMiddle}>
                  <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 16, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Send message to this supplier</Text>
                </View>
                <View style={style.styles.productListHeaderContainerRight}></View>
                <View style={style.styles.productListHeaderContainerRighttwo}>


                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.90 }} >
            <Content>
              {
                (this.state.serverError) ? (
                  <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                ) :
                  (!this.state.connectState) ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                    </View>
                  ) :
                    (this.state.showLoginButton) ? (
                      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                          <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                          </Button>
                        </View>
                      </View>

                    ) :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :


                        <View style={{ paddingLeft: 2, paddingRight: 2, marginTop: 10 }}>

                          <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                            <View style={{ flex: 0.5, flexDirection: 'row' }}>
                              <View style={{ flex: 0.2 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >To:</Text></View>
                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >{this.props.navigation.getParam('sellerCustomerName', 'Not specified')} </Text></View>
                            </View>
                          </View>

                          <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                            <View style={{ flex: 1, flexDirection: 'column' }}>
                              <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Message:</Text></View>
                              <View style={{ flex: 1 }}>
                                <Textarea rowSpan={7} bordered placeholder="Type your message" onChangeText={(message) => this.setState({ message })} ref={'messageClear'} />
                              </View>
                              <View>
                                <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', fontSize: 11 }} > Your message must be between 20-8000 characters </Text>
                              </View>
                            </View>
                          </View>

                          <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                            <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Quantity:</Text></View>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                              <View style={{ flex: 0.6 }}>
                                <Input
                                  style={{ borderWidth: 1, borderColor: '#ddd', height: 40 }}
                                  placeholder='Quantity'
                                  onChangeText={(quantity) => this.setState({ quantity })}
                                  ref={'quantityClear'}
                                  keyboardType={'phone-pad'} />
                              </View>
                              <View style={{ flex: 0.4, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ padding: 10, color: 'red', fontSize: 20 }} > {this.props.navigation.getParam('uomName', 'Not specified')} </Text>
                              </View>
                            </View>
                          </View>

                          <View style={{ flex: 0.8, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingTop: 20 }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                              <View style={{ flex: 0.1, }}>
                                <CheckBox style={{ marginTop: 7 }} color={"#f0ad4e"} selectedColor={"#5cb85c"} onPress={this.toggleRecommend.bind(this)} checked={this.state.checkboxRecommend} />
                              </View>
                              <View style={{ flex: 0.9, }}>
                                <Text style={{ fontSize: 14, padding: 3 }} >Recommend matching suppliers if this supplier doesn’t contact me on Message Center within 24 hours. </Text>
                              </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 15, }}>
                              <View style={{ flex: 0.1 }}>
                                <CheckBox style={{ marginTop: 7 }} color={"#f0ad4e"} selectedColor={"#5cb85c"} onPress={this.toggleTerms.bind(this)} checked={this.state.checkboxTerms} />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={{ fontSize: 16, marginTop: '2%' }} > I agree to share my Business Card to the supplier. </Text>
                              </View>
                            </View>
                          </View>

                          <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                            <Button block rounded onPress={() => this.submitForm()}
                              style={{ bottom: 0, backgroundColor: '#14b351' }} >
                              <Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Send</Text>
                            </Button>
                          </View>



                        </View>



              }
            </Content>
          </View>



          <View style={{ flex: 0.08 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                  <Icon name="ios-home" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }

}


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertBox: {
    backgroundColor: '#1C97F7',
  },
  alertText: {
    fontSize: 12,
    color: '#ffffff',
  },
  conCard: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 20,
  },
  conCardItem: {
    marginLeft: 5,
    marginTop: 5,
  },
  conDetails: {
    fontSize: 15,
    color: 'black',
    marginLeft: 5,
  },
  postCard: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
  }
});