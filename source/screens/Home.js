import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, Dimensions, Image, StatusBar, ActivityIndicator } from 'react-native';
import CarouselBanner from 'react-native-banner-carousel';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-snap-carousel';
import { Icon, Button, } from 'native-base';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import _ from 'lodash';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
var images = [];
const MIN_HEIGHT = 100;
const MAX_HEIGHT = 250;

global.global_isLoggedIn = false;
global.global_LoggedInCustomerName = '';
global.global_LoggedInCustomerId = '';
global.global_LoggedInSellerId = '';
global.global_is_seller = '';
global.global_is_agent = '';
global.global_profileImage = 'http://agro.clematistech.com/assets/images/user.png';
global.global_connectState = 'http://agro.clematistech.com/assets/images/user.png';

export default class MyakpotiScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      item: [],
      service: [],
      category: [],
      weather_update: {},
      latestNews: [],
      loading: true,
      connectState: true,

    }
    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {
    images = [];

    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        this.loadCustomerProfileData();
        this.getBanner();
        this.getCategorySubCategory();
        this.getServices();
        this.getWeather();
        this.getLatestNews();
      } else {
        this.setState({
          connectState: false,
        });
      }
    });

    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setTranslucent(true);

    });
  }
  loadCustomerProfileData = async () => {
    try {
      const savedToken = await AsyncStorage.getItem('@token');

      fetch(serverURL + 'api/login/doAuthenticateMe', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': savedToken,
        },
      })
        .then(response => {
          const status = response.status;
          const data = response.json();

          return Promise.all([status, data]);
        })
        .then((responseJson) => {
          //console.log(responseJson);
          if (responseJson[0] == 200) {
            let total_info = JSON.stringify(responseJson[1].result);
            AsyncStorage.setItem('@total_info', total_info);
            AsyncStorage.setItem('@customer_id', responseJson[1].result.id);

            global_isLoggedIn = true;

            global_LoggedInCustomerId = responseJson[1].result.id;
            global_is_seller = responseJson[1].result.is_seller;
            global_is_agent = responseJson[1].result.is_agent;

            if (typeof (responseJson[1].result.seller_info.id) != 'undefined') {
              global_LoggedInSellerId = responseJson[1].result.seller_info.id;
            }

            if (responseJson[1].result.buyer_info.profile_image) {
              var profileImg = serverURL + 'admin/uploads/bussiness_logo/' + responseJson[1].result.buyer_info.profile_image;
              //alert(profileImg);
              global_profileImage = profileImg;

            }
            global_LoggedInCustomerName = responseJson[1].result.name;
            //console.log(this.state.profileImage);

          }
          else {

            global_isLoggedIn = false;
            global_LoggedInCustomerName = '';
            global_LoggedInCustomerId = '';
            global_LoggedInSellerId = '';
            global_is_seller = '';
            global_is_agent = '';
            global_profileImage = serverURL + 'assets/images/user.png';
            this.logout();
          }
        })
        .catch((error) => {
          console.error(error);
        });
    } catch (e) {
      console.log(e);
    }


  }
  logout = async () => {
    const keys = ['@token', '@total_info']
    try {
      //alert('Pressed!');            
      await AsyncStorage.multiRemove(keys);
      //this.props.navigation.replace('Home');
      global_isLoggedIn = false;
      global_LoggedInCustomerName = '';
      global_LoggedInCustomerId = '';
      global_LoggedInSellerId = '';
      global_is_seller = '';
      global_is_agent = '';
      global_profileImage = serverURL + 'assets/images/user.png';
    }
    catch (error) {
      console.error();
    }
  }
  getLatestNews = () => {
    const url = serverURL + "api/news/getNews";

    var data = {
      resultPerPage: 4,
    };
    //console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res)  
        var latNews = [];
        var data = res.result;

        if (res.status === 200) {
          var tempNews = res.news;

          tempNews.map((y) => {
            var nc = {
              "name": y.title,
              'title': y.title,
              "date": y.date,
              "image": y.image,
              'id': y.id,
              "description": y.description
            };
            latNews.push(nc);
          })

          this.setState({
            latestNews: latNews,
            loading: false
          });
          //console.log(this.state.latestNews);
        }

      })
      .catch(error => {
        console.log("aa", JSON.stringify(error))
      })

  }
  getCategorySubCategory = () => {
    const url = serverURL + "api/main";
    var method = [];
    method.push('getCategoryItem');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var categories = [];
        index = 0;
        var temp = res.result.item;
        while (parseInt(index) < parseInt(temp.length)) {
          var category = {
            'category_id': temp[index].category_id,
            'category_name': temp[index].category_name,
            'image': temp[index].image
          };
          categories.push(category)
          index++;
        }
        this.setState({
          category: categories
        });
      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }

  getWeather = () => {
    const url = serverURL + "api/weather/weatherupdate";
    var data = {};
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var weather_update = res.weather_update;

        this.setState({
          weather_update: weather_update
        });
      })
      .catch(error => {
        //alert(JSON.stringify(error))
      })
  }
  getServices = () => {
    const url = serverURL + "api/main";
    var method = [];
    method.push('getService');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',

      }),
      body: JSON.stringify(data),


    })
      .then(res => res.json())
      .then(res => {
        var service = res.result.service;


        // console.log(service)
        this.setState({
          service: service
        });
      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }
  getBanner = () => {

    const url = serverURL + "api/main";
    var method = [];
    method.push('getBanner');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',

      }),
      body: JSON.stringify(data),


    })
      .then(res => res.json())
      .then(res => {
        var banner = res.result.banner;

        banner.map((item) => {
          images.push(serverURL + 'admin/uploads/banner/' + item.image)
        });

        this.setState({
          images: res.result.banner
        });
      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }
  renderPage(image, index) {
    return (
      <View key={index}>

        <Image style={{ width: BannerWidth, height: BannerHeight }} source={{ uri: image }} />
      </View>
    );
  }

  _renderNews({ item, index }) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsDetails', { item: item })} >
        <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }} >

          <View style={{ width: '100%', }} >
            <View style={{}} >
              <Image style={{ width: '100%', height: 200, borderRadius: 10 }} source={{ uri: serverURL + 'admin/uploads/news/' + item.image[0] }} />
            </View>
            <View style={{}} >
              <Text numberOfLines={1} style={{ color: '#000', left: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold', width: '90%', flex: 1 }}>{`${item.name}`}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10 }}>
              <View style={{ width: 30 }}><Icon name="ios-calendar" style={{ color: '#ff7519' }} /></View>
              <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', paddingTop: 5 }} >{`${item.date}`}</Text></View>
            </View>
          </View>

        </View>
      </TouchableOpacity>
    );
  }
  _renderCategory({ item, index }) {

    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'product', 'data': item, 'item': 1, cat_id: item.category_id })}>
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }}  >
          <Text style={{ position: 'absolute', zIndex: 9, color: '#fff', fontSize: 20, fontWeight: 'bold', paddingTop: 4, justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%' }}> {`${item.category_name}`}</Text>
          <Image style={{ width: '100%', height: 200, borderRadius: 10, opacity: 0.5, backgroundColor: 'black', }} source={{ uri: serverURL + 'admin/uploads/category/' + item.image }} />
          <View>

          </View>
        </View>
      </TouchableOpacity>
    );
  }
  _renderService({ item, index }) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'service', 'data': item, 'item': 1, cat_id: item.service_category_id })}>
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }} >
          <Text style={{ position: 'absolute', zIndex: 9, color: '#fff', fontSize: 20, fontWeight: 'bold', paddingTop: 4, justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%' }}> {`${item.service_category_name}`}</Text>
          <Image style={{ width: '100%', height: 200, borderRadius: 10, opacity: 0.5, backgroundColor: 'black', }} source={{ uri: serverURL + 'admin/uploads/servicecategory/' + item.image }} />
          <View>

          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderNews({ item, index }) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsDetails', { item: item })} >
        <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }} >

          <View style={{ width: '100%', }} >
            <View style={{}} >
              <Image style={{ width: '100%', height: 200, borderRadius: 10 }} source={{ uri: serverURL + 'admin/uploads/news/' + item.image[0] }} />
            </View>
            <View style={{}} >
              <Text numberOfLines={1} style={{ color: '#000', left: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold', width: '90%', flex: 1 }}>{`${item.name}`}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingLeft: 10 }}>
              <View style={{ width: 30 }}><Icon name="ios-calendar" style={{ color: '#ff7519' }} /></View>
              <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', paddingTop: 5 }} >{`${item.date}`}</Text></View>
            </View>
          </View>

        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (


      <View style={{ flex: 1, flexDirection: 'column' }}>
        {
          (!this.state.connectState) ? (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
            </View>
          ) :
            this.state.loading ? (
              <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
            )
              : <View style={{ flex: 1 }} >
                <HeaderImageScrollView
                  maxHeight={MAX_HEIGHT}
                  minHeight={MIN_HEIGHT}
                  maxOverlayOpacity={0.6}
                  minOverlayOpacity={0.3}

                  renderFixedForeground={() => (

                    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', height: '19%', flex: 1, flexDirection: 'column' }}>
                      <CarouselBanner autoplay autoplayTimeout={5000}
                        loop
                        index={0}
                        pageIndicatorStyle={{ backgroundColor: '#fff', top: 2 }}
                        activePageIndicatorStyle={{ backgroundColor: '#000' }}
                        pageIndicatorContainerStyle={{ backgroundColor: 'grey', height: 10, opacity: 0.7, borderRadius: 10 }}
                        pageSize={BannerWidth}
                      >
                        {images.map((image, index) => this.renderPage(image, index))}
                      </CarouselBanner>

                      <View style={{ position: 'absolute', top: 35, left: 10, backgroundColor: '#fff', width: '95%', height: '19%', flex: 1, flexDirection: 'row', borderRadius: 2 }}>


                        <View style={{ left: 5, flex: 0.10, justifyContent: 'center', alignItems: 'center', }} >
                          <TouchableOpacity onPress={this.props.navigation.openDrawer}>
                            <Image source={require('../assets/images/menu-button.png')} />
                          </TouchableOpacity>
                        </View>

                        <View style={{ flex: 0.90, }} >
                          <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 0.90, paddingTop: 12, paddingLeft: 15 }} >
                              <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchItem')} >
                                <Text style={{ fontSize: 18, color: 'grey' }}>Search products or suppliers</Text>
                              </TouchableOpacity>
                            </View>
                            <View style={{ flex: 0.10, justifyContent: 'center', alignItems: 'center', paddingRight: 10 }} >
                              <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchItem')} >
                                <Icon name="search" />
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>


                      </View>


                    </View>

                  )}

                >
                  {/*-Landing page menu start-*/}
                  <View style={{ flex: 1, flexDirection: 'column' }} >
                    <View style={{ flex: 0.10, backgroundColor: '#ECF0F1' }} >
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.33 }} >
                          <Button vertical onPress={() => this.props.navigation.navigate('Categories')} style={{ backgroundColor: '#ECF0F1', flex: 1 }}>
                            <Icon name="list" style={{ color: 'white', backgroundColor: '#006400', width: 40, height: 40, alignItem: 'center', paddingTop: 9, textAlign: 'center', borderRadius: 100, top: 5 }} />
                            <Text style={{ color: '#000', top: 5 }}>Categories</Text>
                          </Button>
                        </View>
                        <View style={{ flex: 0.33 }} >
                          <Button vertical onPress={() => this.props.navigation.navigate('Services')} style={{ backgroundColor: '#ECF0F1', flex: 1 }}>
                            <Icon name="cog" style={{ color: 'white', backgroundColor: '#20B2AA', width: 40, height: 40, alignItem: 'center', paddingTop: 9, textAlign: 'center', borderRadius: 100, top: 5 }} />
                            <Text style={{ color: '#000', top: 5 }}>Services</Text>
                          </Button>
                        </View>
                        <View style={{ flex: 0.34 }} >
                          <Button vertical onPress={() => this.props.navigation.navigate('SendRfq')} style={{ backgroundColor: '#ECF0F1', flex: 1 }}>
                            <Image source={require('../assets/images/rfq.png')} style={{ width: 40, height: 40, borderRadius: 100, top: 5 }} />
                            <Text style={{ color: '#000', top: 5 }}>RFQ</Text>
                          </Button>
                        </View>

                      </View>
                    </View>
                  </View>
                  {/*-Landing page menu end-*/}
                  {/*-My market Start-*/}
                  <View style={{ backgroundColor: '#ECF0F1' }} >
                    <View style={{ flex: 1, flexDirection: 'row', top: 10, margin: 10, height: 35, justifyContent: 'flex-start', alignItems: 'center' }} >
                      <View style={{ flex: 0.10, left: 2 }}  >

                        <Icon name="grid" style={{ color: '#006400' }} />

                      </View>
                      <View style={{ flex: 0.15 }} >

                        <Text style={{ color: '#000', textAlign: 'left', left: -9, fontSize: 20, fontWeight: 'bold' }}>  My</Text>
                      </View>
                      <View style={{ flex: 0.25 }} >
                        <Text style={{ textAlign: 'left', left: -9, color: '#004c00', fontSize: 20, fontWeight: 'bold' }}>Market</Text>
                      </View>
                      <View style={{ flex: 0.50, flexDirection: 'row-reverse', }} >
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Categories')} >
                          <Image source={require('../assets/images/arrow-forward.png')} style={{ width: 30, height: 30, borderRadius: 100, top: 2 }} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' }} >

                      <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.category}
                        loop={true}
                        hasParallaxImages={true}
                        renderItem={this._renderCategory.bind(this)}
                        sliderWidth={BannerWidth}
                        itemWidth={300}
                      />

                    </View>
                  </View>
                  {/*-My market end-*/}
                  {/*-My Service Start-*/}
                  <View style={{ backgroundColor: '#ECF0F1' }} >
                    <View style={{ flex: 1, flexDirection: 'row', top: 10, margin: 10, height: 35, justifyContent: 'flex-start', alignItems: 'center' }} >
                      <View style={{ flex: 0.10, left: 2 }} >
                        <Icon name="cog" style={{ color: '#20B2AA' }} />
                      </View>
                      <View style={{ flex: 0.15 }} >

                        <Text style={{ color: '#000', textAlign: 'left', left: -3, fontSize: 20, fontWeight: 'bold' }}> Our</Text>
                      </View>
                      <View style={{ flex: 0.25 }} >
                        <Text style={{ textAlign: 'left', left: -3, color: '#004c00', fontSize: 20, fontWeight: 'bold' }}>Services</Text>
                      </View>
                      <View style={{ flex: 0.50, flexDirection: 'row-reverse', }} >
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Services')} >
                          <Image source={require('../assets/images/arrow-forward.png')} style={{ width: 30, height: 30, borderRadius: 100, top: 2 }} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' }} >

                      <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.service}
                        loop={true}
                        hasParallaxImages={true}
                        renderItem={this._renderService.bind(this)}
                        sliderWidth={BannerWidth}
                        itemWidth={300}
                      />

                    </View>
                  </View>
                  {/*-My Service end-*/}
                  {/*-News Start-*/}

                  <View style={{ backgroundColor: '#ECF0F1' }} >
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Newslist')} >
                      <View style={{ flex: 1, flexDirection: 'row', top: 10, margin: 10, height: 35, justifyContent: 'flex-start', alignItems: 'center' }} >

                        <View style={{ flex: 0.08, left: 2 }} >
                          <Icon name="radio" style={{ color: '#006400' }} />
                        </View>

                        <View style={{ flex: 0.20 }} >
                          <Text style={{ textAlign: 'left', left: 0, color: '#004c00', fontSize: 20, fontWeight: 'bold' }}>News</Text>
                        </View>

                        <View style={{ flex: 0.72, flexDirection: 'row-reverse', }} >
                          <Image source={require('../assets/images/arrow-forward.png')} style={{ width: 30, height: 30, borderRadius: 100, top: 2 }} />
                        </View>

                      </View>
                    </TouchableOpacity>

                    <View style={{ flex: 1, flexDirection: 'column' }} >

                      <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.latestNews}
                        loop={true}
                        hasParallaxImages={true}
                        renderItem={this._renderNews.bind(this)}
                        sliderWidth={BannerWidth}
                        itemWidth={300}
                      />

                    </View>
                  </View>
                  {/*-News end-*/}
                  {/*-Weather Start-*/}
                  <View style={{ flex: 1, flexDirection: 'column', minHeight: 150, backgroundColor: '#ABEBC6', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', justifyContent: 'flex-start', shadowOffset: { width: 0, height: 20 }, shadowRadius: 52, borderRadius: 10 }} >
                    <View style={{ flex: 0.20, flexDirection: 'row' }} >
                      <View style={{ flex: 0.50, left: 10, top: 10 }} >
                        <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 16 }}>{`${this.state.weather_update.locationCity}`}</Text>
                      </View>
                      <View style={{ flex: 0.50, right: 10, top: 10 }} >
                        <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 16 }}>{`${this.state.weather_update.day}`}, {`${this.state.weather_update.date}`}</Text>
                      </View>
                    </View>
                    <View style={{ flex: 0.30, flexDirection: 'row' }} >
                      <View style={{ flex: 1, left: 10, top: 10 }} >
                        <View style={{ flex: 1, flexDirection: 'column' }} >
                          <View style={{ flex: 0.30 }} >
                            <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 16 }}>{`${this.state.weather_update.currentTemp}`}{"\u2103"}, {`${this.state.weather_update.category}`}</Text>
                          </View>

                          <View style={{ flex: 0.70 }} >
                            <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 16 }}> {`${this.state.weather_update.windSpeed}`}</Text>

                          </View>
                        </View>

                      </View>
                      <View style={{ flex: 0.50 }} >
                        <Image style={{ width: '100%', height: '100%' }} source={{ uri: serverURL + 'assets/images/weather/app/' + this.state.weather_update.weatherIcon }} />
                      </View>
                    </View>

                  </View>
                  {/*-Weather end-*/}

                </HeaderImageScrollView>


              </View>
        }


      </View>
    );
  }
}
