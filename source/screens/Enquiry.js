import React from 'react';
import { SafeAreaView, View, Text, Dimensions, TouchableOpacity, StatusBar, ActivityIndicator, YellowBox } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Button, Container, Content, List, Body, Thumbnail, Left, ListItem, Card, CardItem, } from 'native-base';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

import { NavigationEvents } from 'react-navigation';

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default class Enquiry extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            serverData: [],
            pageno: 1,
            unread: 0,
            count: 0,
            forceReload: false,

            enquiryBoxshw: false,
            enquiryBoxText: null,

            errorMsg: null,
            showLoginButton: false,
            connectState: true,
        }
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };

    componentDidMount() {
        this.getEnquiryListData();
    }

    getEnquiryListData = async () => {

        if (global_isLoggedIn) {
            this.setState({
                errorMsg: null,
                showLoginButton: false
            });

            let url = serverURL + 'api/enquiry/getEnquiry';
            let savedToken = await AsyncStorage.getItem('@token');

            var data = { pageno: this.state.pageno, enq_type: 1 };

            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: savedToken,
                    'credentials': 'same-origin'
                },
                body: JSON.stringify(data),
            })
                .then(res => res.json())
                .then(res => {
                    //console.log('Hello', res);
                    var data = res.result;

                    if (res.status === 200) {
                        if (res.response_data.length > 0) {
                            this.setState({
                                serverData: res.response_data,
                                unread: res.response_data.unread,
                                loading: false,
                                forceReload: false,
                            });
                        }
                        else {
                            this.setState({
                                loading: false,
                                enquiryBoxshw: true,
                                enquiryBoxText: 'Sorry! No Enquiry Data Found!',
                                forceReload: false,
                            });
                        }
                    }
                    else {
                        this.setState({
                            loading: false,
                            enquiryBoxshw: true,
                            enquiryBoxText: 'Sorry! No Enquiry Data Found!',
                            forceReload: false,
                        });
                    }

                })
                .catch(error => {
                    console.log(JSON.stringify(error))
                });
        }
        else {
            this.setState({
                errorMsg: 'Please login to view this section.',
                showLoginButton: true
            });
        }
    }

    goBack() {
        // this.setState({
        //     serverData: [],
        //     unread: 0,
        //     count: 0,
        //     loading: false,
        //     forceReload: true
        // }, () => {
        //     this.props.navigation.goBack(null);
        // });
        this.props.navigation.goBack(null);
    }

    render() {


        const serverData = Object.values(this.state.serverData);

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Container>
                    <View style={{ flex: 1, flexDirection: 'column' }}>

                        <StatusBar backgroundColor="transparent" barStyle="dark-content" />

                        <NavigationEvents onDidFocus={() => this.getEnquiryListData()} />

                        <View style={{ flex: 0.10 }} >
                            <View style={style.styles.categoriesHeader}>
                                <View style={style.styles.signinHeaderContainer}>
                                    <View style={{ flex: 0.30, justifyContent: 'center', alignItems: 'center', paddingTop: 20, }}>
                                        <TouchableOpacity onPress={() => this.goBack()} style={{ width: (Width * 15 / 100), height: (Height * 5 / 100), justifyContent: 'center', alignItems: 'center' }}>
                                            <Icon name="ios-arrow-back" style={{ fontSize: 30, color: '#000', }} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={style.styles.productListHeaderContainerMiddle}>
                                        <Text style={{ textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Enquiries</Text>
                                    </View>
                                    <View style={{ flex: 0.25, top: (Height * 5.5 / 100) }}>
                                        {
                                            (this.state.unread > 0) ?
                                                <Text style={{ textAlign: 'left', paddingRight: 5, color: '#04ab30' }}> {this.state.unread} Unread</Text>
                                                :
                                                <Text style={{ textAlign: 'left', paddingRight: 5, color: '#04ab30' }}> </Text>
                                        }
                                    </View>

                                    <View style={style.styles.productListHeaderContainerRighttwo}></View>
                                </View>
                            </View>
                        </View>

                        <View style={{ flex: 0.80, paddingTop: 5, }} >
                            <Content>

                                {
                                    (!this.state.connectState) ? (
                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                                        </View>
                                    ) :
                                        (this.state.showLoginButton)
                                            ?
                                            (
                                                <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                                                    <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                                                    <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                                                        <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                                                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                                                        </Button>
                                                    </View>
                                                </View>
                                            ) :
                                            (!this.state.loading && this.state.serverData.length == 0) ?
                                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 250 }}>
                                                    <Text style={{ fontSize: 24, color: '#a94442' }}>No Data Found!!</Text>
                                                </View>
                                                :
                                                (this.state.enquiryBoxshw)
                                                    ?
                                                    (
                                                        <View style={{ top: (Height * 30 / 100), width: Width, justifyContent: 'center', alignItems: 'center' }} >
                                                            <Card style={{ width: Width * 95 / 100 }}>
                                                                <CardItem>
                                                                    <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                        <Text style={{ fontSize: 20, }}>
                                                                            {this.state.enquiryBoxText}
                                                                        </Text>
                                                                    </Body>
                                                                </CardItem>
                                                            </Card>
                                                        </View>
                                                    )
                                                    :
                                                    (this.state.loading)
                                                        ?
                                                        (<View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>)
                                                        :
                                                        (
                                                            <List>
                                                                {
                                                                    (serverData || []).map((y) => {
                                                                        let profileImg = serverURL + 'assets/images/user.png';
                                                                        if (y.seller_profile_image != '') {
                                                                            profileImg = serverURL + 'admin/uploads/bussiness_logo/' + y.seller_profile_image;
                                                                        }
                                                                        let readcolor = '#888';

                                                                        return <ListItem avatar style={{ width: '92%' }} onPress={() => this.props.navigation.navigate('MyMessageDetails', {
                                                                            reltype: y.type,
                                                                            relpic: profileImg,
                                                                            relname: y.buyer_name,
                                                                            relid: y.enq_id
                                                                        })}>
                                                                            <Left>
                                                                                <Thumbnail rounded source={{ uri: profileImg }} />
                                                                            </Left>
                                                                            <Body>
                                                                                <Text style={{ color: readcolor }}> {y.seller_name} </Text>
                                                                                <Text note numberOfLines={3} style={{ paddingTop: 5, paddingBottom: 5, color: readcolor }}> {y.enq_message} </Text>
                                                                                <Text style={{ color: readcolor, textAlign: 'right' }}> {y.enq_date} </Text>
                                                                            </Body>
                                                                        </ListItem>

                                                                    })
                                                                }

                                                            </List>
                                                        )

                                }

                            </Content>
                        </View>

                        <View style={{ flex: 0.10, marginBottom: 0, paddingBottom: 0 }} >
                            <Footer style={{ height: '100%' }} >
                                <FooterTab style={{ backgroundColor: '#000' }}>
                                    <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                                        <Icon name="ios-home" style={{ color: 'grey' }} />
                                        <Text style={{ color: 'grey' }}>Home</Text>
                                    </Button>
                                    <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                                        <Icon name="ios-person" style={{ color: 'grey' }} />
                                        <Text style={{ color: 'grey' }}>Myakpoti</Text>
                                    </Button>
                                    <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                                        <Icon name="ios-list" style={{ color: 'grey' }} />
                                        <Text style={{ color: 'grey' }}>Categories</Text>
                                    </Button>
                                    <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                                        <Icon name="ios-cog" style={{ color: 'grey' }} />
                                        <Text style={{ color: 'grey' }}>Services</Text>
                                    </Button>
                                    <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                                        <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                                        <Text style={{ color: 'grey' }}>Messenger</Text>
                                    </Button>
                                </FooterTab>
                            </Footer>
                        </View>

                    </View>
                </Container>
            </SafeAreaView>
        )
    }
}