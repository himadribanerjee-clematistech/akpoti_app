import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator, Alert, Image, YellowBox } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Container, Content, Item, Button, Input, Form, Textarea, Picker } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const serverURL = 'http://agro.clematistech.com/';

const createFormData = (photo, body) => {
  const data = new FormData();

  photo.forEach((item, i) => {
    data.append("image[]", {
      uri: Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
      type: item.type,
      name: item.filename || `filename${i}.jpg`,
      //data: item.data
    });
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};

export default class AddSellerItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isSubmited: false,
      validForm: true,
      customer_id: null,
      seller_id: null,
      selectedImages: [],
      shipping_information: '',
      packing_information: '',
      special_information: '',
      organic_YN: 0,
      uom_id: '',
      min_quantity: 0,
      max_price: 0.00,
      min_price: 0.00,
      item_description: '',
      species_id: '',
      item_id: '',
      category_id: '',
      categoryArray: [],
      subcategory_id: '',
      subcategoryArray: [],
      itemArray: [],
      speciesArray: [],
      uomArray: [],
      isOrganicArray: [{ "id": 1, "value": "Yes" }, { "id": 0, "value": "No" }],
      profileType: 'seller',
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',


    }

    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
      quality: 0.3
    }
    ImagePicker.launchImageLibrary(options, response => {
      var fileSizeInGb = parseInt(Math.floor(Math.log(response.fileSize) / Math.log(1024)));
      //alert(fileSizeInGb);
      if (fileSizeInGb > 2) {
        Alert.alert(
          'Oops !',
          'Image size can\'t be greater than 2 MB!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      } else {
        if (response.uri) {
          var selectedImg = [];
          this.state.selectedImages.map((y) => {
            selectedImg.push(y);
          })
          //alert(response.fileSize);
          selectedImg.push(response);
          this.setState({ selectedImages: selectedImg })

          //console.log(this.state.selectedImages)
        }
      }

    })
  }

  componentDidMount() {
    //alert(global_LoggedInCustomerName+'='+global_LoggedInCustomerId);
    if (global_isLoggedIn) {
      if (global_is_seller == 0) {
        this.setState({
          profileType: 'buyer',
          errorMsg: 'Upgrade to seller to view this section',
          loading: false

        });
      } else {
        this.setState({
          customer_id: global_LoggedInCustomerId
        });

        NetInfo.isConnected.fetch().then(isConnected => {
          if (isConnected) {
            this.getCategoriesData();
          } else {
            this.setState({
              connectState: false,
            });
          }
        });
      }
    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }



  }

  getToken = async () => {
    let savedToken = await AsyncStorage.getItem('@token');
    return savedToken;
  }

  getCategoriesData = async () => {
    const savedToken = await AsyncStorage.getItem('@token');
    this.setState({
      subcategoryArray: [],
      itemArray: [],
      speciesArray: [],
      uomArray: []
    });


    const url = serverURL + "api/categories";


    var data = {
      pageno: 1,
    };
    //console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {

        var data = res.result;
        //alert(res.status)
        //console.log(data.category);
        var sellerCAtegories = [];
        if (res.status === 200) {
          data.category.map((y) => {

            sellerCAtegories.push(y);

          })

          this.setState({
            categoryArray: sellerCAtegories
          });
        } else {
          this.setState({
            categoryArray: []
          });
        }

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })

  }

  getSubCategoriesData = (value) => {
    const url = serverURL + "api/quotation/getsubCat";
    this.setState({
      itemArray: [],
      speciesArray: [],
      uomArray: []
    });

    if (this.state.category_id !== null) {
      var data = {
        id: this.state.category_id,
      };
      //console.log(url,data)
      fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          //console.log(res)  
          //die;
          var data1 = res.result;
          //alert(res.status)
          //console.log(res);
          var sellerSubCAtegories = [];
          var uomArr = [];

          if (res.status === 200) {

            data1.subcategory.map((y) => {

              sellerSubCAtegories.push(y);

            })

            this.setState({
              subcategoryArray: sellerSubCAtegories
            });
            //console.log(data1.uomlist);
            //console.log(this.state.subcategoryArray);
            this.setState({
              uomArray: uomArr
            });
            data1.uomlist.map((y) => {
              uomArr.push(y);
            })

            this.setState({
              uomArray: uomArr
            });

          } else {
            this.setState({
              subcategoryArray: []
            });
          }


        })
        .catch(error => {
          // console.log("aa",JSON.stringify(error))
        })
    }


  }


  getItemsData = (value) => {
    const url = serverURL + "api/itemBySubCat";
    this.setState({
      itemArray: [],
      speciesArray: []
    });

    if (this.state.subcategory_id !== null && this.state.category_id !== null) {
      var data = {
        id: this.state.subcategory_id,
        category_id: this.state.category_id,
      };
      //console.log(url,data)
      fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          var data1 = res.result;
          //alert(res.status)
          //console.log(res.status);
          //console.log(data1);
          var sellerItems = [];

          this.setState({
            itemArray: sellerItems
          });

          if (res.status === 200) {
            data1.item.map((y) => {
              sellerItems.push(y);
            })

            this.setState({
              itemArray: sellerItems
            });
          } else {
            this.setState({
              itemArray: []
            });
          }


        })
        .catch(error => {
          // console.log("aa",JSON.stringify(error))
        })

    }


  }


  getSpeciesData = (value) => {
    const url = serverURL + "api/SpeciesByItem";
    if (this.state.item_id !== null) {
      var data = {
        id: this.state.item_id
      };
      //console.log(url,data)
      fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          var data1 = res.result;
          //alert(res.status)
          //console.log(res.status);
          //console.log(data1);

          var sellerSpecies = [];

          this.setState({
            speciesArray: sellerSpecies
          });

          if (res.status === 200) {
            data1.species.map((y) => {
              sellerSpecies.push(y);
            })

            this.setState({
              speciesArray: sellerSpecies
            });
          } else {
            this.setState({
              speciesArray: []
            });
          }


        })
        .catch(error => {
          // console.log("aa",JSON.stringify(error))
        })
    }


  }


  onChangeCategory(value) {
    this.setState({
      category_id: value
    }, () => {
      this.getSubCategoriesData(value);
    });
  }

  onChangeSubCategory(value) {
    this.setState({
      subcategory_id: value

    }, () => {
      this.getItemsData(value);
    });

  }

  onChangeItem(value) {
    this.setState({
      item_id: value
    }, () => {
      this.getSpeciesData(value);
    });

  }

  onChangeSpecies(value) {
    this.setState({
      species_id: value
    });
  }

  onChangeUOMId(value) {
    this.setState({
      uom_id: value
    });
  }

  onChangeIsOrganic(value) {
    this.setState({
      organic_YN: value
    });
  }


  submitForm = async () => {
    var submit = true;
    let savedToken = await AsyncStorage.getItem('@token');

    NetInfo.isConnected.fetch().then(isConnected => {
      //console.log(isConnected);
      if (isConnected) {
        if (this.state.category_id == '') {
          Alert.alert(
            'Oops !',
            'Select category!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.subcategory_id == '') {
          Alert.alert(
            'Oops !',
            'Select sub category!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.item_id == '') {
          Alert.alert(
            'Oops !',
            'Select Item!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.species_id == '') {
          Alert.alert(
            'Oops !',
            'Select Species!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.item_description == '') {
          Alert.alert(
            'Oops !',
            'Item description is empty!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }

        if (parseInt(this.state.min_quantity) < 1) {
          Alert.alert(
            'Oops !',
            'Minimum quantity cannot be less than 1', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.min_price == '') {
          Alert.alert(
            'Oops !',
            'Minimum price is empty!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        } else {
          var regex = /(?:\d*\.\d{1,2}|\d+)$/;
          if (regex.test(this.state.min_price)) {
            if (this.state.min_price < 0) {
              Alert.alert(
                'Oops !',
                'Negative price is not allowed!', [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                { cancelable: false })
              submit = false;
              return false;
            }
          } else {
            Alert.alert(
              'Oops !',
              'Invalid minimum price!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }

        }

        if (this.state.max_price == '') {
          Alert.alert(
            'Oops !',
            'Maximum price is empty!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        } else {
          var regex = /(?:\d*\.\d{1,2}|\d+)$/;
          if (regex.test(this.state.max_price)) {
            if (this.state.max_price < 0) {
              Alert.alert(
                'Oops !',
                'Negative price is not allowed!', [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                { cancelable: false })
              submit = false;
              return false;
            }
          } else {
            Alert.alert(
              'Oops !',
              'Invalid minimum price!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
        }

        var minPrice = parseFloat(this.state.min_price);
        var maxPrice = parseFloat(this.state.max_price);
        //console.log(minPrice+'='+maxPrice);

        if (parseFloat(minPrice) > parseFloat(maxPrice)) // 233 > 2123
        {
          Alert.alert(
            'Oops !',
            'Maximum price should be greater than minimum price.', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }

        if (submit) {
          this.setState({ isSubmited: true });
          //alert('successfull');

          //let url = serverURL+"api/seller/testdata";
          let url = serverURL + "api/seller/addSellerItem";
          //console.log(url);
          //console.log(savedToken);


          fetch(url, {
            method: "POST",
            headers: {
              'Content-Type': 'multipart/form-data',
              Authorization: savedToken,
              'credentials': 'same-origin'
            },
            timeout: 0,
            compress: true,
            body: createFormData(this.state.selectedImages, {
              customer_id: this.state.customer_id,
              seller_id: global_LoggedInSellerId,
              shipping_information: this.state.shipping_information,
              packing_information: this.state.packing_information,
              special_information: this.state.special_information,
              organic_YN: this.state.organic_YN,
              uom_id: this.state.uom_id,
              min_quantity: this.state.min_quantity,
              max_price: this.state.max_price,
              min_price: this.state.min_price,
              item_description: this.state.item_description,
              species_id: this.state.species_id,
              item_id: this.state.item_id
            })
          }).then(res => res.json())
            .then(res => {
              //console.log(res);
              if (res.status == 200) {
                Alert.alert(
                  'Success !',
                  res.msg, [
                  { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
                  { cancelable: false })
              } else {
                Alert.alert(
                  'Success !',
                  res.msg, [
                  { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
                  { cancelable: false })
                this.setState({
                  isSubmited: false,
                })
              }

            })
            .catch(err => {
              console.error("error uploading images: ", err);
            });

        }
      } else {
        Alert.alert(
          'Oops !',
          'Seems like you don\'t have an active internet connection!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      }
    });

  }

  nextAction() {
    this.setState({
      loading: false,
      isSubmited: false,
      validForm: true,
      customer_id: null,
      seller_id: null,
      selectedImages: [],
      shipping_information: '',
      packing_information: '',
      special_information: '',
      organic_YN: 0,
      uom_id: '',
      min_quantity: 0,
      max_price: 0.00,
      min_price: 0.00,
      item_description: '',
      species_id: '',
      item_id: '',
      category_id: '',
      categoryArray: [],
      subcategory_id: '',
      subcategoryArray: [],
      itemArray: [],
      speciesArray: [],
      uomArray: [],
      isOrganicArray: [{ "id": 1, "value": "Yes" }, { "id": 0, "value": "No" }]
    })
  }

  gotoList() {
    this.props.navigation.navigate('MyItems', { reloadPage: 'yes' });
  }

  delImg(ind) {
    //console.log(ind);
    var selectedImg = [];
    this.state.selectedImages.map((y, index) => {
      if (index == ind) {
        // Skip img
      } else {
        selectedImg.push(y);
      }

    })
    this.setState({ selectedImages: selectedImg })
  }



  render() {
    const { selectedImages } = this.state
    return (
      <Container>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 17, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Add Seller Item</Text></View>
                <View style={style.styles.productListHeaderContainerRight}></View>
                <View style={style.styles.productListHeaderContainerRighttwo}>


                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.80 }} >
            <Content>
              {
                (this.state.serverError) ? (
                  <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                ) :
                  (!this.state.connectState) ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10, marginTop: 50 }} />
                    </View>
                  ) :
                    (this.state.showLoginButton) ? (
                      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                          <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                          </Button>
                        </View>
                      </View>

                    ) :
                      (!this.state.loading && this.state.profileType == 'buyer') ? (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                          <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#eb5234', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                          <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                            <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('UpgradeSeller')}>
                              <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Upgrade To Seller</Text>
                            </Button>
                          </View>
                        </View>) :
                        this.state.loading ? (
                          <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                        ) :

                          <Form>
                            <View style={{ paddingLeft: 2, paddingRight: 2, marginTop: 10 }}>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                                <View style={{ flex: 0.5, flexDirection: 'row' }}>
                                  <View style={{ flex: 0.3 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingTop: 13 }} >Category:</Text></View>
                                  <View style={{ flex: 0.7 }}>
                                    <Item picker>
                                      <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                        placeholder="Select Category"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.category_id}
                                        onValueChange={(itemValue, itemIndex) => this.onChangeCategory(itemValue)}>{
                                          this.state.categoryArray.map((v) => {
                                            return <Picker.Item label={v.categoryname} value={v.category_id} />
                                          })
                                        }
                                      </Picker>
                                    </Item>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                                <View style={{ flex: 0.5, flexDirection: 'row' }}>
                                  <View style={{ flex: 0.3 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingTop: 13 }} >Sub Category:</Text></View>
                                  <View style={{ flex: 0.7 }}>
                                    <Item picker>
                                      <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                        placeholder="Select Sub Category"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.subcategory_id}
                                        onValueChange={(itemValue, itemIndex) => this.onChangeSubCategory(itemValue)}>{
                                          this.state.subcategoryArray.map((v) => {
                                            return <Picker.Item label={v.subcategory_name} value={v.id} />
                                          })
                                        }
                                      </Picker>
                                    </Item>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                                <View style={{ flex: 0.5, flexDirection: 'row' }}>
                                  <View style={{ flex: 0.3 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingTop: 13 }} >Item:</Text></View>
                                  <View style={{ flex: 0.7 }}>
                                    <Item picker>
                                      <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                        placeholder="Select Item"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.item_id}
                                        onValueChange={(itemValue, itemIndex) => this.onChangeItem(itemValue)}>{
                                          this.state.itemArray.map((v) => {
                                            return <Picker.Item label={v.item_name} value={v.id} />
                                          })
                                        }
                                      </Picker>
                                    </Item>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                                <View style={{ flex: 0.5, flexDirection: 'row' }}>
                                  <View style={{ flex: 0.3 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingTop: 13 }} >Species:</Text></View>
                                  <View style={{ flex: 0.7 }}>
                                    <Item picker>
                                      <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                        placeholder="Select Species"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.species_id}
                                        onValueChange={(itemValue, itemIndex) => this.onChangeSpecies(itemValue)}>{
                                          this.state.speciesArray.map((v) => {
                                            return <Picker.Item label={v.species_name} value={v.id} />
                                          })
                                        }
                                      </Picker>
                                    </Item>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                  <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Item Description:</Text></View>
                                  <View style={{ flex: 1 }}>
                                    <Textarea rowSpan={7} bordered placeholder="Type item description" onChangeText={(item_description) => this.setState({ item_description })} ref={'messageClear'} />
                                  </View>
                                  <View>
                                  </View>
                                </View>
                              </View>


                              <View style={{ flex: 0.8, flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                  <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Min Price:</Text></View>

                                  <View style={{ flex: 0.4 }}>
                                    <Input style={{ borderWidth: 1, borderColor: '#ddd' }} placeholder='Min Price' onChangeText={(min_price) => this.setState({ min_price })} keyboardType={'phone-pad'} />
                                  </View>

                                </View>
                                <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                  <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Max Price:</Text></View>

                                  <View style={{ flex: 0.6 }}>
                                    <Input style={{ borderWidth: 1, borderColor: '#ddd' }} placeholder='Max Price' onChangeText={(max_price) => this.setState({ max_price })} keyboardType={'phone-pad'} />
                                  </View>

                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                  <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Min Quantity:</Text></View>

                                  <View style={{ flex: 0.4 }}>
                                    <Input style={{ borderWidth: 1, borderColor: '#ddd' }} placeholder='Quantity' onChangeText={(min_quantity) => this.setState({ min_quantity })} ref={'quantityClear'} keyboardType={'phone-pad'} />
                                  </View>

                                </View>
                                <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                  <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Unit of Measurement:</Text></View>

                                  <View style={{ flex: 0.6 }}>
                                    <Item picker>
                                      <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                        placeholder="Select UOM"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.uom_id}
                                        onValueChange={(itemValue, itemIndex) => this.onChangeUOMId(itemValue)}>{
                                          this.state.uomArray.map((v) => {
                                            return <Picker.Item label={v.uom_name} value={v.uom_id} />
                                          })
                                        }
                                      </Picker>
                                    </Item>
                                  </View>

                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                  <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Organic?:</Text></View>
                                  <View style={{ flex: 1 }}>
                                    <Item picker>
                                      <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                        placeholder="Is Organic?"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.organic_YN}
                                        onValueChange={(itemValue, itemIndex) => this.onChangeIsOrganic(itemValue)}>{
                                          this.state.isOrganicArray.map((v) => {
                                            return <Picker.Item label={v.value} value={v.id} />
                                          })
                                        }
                                      </Picker>
                                    </Item>
                                  </View>
                                  <View>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                  <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Special Information:</Text></View>
                                  <View style={{ flex: 1 }}>
                                    <Textarea rowSpan={7} bordered placeholder="Type special information(if any)" onChangeText={(special_information) => this.setState({ special_information })} />
                                  </View>
                                  <View>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                  <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Packing Information:</Text></View>
                                  <View style={{ flex: 1 }}>
                                    <Textarea rowSpan={7} bordered placeholder="Type packing information(if any)" onChangeText={(packing_information) => this.setState({ packing_information })} />
                                  </View>
                                  <View>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                  <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Shipping Information:</Text></View>
                                  <View style={{ flex: 1 }}>
                                    <Textarea rowSpan={7} bordered placeholder="Type shipping information(if any)" onChangeText={(shipping_information) => this.setState({ shipping_information })} />
                                  </View>
                                  <View>
                                  </View>
                                </View>
                              </View>

                              <View style={{ flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10 }}>
                                  {

                                    this.state.selectedImages.map((y, index) => {
                                      return <View style={{ flex: 0.5, paddingLeft: 5 }}>
                                        <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                          <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.delImg(index); }} />
                                        </View>
                                        <Image
                                          source={{ uri: y.uri }}
                                          style={{ width: 100, height: 100 }}
                                        /></View>
                                    })
                                  }
                                </View>
                                <Button rounded title="Choose Photo" onPress={this.handleChoosePhoto} ><Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Upload Image</Text>
                                </Button>
                              </View>
                              {
                                this.state.isSubmited ?
                                  <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 30 }}>
                                    <Button bordered rounded success >
                                      <Text style={{ color: '#14b351', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Sending request. Please wait...</Text>
                                    </Button>
                                  </View>
                                  :

                                  <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 30 }}>
                                    <Button block rounded onPress={() => this.submitForm()}
                                      style={{ bottom: 0, backgroundColor: '#14b351' }} >
                                      <Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Submit</Text>
                                    </Button>
                                  </View>
                              }



                            </View>
                          </Form>


              }
            </Content>
          </View>



          <View style={{ flex: 0.10 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                  <Icon name="ios-home" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }

}


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertBox: {
    backgroundColor: '#1C97F7',
  },
  alertText: {
    fontSize: 12,
    color: '#ffffff',
  },
  conCard: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 20,
  },
  conCardItem: {
    marginLeft: 5,
    marginTop: 5,
  },
  conDetails: {
    fontSize: 15,
    color: 'black',
    marginLeft: 5,
  },
  postCard: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
  }
});