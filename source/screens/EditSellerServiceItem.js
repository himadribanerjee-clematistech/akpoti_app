import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator, Alert, Image, YellowBox } from 'react-native';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Container, Content, Item, Button, Input, Form, Textarea, Picker, } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const createFormData = (photo, body) => {
  const data = new FormData();
  if (photo.length > 0) {
    photo.forEach((item, i) => {
      if (item.uri != '') {
        data.append("image[]", {
          uri: Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
          type: item.type,
          name: item.filename || `filename${i}.jpg`,
          //data: item.data
        });
      }

    });
  }


  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });
  //console.log(data);
  return data;
};

export default class EditSellerServiceItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isSubmited: false,
      validForm: true,
      customer_id: null,
      seller_id: null,
      selectedImages: [],
      existingImages: [],
      terms_condition: '',
      special_information: '',
      uom_id: '',
      max_price: 0.00,
      min_price: 0.00,
      item_description: '',
      item_id: '',
      category_id: '',
      categoryArray: [],
      itemArray: [],
      uomArray: [],
      id: null,
      serverError: false,
      connectState: true,
      showLoginButton: false,
      errorMsg: 'Something went wrong. Please try later.',
      profileType: 'seller',


    }

    this.state.id = this.props.navigation.getParam('productDetailsId', null);

    this.CheckConnectivity();


  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
      quality: 0.3
    }
    ImagePicker.launchImageLibrary(options, response => {
      var fileSizeInGb = parseInt(Math.floor(Math.log(response.fileSize) / Math.log(1024)));
      //alert(fileSizeInGb);
      if (fileSizeInGb > 2) {
        Alert.alert(
          'Oops !',
          'Image size can\'t be greater than 2 MB!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      } else {
        if (response.uri) {
          var selectedImg = [];
          this.state.selectedImages.map((y) => {
            selectedImg.push(y);
          })
          //alert(response.fileSize);
          selectedImg.push(response);
          this.setState({ selectedImages: selectedImg })

          //console.log(this.state.selectedImages)
        }
      }

    })
  }

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        if (global_isLoggedIn) {
          if (global_is_seller == 0) {
            this.setState({
              profileType: 'buyer',
              errorMsg: 'Upgrade to seller to view this section',
              loading: false

            });
          } else {
            this.setState({
              customer_id: global_LoggedInCustomerId
            });
            this.getSellerServiceDetailsById();
          }
        } else {
          this.setState({
            errorMsg: 'Please login to view this section.',
            showLoginButton: true

          });
        }
      } else {
        this.setState({
          connectState: false,
        });
      }
    });
  }

  getSellerServiceDetailsById = async () => {

    let savedToken = await AsyncStorage.getItem('@token');
    let url = serverURL + "api/services/getServiceDetailsById";
    var data = {
      id: this.state.id,
    };
    //console.log(url,data,savedToken);
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log('RET:',res);
        var data = res.result;
        //alert(res.status)
        if (res.status === 200) {
          let extImg = [];
          res.result.seller_item.seller_item_images.map((y) => {
            extImg.push(y);
          })

          this.setState({
            terms_condition: res.result.seller_item.terms_condition,
            special_information: res.result.seller_item.special_information,
            uom_id: res.result.seller_item.uom_id,
            max_price: res.result.seller_item.max_rate,
            min_price: res.result.seller_item.min_rate,
            item_description: res.result.seller_item.item_description,
            item_id: res.result.seller_item.item_id,
            category_id: res.result.seller_item.category_id,
            seller_id: global_LoggedInSellerId,
            existingImages: extImg

          }, () => {
            this.getCategoriesData().then(res => this.getItemsData()).then(res => this.setState({ loading: false }));


          });
          //console.log(this.state) 
        } else {
          this.setState({
            serverError: true,
            errorMsg: res.msg

          });
        }
        //console.log(this.state.serverData);    
      })
      .catch(error => {
        console.log("aa12", JSON.stringify(error))
      })

  }

  getToken = async () => {
    let savedToken = await AsyncStorage.getItem('@token');
    return savedToken;
  }

  getCategoriesData = async () => {
    const savedToken = await AsyncStorage.getItem('@token');
    this.setState({
      subcategoryArray: [],
      itemArray: [],
      speciesArray: [],
      uomArray: []
    });


    const url = serverURL + "api/servicecategories";


    var data = {
      pageno: 1,
    };
    //console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {

        var data = res.result;
        //alert(res.status)
        //console.log(data.category);
        var sellerCAtegories = [];
        if (res.status === 200) {
          data.category.map((y) => {

            sellerCAtegories.push(y);

          })

          this.setState({
            categoryArray: sellerCAtegories
          });
        } else {
          this.setState({
            categoryArray: []
          });
        }

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })

  }



  getItemsData = async (value) => {
    const url = serverURL + "api/services/getServiceItemList";
    let savedToken = await AsyncStorage.getItem('@token');
    this.setState({
      itemArray: []
    });

    if (this.state.category_id !== null) {
      var data = {
        service_category_id: this.state.category_id,
      };
      //console.log(url,data)
      fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
          Authorization: savedToken,
          'credentials': 'same-origin'
        }),
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          var data1 = res.result;
          //alert(res.status)
          //console.log(res.status);
          //console.log(data1);
          var sellerItems = [];

          this.setState({
            itemArray: sellerItems
          });

          if (res.status === 200) {
            data1.item.map((y) => {
              sellerItems.push(y);
            })

            this.setState({
              itemArray: sellerItems
            });

            var uomArr = [];
            this.setState({
              uomArray: uomArr
            });
            data1.uomlist.map((y) => {
              uomArr.push(y);
            })

            this.setState({
              uomArray: uomArr
            });

          } else {
            this.setState({
              itemArray: [],
              uomArray: []
            });
          }


        })
        .catch(error => {
          // console.log("aa",JSON.stringify(error))
        })

    }


  }


  onChangeCategory(value) {
    this.setState({
      category_id: value
    }, () => {
      this.getItemsData(value);
    });
  }


  onChangeItem(value) {
    this.setState({
      item_id: value
    }, () => {

    });

  }

  onChangeUOMId(value) {
    this.setState({
      uom_id: value
    });
  }



  submitForm = async () => {
    var submit = true;
    let savedToken = await AsyncStorage.getItem('@token');

    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        if (this.state.category_id == '') {
          Alert.alert(
            'Oops !',
            'Select category!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.item_id == '') {
          Alert.alert(
            'Oops !',
            'Select Item!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.item_description == '') {
          Alert.alert(
            'Oops !',
            'Item description is empty!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }
        if (this.state.min_price == '') {
          Alert.alert(
            'Oops !',
            'Minimum price is empty!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        } else {
          var regex = /(?:\d*\.\d{1,2}|\d+)$/;
          if (regex.test(this.state.min_price)) {
            if (this.state.min_price < 0) {
              Alert.alert(
                'Oops !',
                'Negative price is not allowed!', [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                { cancelable: false })
              submit = false;
              return false;
            }
          } else {
            Alert.alert(
              'Oops !',
              'Invalid minimum price!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }

        }
        if (this.state.max_price == '') {
          Alert.alert(
            'Oops !',
            'Maximum price is empty!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        } else {
          var regex = /(?:\d*\.\d{1,2}|\d+)$/;
          if (regex.test(this.state.max_price)) {
            if (this.state.max_price < 0) {
              Alert.alert(
                'Oops !',
                'Negative price is not allowed!', [
                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                { cancelable: false })
              submit = false;
              return false;
            }
          } else {
            Alert.alert(
              'Oops !',
              'Invalid minimum price!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
        }

        var minPrice = parseFloat(this.state.min_price);
        var maxPrice = parseFloat(this.state.max_price);
        //console.log(minPrice+'='+maxPrice);

        if (parseFloat(minPrice) > parseFloat(maxPrice)) // 233 > 2123
        {
          Alert.alert(
            'Oops !',
            'Maximum price should be greater than minimum price.', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
          submit = false;
          return false;
        }

        if (submit) {
          this.setState({ isSubmited: true });
          //alert('successfull');

          //let url = serverURL+"api/seller/testdata";
          let url = serverURL + "api/services/updateSellerService";
          //console.log(url);
          //console.log(savedToken);


          fetch(url, {
            method: "POST",
            headers: {
              'Content-Type': 'multipart/form-data',
              Authorization: savedToken,
              'credentials': 'same-origin'
            },
            timeout: 0,
            compress: true,
            body: createFormData(this.state.selectedImages, {
              customer_id: this.state.customer_id,
              seller_id: global_LoggedInSellerId,
              terms_condition: this.state.terms_condition,
              special_information: this.state.special_information,
              uom_id: this.state.uom_id,
              max_price: this.state.max_price,
              min_price: this.state.min_price,
              item_description: this.state.item_description,
              item_id: this.state.item_id,
              id: this.state.id
            })
          }).then(res => res.json())
            .then(res => {
              //console.log(res);
              if (res.status == 200) {
                Alert.alert(
                  'Success !',
                  res.msg, [
                  { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
                  { cancelable: false })
              } else {
                Alert.alert(
                  'Success !',
                  res.msg, [
                  { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
                  { cancelable: false })
                this.setState({
                  isSubmited: false,
                })
              }

            })
            .catch(err => {
              console.error("error uploading images: ", err);
            });

        }
      } else {
        Alert.alert(
          'Oops !',
          'Seems like you don\'t have an active internet connection!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      }
    });

  }

  nextAction() {
    this.setState({ isSubmited: false });
  }

  gotoList() {
    this.props.navigation.navigate('MySellerServices', { reloadPage: 'yes' });
  }

  delImg(ind) {
    //console.log(ind);
    var selectedImg = [];
    this.state.selectedImages.map((y, index) => {
      if (index == ind) {
        // Skip img
      } else {
        selectedImg.push(y);
      }

    })
    this.setState({ selectedImages: selectedImg })
  }

  deleteSellerServiceImage = async (image_id) => {
    //alert(image_id);
    let savedToken = await AsyncStorage.getItem('@token');
    let url = serverURL + "api/services/deleteServiceItemImage";
    var data = {
      id: image_id,
      customer_id: global_LoggedInCustomerId,
      seller_id: global_LoggedInSellerId,
      service_item_id: this.state.id
    };
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res);
        var data = res.result;
        //alert(res.status)
        if (res.status === 200) {
          Alert.alert(
            'Success !',
            'Seller Item  successfully deleted', [
            { text: 'OK', onPress: () => this.getMyServiceItemsData(), style: 'cancel' },],
            { cancelable: false })

        }

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }



  render() {
    const { selectedImages } = this.state
    return (
      <Container>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 17, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Edit Seller Service</Text></View>
                <View style={style.styles.productListHeaderContainerRight}></View>
                <View style={style.styles.productListHeaderContainerRighttwo}>


                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.80 }} >
            <Content>
              {
                (this.state.serverError) ? (
                  <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                ) :
                  (!this.state.connectState) ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                    </View>
                  ) :
                    (this.state.showLoginButton) ? (
                      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                          <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                          </Button>
                        </View>
                      </View>

                    ) :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :

                        <Form>
                          <View style={{ paddingLeft: 2, paddingRight: 2, marginTop: 10 }}>

                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                              <View style={{ flex: 0.5, flexDirection: 'row' }}>
                                <View style={{ flex: 0.3 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingTop: 13 }} >Category:</Text></View>
                                <View style={{ flex: 0.7 }}>
                                  <Item picker>
                                    <Picker
                                      mode="dropdown"
                                      iosIcon={<Icon name="arrow-down" />}
                                      style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                      placeholder="Select Category"
                                      placeholderStyle={{ color: "#bfc6ea" }}
                                      placeholderIconColor="#007aff"
                                      selectedValue={this.state.category_id}
                                      onValueChange={(itemValue, itemIndex) => this.onChangeCategory(itemValue)}>{
                                        this.state.categoryArray.map((v) => {
                                          return <Picker.Item label={v.categoryname} value={v.category_id} />
                                        })
                                      }
                                    </Picker>
                                  </Item>
                                </View>
                              </View>
                            </View>


                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
                              <View style={{ flex: 0.5, flexDirection: 'row' }}>
                                <View style={{ flex: 0.3 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingTop: 13 }} >Service Name:</Text></View>
                                <View style={{ flex: 0.7 }}>
                                  <Item picker>
                                    <Picker
                                      mode="dropdown"
                                      iosIcon={<Icon name="arrow-down" />}
                                      style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                      placeholder="Select Item"
                                      placeholderStyle={{ color: "#bfc6ea" }}
                                      placeholderIconColor="#007aff"
                                      selectedValue={this.state.item_id}
                                      onValueChange={(itemValue, itemIndex) => this.onChangeItem(itemValue)}>{
                                        this.state.itemArray.map((v) => {
                                          return <Picker.Item label={v.name} value={v.service_item_id} />
                                        })
                                      }
                                    </Picker>
                                  </Item>
                                </View>
                              </View>
                            </View>


                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Service Description:</Text></View>
                                <View style={{ flex: 1 }}>
                                  <Textarea rowSpan={7} bordered placeholder="Type item description" onChangeText={(item_description) => this.setState({ item_description })} ref={'messageClear'} value={this.state.item_description} />
                                </View>
                                <View>
                                </View>
                              </View>
                            </View>


                            <View style={{ flex: 0.8, flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Min Price:</Text></View>

                                <View style={{ flex: 0.4 }}>
                                  <Input style={{ borderWidth: 1, borderColor: '#ddd' }} placeholder='Min Price' onChangeText={(min_price) => this.setState({ min_price })} keyboardType={'phone-pad'} value={this.state.min_price} />
                                </View>

                              </View>
                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Max Price:</Text></View>

                                <View style={{ flex: 0.6 }}>
                                  <Input style={{ borderWidth: 1, borderColor: '#ddd' }} placeholder='Max Price' onChangeText={(max_price) => this.setState({ max_price })} keyboardType={'phone-pad'} value={this.state.max_price} />
                                </View>

                              </View>
                            </View>

                            <View style={{ flex: 0.8, flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                              <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Unit of Measurement:</Text></View>

                                <View style={{ flex: 0.6 }}>
                                  <Item picker>
                                    <Picker
                                      mode="dropdown"
                                      iosIcon={<Icon name="arrow-down" />}
                                      style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                      placeholder="Select UOM"
                                      placeholderStyle={{ color: "#bfc6ea" }}
                                      placeholderIconColor="#007aff"
                                      selectedValue={this.state.uom_id}
                                      onValueChange={(itemValue, itemIndex) => this.onChangeUOMId(itemValue)}>{
                                        this.state.uomArray.map((v) => {
                                          return <Picker.Item label={v.uom_name} value={v.uom_id} />
                                        })
                                      }
                                    </Picker>
                                  </Item>
                                </View>

                              </View>
                            </View>

                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Special Information:</Text></View>
                                <View style={{ flex: 1 }}>
                                  <Textarea rowSpan={7} bordered placeholder="Type special information(if any)" onChangeText={(special_information) => this.setState({ special_information })} value={this.state.special_information} />
                                </View>
                                <View>
                                </View>
                              </View>
                            </View>

                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Terms & Conditions:</Text></View>
                                <View style={{ flex: 1 }}>
                                  <Textarea rowSpan={7} bordered placeholder="Terms & Conditions(if any)" onChangeText={(terms_condition) => this.setState({ terms_condition })} value={this.state.terms_condition} />
                                </View>
                                <View>
                                </View>
                              </View>
                            </View>

                            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }}>
                              <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10 }}>
                                {

                                  this.state.existingImages.map((y, index) => {
                                    return <View style={{ flex: 0.5, paddingLeft: 5 }}>
                                      <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                        <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => Alert.alert('Warning !', 'This image will be deleted!', [{ text: 'OK', onPress: () => this.deleteSellerServiceImage(y.image_id), style: 'cancel' },], { cancelable: true })} />
                                      </View>
                                      <Image
                                        source={{ uri: serverURL + 'admin/uploads/selleritem/' + y.image }}
                                        style={{ width: 100, height: 100 }}
                                      /></View>
                                  })
                                }
                              </View>
                            </View>

                            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }}>
                              <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10 }}>
                                {

                                  this.state.selectedImages.map((y, index) => {
                                    return <View style={{ flex: 0.5, paddingLeft: 5 }}>
                                      <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                        <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.delImg(index); }} />
                                      </View>
                                      <Image
                                        source={{ uri: y.uri }}
                                        style={{ width: 100, height: 100 }}
                                      /></View>
                                  })
                                }
                              </View>
                              <Button rounded title="Choose Photo" onPress={this.handleChoosePhoto} ><Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Upload Image</Text>
                              </Button>
                            </View>

                            <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                              <Button block rounded onPress={() => this.submitForm()}
                                style={{ bottom: 0, backgroundColor: '#14b351' }} >
                                <Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Submit</Text>
                              </Button>
                            </View>



                          </View>
                        </Form>


              }
            </Content>
          </View>



          <View style={{ flex: 0.10 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                  <Icon name="ios-home" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }

}


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertBox: {
    backgroundColor: '#1C97F7',
  },
  alertText: {
    fontSize: 12,
    color: '#ffffff',
  },
  conCard: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 20,
  },
  conCardItem: {
    marginLeft: 5,
    marginTop: 5,
  },
  conDetails: {
    fontSize: 15,
    color: 'black',
    marginLeft: 5,
  },
  postCard: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
  }
});