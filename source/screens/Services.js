import React from 'react';
import { View, Text, TouchableOpacity, Dimensions, StatusBar, FlatList, Image, ActivityIndicator, YellowBox } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Container, Content, List, ListItem, Thumbnail } from 'native-base';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

export default class CategoriesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      service: [],
      loading: true,
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',
    }
    this.CheckConnectivity();
  }
  componentDidMount() {
    /* StatusBar.setBackgroundColor('transparent');
      StatusBar.setBarStyle("dark-content")
     StatusBar.setTranslucent(false)*/
    this.getAllCategory();
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setTranslucent(true);

    });

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentWillUnmount() {
    this._navListener.remove();
  }

  getAllCategory = () => {
    const url = serverURL + "api/main";
    var method = [];
    method.push('getService');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var services = [];
        index = 0;
        var temp = res.result.service;
        while (parseInt(index) < parseInt(temp.length)) {
          var service = {
            'service_id': temp[index].id,
            'service_name': temp[index].service_category_name,
            'image': temp[index].image
          };
          services.push(service)
          index++;
        }

        this.setState({
          service: services,
          loading: false
        });
      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }
  render() {
    return (
      <Container>
        <View>
          {
            (this.state.serverError) ? (
              <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
            ) :
              (!this.state.connectState) ? (
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center', marginTop: 300 }}><Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} /></View>

                </View>
              ) :
                this.state.loading ? <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                  : null
          }
        </View>
        <View style={style.styles.categoriesHeader}>
          <View style={style.styles.signinHeaderContainer}>
            <View style={style.styles.categoriesHeaderContainerLeft}>
              <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
            </View>
            <View style={style.styles.categoriesHeaderContainerMiddle}>
              <Text style={style.styles.categoriesHeaderContainerMiddleText}> Services</Text>
            </View>
            <View style={style.styles.categoriesHeaderContainerRight}>
              <TouchableOpacity onPress={() => navigation.navigate({ routeName: 'Myakpoti' })}
                style={{ right: Platform.OS === 'ios' ? Dimensions.get("window").height < 667 ? '10%' : '5%' : '25%', backgroundColor: 'transparent', paddingLeft: 15 }}>
                <Icon name="search" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Content>
          <List>

            <FlatList
              data={this.state.service}
              keyExtractor={item => item.service_id}
              renderItem={({ item }) =>

                <ListItem onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'service', 'data': item, 'item': 1, cat_id: item.service_id })}>
                  <Thumbnail square size={80} source={{ uri: serverURL + 'admin/uploads/servicecategory/' + item.image }} />
                  <Text style={{ left: 10, fontSize: 16 }}>{`${item.service_name}`}</Text>

                </ListItem>
              }
            />


          </List>
        </Content>
      </Container>
    );
  }
}