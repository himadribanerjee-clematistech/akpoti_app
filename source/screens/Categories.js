import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, Dimensions, StatusBar, FlatList, ActivityIndicator, Image, } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Container, Content, List, ListItem, Thumbnail } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
const serverURL = 'http://agro.clematistech.com/';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

export default class CategoriesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [],
      loading: true,
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',
    }
    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {
    /* StatusBar.setBackgroundColor('transparent');
      StatusBar.setBarStyle("dark-content")
     StatusBar.setTranslucent(false)*/
    this.getAllCategory();
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setTranslucent(true);

    });

  }

  componentWillUnmount() {
    this._navListener.remove();
  }

  getAllCategory = () => {
    const url = serverURL + "api/main";
    var method = [];
    method.push('getCategoryItem');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var categories = [];
        index = 0;
        var temp = res.result.item;
        while (parseInt(index) < parseInt(temp.length)) {
          var category = {
            'category_id': temp[index].category_id,
            'category_name': temp[index].category_name,
            'image': temp[index].image
          };
          categories.push(category)
          index++;
        }
        this.setState({
          category: categories,
          loading: false
        });
      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }
  render() {
    return (
      <Container>
        <View style={style.styles.categoriesHeader}>
          <View style={style.styles.signinHeaderContainer}>
            <View style={style.styles.categoriesHeaderContainerLeft}>
              <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
            </View>
            <View style={style.styles.categoriesHeaderContainerMiddle}>
              <Text style={style.styles.categoriesHeaderContainerMiddleText}> Categories</Text>
            </View>
            <View style={style.styles.categoriesHeaderContainerRight}>
              <TouchableOpacity onPress={() => navigation.navigate({ routeName: 'Myakpoti' })}
                style={{ right: Platform.OS === 'ios' ? Dimensions.get("window").height < 667 ? '10%' : '5%' : '25%', backgroundColor: 'transparent', paddingLeft: 15 }}>
                <Icon name="search" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {
          (!this.state.connectState) ? (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
            </View>
          ) :
            this.state.loading ? (
              <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
            )
              :

              <Content>
                <List>

                  <FlatList
                    data={this.state.category}
                    keyExtractor={item => item.category_id}
                    renderItem={({ item }) =>

                      <ListItem onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'product', 'data': item, 'item': 1, cat_id: item.category_id })}>
                        <Thumbnail square size={80} source={{ uri: serverURL + 'admin/uploads/category/' + item.image }} />
                        <Text style={{ left: 10, fontSize: 16 }}>{`${item.category_name}`}</Text>

                      </ListItem>
                    }
                  />


                </List>
              </Content>
        }
      </Container>
    );
  }
}