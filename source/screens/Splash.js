import React, { Component } from 'react';
import { View, Text, StatusBar, Image, YellowBox } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

YellowBox.ignoreWarnings(["Warning:"]);

export default class Splash extends Component {
	constructor(props) {
		super(props);

	}

	routeChange() {
		setTimeout(() => {
			this.props.navigation.navigate('Home');
		}, 1500);
	}
	componentWillMount() {

		this._navListener = this.props.navigation.addListener('didFocus', () => {
			StatusBar.setBarStyle('dark-content');
			StatusBar.setBackgroundColor('transparent');
			StatusBar.setTranslucent(true);

		});
	}

	render() {
		return (
			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onLayout={() => this.routeChange()}>
				<View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, height: 200, justifyContent: 'center', alignItems: 'center', }}>
					<Image source={require('../assets/images/logo.png')} />
					<View style={{ paddingTop: 10, justifyContent: 'center', alignItems: 'center' }}>
						<Text style={{ fontSize: 12, color: '#D5D8DC', }}>
							Copyright <Icon name="copyright" size={15} /> 2019 Akpoti Marketplace. All Rights Reserved.
						</Text>
						<Text style={{ fontSize: 16, color: '#D5D8DC' }}>
							Powered By Akpoti
						</Text>
					</View>
				</View>
			</View>
		)
	}
}