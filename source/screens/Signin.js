import React from 'react';
import { View, Text, StatusBar, Dimensions, TouchableOpacity, Image, ActivityIndicator, YellowBox } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Content, Form, Item, Input, Label } from 'native-base';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

import * as style from '../config';

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showLoader: false,

            showNumText: false,
            numberMessage: '',
            showPassText: false,
            passwordMessage: '',
            errorText: false,
            errorMsg: '',

            showPass: true,
            icon: 'eye-off',
            userNumber: '',
            userpassWord: '',
            serverError: false,
            connectState: true,
            errorMsg: 'Something went wrong. Please try later.',
        }
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };

    // _loadInitialState = async () => {
    //     var tokenSaved = await AsyncStorage.getItem('@token');

    //     if (tokenSaved !== null) {
    //         this._profile();
    //     }
    // }

    // componentWillMount() {
    //     this._loadInitialState().done();
    // }

    back = () => {
        this.props.navigation.navigate('Home');
    }

    _showPassword = () => {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            showPass: !prevState.showPass
        }))
    }

    _register = () => {
        this.props.navigation.navigate('Register');
    }

    _login = () => {
        try {
            this.setState({ showLoader: true });

            let userNumber = this.state.userNumber;
            let userpassWord = this.state.userpassWord;

            if (userNumber == '') {
                this.setState({
                    showLoader: false,
                    showNumText: true,
                    numberMessage: 'Contact Number is Required !!',
                });
            }
            else if (userpassWord == '') {
                this.setState({
                    showLoader: false,
                    showNumText: false,
                    showPassText: true,
                    passwordMessage: 'Password is Mandatory!! ',
                });
            }
            else {
                this.setState({
                    showNumText: false,
                    showPassText: false,
                });

                fetch(serverURL + 'api/login/doLogin', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        user_name: this.state.userNumber,
                        password: this.state.userpassWord,
                    }),
                })
                    .then(response => {
                        const status = response.status;
                        const data = response.json();

                        return Promise.all([status, data]);
                    })
                    .then((responseJson) => {
                        if (responseJson[0] == 404) {
                            this.setState({
                                showLoader: false,
                                errorText: true,
                                errorMsg: responseJson[1].msg,
                            });
                        }
                        else if (responseJson[0] == 200) {
                            AsyncStorage.clear();
                            AsyncStorage.setItem('@token', responseJson[1].token);

                            this.props.navigation.replace("Home");

                        }
                        else {
                            this.setState({
                                showLoader: false,
                                errorText: true,
                                errorMsg: 'Server Is Too Busy Right Now. Please Check Again Later',
                            });
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        return (
            <View style={style.styles.container}>

                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <View style={style.styles.signinHeader}>
                    <View style={style.styles.signinHeaderContainer}>
                        <View style={style.styles.signinHeaderContainerLeft}>
                            <TouchableOpacity onPress={this.back}>
                                <Icon name="ios-close" style={{ fontSize: 50, color: '#000', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={style.styles.signinHeaderContainerRight}>
                            <Text style={style.styles.signinHeaderContainerRightText}> Sign In</Text>
                        </View>
                    </View>
                </View>

                {
                    (this.state.serverError) ? (
                        <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                    ) :
                        (!this.state.connectState) ? (
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                            </View>
                        ) :
                            <View style={{ flex: 1, top: (Height * 2 / 100), }}>

                                {
                                    this.state.showLoader
                                        ?
                                        <View style={{ backgroundColor: '#EAFAF1', justifyContent: 'center', alignItems: 'center', margin: 20, padding: 5 }}>
                                            <ActivityIndicator size="large" color="#ffa500" />
                                            <Text>Please Wait..</Text>
                                        </View>


                                        :
                                        null
                                }

                                {
                                    this.state.errorText
                                        ?
                                        <View style={{ backgroundColor: '#FBEEE6', justifyContent: 'center', alignItems: 'center', margin: 20, padding: 5, height: (Height * 6 / 100), }}>
                                            <Text style={{ color: '#FF3333', fontSize: 18 }}> {this.state.errorMsg} </Text>
                                        </View>


                                        :
                                        null
                                }

                                <Content>
                                    <Form style={{ height: (Height * 35 / 100), }}>
                                        <Item floatingLabel style={{ width: (Width * 90 / 100) }}>
                                            <Label>User Contact Number</Label>
                                            <Input
                                                keyboardType='phone-pad'
                                                returnKeyType='done'
                                                autoCorrect={false}
                                                ref={"txtUnum"}
                                                //onSubmitEditing={ () => this.refs.txtPass.focus() }
                                                onChangeText={userNumber => this.setState({ userNumber })}
                                            />

                                        </Item>

                                        {
                                            this.state.showNumText
                                                ?
                                                <View>
                                                    <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > {this.state.numberMessage} </Text>
                                                </View>
                                                :
                                                null
                                        }

                                        <Item floatingLabel style={{ width: (Width * 90 / 100), }}>
                                            <Label>Password</Label>
                                            <Input
                                                returnKeyType='done'
                                                autoCorrect={false}
                                                secureTextEntry={this.state.showPass}
                                                ref={"txtPass"}
                                                onChangeText={userpassWord => this.setState({ userpassWord })}
                                            />

                                            <Icon active name={this.state.icon} onPress={this._showPassword} />

                                        </Item>

                                        {
                                            this.state.showPassText
                                                ?
                                                <View>
                                                    <Text style={{ paddingTop: 5, textAlign: 'left', color: 'red', left: 15 }} > {this.state.passwordMessage} </Text>
                                                </View>
                                                :
                                                null
                                        }

                                        <View style={{ justifyContent: 'center', alignItems: 'center', top: 40 }}>
                                            <TouchableOpacity onPress={this._login} style={{ height: 40, width: (Width * 90 / 100), borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, }}>
                                                <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}>SIGN IN</Text>
                                            </TouchableOpacity>
                                        </View>

                                    </Form>

                                    {/* <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ color: '#ABB2B9', textAlign: 'center', fontSize: 12, }}>Forgot Password?</Text>
                            </View> */}

                                    <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 10 }}>
                                        <Text style={{ color: '#ABB2B9', textAlign: 'center', fontSize: 12, }}>Don't Have an Account? <Text style={{ color: '#ffa500', fontSize: 14, }} onPress={() => this._register()}>Register</Text></Text>
                                    </View>

                                </Content>
                            </View>
                }




            </View>
        )
    }
}