import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator, Alert, Image, FlatList, YellowBox } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Container, Content, Item, Button, Input, Form, Textarea, CheckBox, ListItem, Picker, } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const createFormData = (photo, body) => {
  const data = new FormData();

  photo.forEach((item, i) => {
    data.append("image[]", {
      uri: Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
      type: item.type,
      name: item.filename || `filename${i}.jpg`,
      //data: item.data
    });
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};

export default class SendRfq extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isSubmited: false,
      validForm: true,
      customer_id: null,
      seller_id: null,
      selectedImages: [],
      rfqSearchKeyWord: '',
      serverData: [],
      displayText: '',
      uomArray: [],
      uom_id: '',
      quantity: 0,
      message: 'Dear Sir/Madam, I\'m looking for products with the following specifications:',
      other_requerment: '',
      checkboxTerms: false,
      item_id: '',
      cat_id: '',
      subcat_id: '',
      species_id: '',
      buyer_id: '',
      rfq_type: '',
      noInternet: false,
      errorMsg: 'Network connection occured. Please try later.',
      showLoginButton: false,


    }

    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
      quality: 0.3
    }
    ImagePicker.launchImageLibrary(options, response => {
      var fileSizeInGb = parseInt(Math.floor(Math.log(response.fileSize) / Math.log(1024)));
      //alert(fileSizeInGb);
      if (fileSizeInGb > 2) {
        Alert.alert(
          'Oops !',
          'Image size can\'t be greater than 2 MB!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      } else {
        if (response.uri) {
          var selectedImg = [];
          this.state.selectedImages.map((y) => {
            selectedImg.push(y);
          })
          //alert(response.fileSize);
          selectedImg.push(response);
          this.setState({ selectedImages: selectedImg })

          //console.log(this.state.selectedImages)
        }
      }

    })
  }

  componentDidMount() {
    //alert(global_LoggedInCustomerName+'='+global_LoggedInCustomerId);
    if (global_isLoggedIn) {
      this.setState({
        errorMsg: '',
        showLoginButton: false

      });
    } else {

      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }
    this.setState({
      customer_id: global_LoggedInCustomerId
    });

  }

  rfqSearchKeyWord = async (text) => {

    const savedToken = await AsyncStorage.getItem('@token');
    this.setState({
      rfqSearchKeyWord: text,
      displayText: text
    });
    if (text != '') {

      this.setState({
        serverData: []
      });

      let url = serverURL + "api/quotation/items";

      var data = {
        keyword: text
      };
      fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {

          if (res.status === 200) {
            var item = res.result;
            this.setState({
              serverData: item
            });
          }

        })
        .catch(error => {
          // alert(JSON.stringify(error))
        })
    } else {
      this.setState({
        serverData: []
      });
    }
  }

  onChangeUOMId(value) {
    this.setState({
      uom_id: value
    });
  }

  toggleTerms() {
    this.setState({ checkboxTerms: !this.state.checkboxTerms });
  }

  delImg(ind) {
    //console.log(ind);
    var selectedImg = [];
    this.state.selectedImages.map((y, index) => {
      if (index == ind) {
        // Skip img
      } else {
        selectedImg.push(y);
      }

    })
    this.setState({ selectedImages: selectedImg })
  }

  selectRFQ(item) {
    this.setState({ serverData: [] })
    //console.log(item);
    if (item.type == 1) {
      let displayText = item.category_name + ' > ' + item.subcat_name + ' > ' + item.item_name + ' > ' + item.species_name;
      this.setState({
        displayText: displayText,
        serverData: [],
        uomArray: item.uom_list,
        cat_id: item.category_id,
        subcat_id: item.subcat_id,
        species_id: item.species_id,
        item_id: item.id,
        rfq_type: 1
      })
    } else if (item.type == 2) {
      let displayText = item.service_category_name + ' > ' + item.name;
      this.setState({
        displayText: displayText,
        serverData: [],
        uomArray: item.uom_list,
        cat_id: item.id,
        subcat_id: '',
        species_id: '',
        item_id: item.service_item_id,
        rfq_type: 2
      })
    }
  }

  submitRfqQuoteForm = async () => {
    var submit = true;
    let savedToken = await AsyncStorage.getItem('@token');

    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        if (global_isLoggedIn) {
          if (this.state.species_id == '' && this.state.rfq_type == 1) {
            Alert.alert(
              'Oops !',
              'Something went wrong!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
          if (this.state.uom_id == '') {
            Alert.alert(
              'Oops !',
              'Unit of measurement is missing! Please select an item.', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
          if (this.state.quantity == '') {
            Alert.alert(
              'Oops !',
              'Quantity is missing!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
          if (this.state.quantity < 1) {
            Alert.alert(
              'Oops !',
              'Invalid Quantity', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
          if (this.state.message == '') {
            Alert.alert(
              'Oops !',
              'Specify your requirement!', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
            submit = false;
            return false;
          }
          if (!this.state.checkboxTerms) {
            Alert.alert(
              'Oops !',
              'Please agree with terms & conditions', [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
              { cancelable: false })
          }
          if (submit) {
            this.setState({ isSubmited: true });
            let url = serverURL + "api/rfq/addRFQ";
            //console.log(url);
            //console.log(savedToken);


            fetch(url, {
              method: "POST",
              headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: savedToken,
                'credentials': 'same-origin'
              },
              timeout: 0,
              compress: true,
              body: createFormData(this.state.selectedImages, {
                customer_id: this.state.customer_id,
                uom_id: this.state.uom_id,
                quantity: this.state.quantity,
                message: this.state.message,
                other_requerment: this.state.other_requerment,
                item_id: this.state.item_id,
                cat_id: this.state.cat_id,
                subcat_id: this.state.subcat_id,
                species_id: this.state.species_id,
                buyer_id: this.state.customer_id,
                type: this.state.rfq_type
              })
            }).then(res => res.json())
              .then(res => {
                //console.log(res);
                if (res.status == 200) {
                  Alert.alert(
                    'Success !',
                    res.msg, [
                    { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
                    { cancelable: false })
                } else {
                  Alert.alert(
                    'Success !',
                    res.msg, [
                    { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
                    { cancelable: false })
                  this.setState({
                    isSubmited: false,
                  })
                }

              })
              .catch(err => {
                console.error("error uploading images: ", err);
              });
          }
        } else {
          this.setState({
            errorMsg: 'Please login to view this section.',
            showLoginButton: true

          });
        }
      } else {
        Alert.alert(
          'Oops !',
          'Seems like you don\'t have an active internet connection!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      }
    });

  }

  nextAction() {
    this.setState({
      loading: false,
      isSubmited: false,
      validForm: true,
      customer_id: null,
      seller_id: null,
      selectedImages: [],
      rfqSearchKeyWord: '',
      serverData: '',
      displayText: '',
      uomArray: [],
      uom_id: '',
      quantity: 0,
      message: 'Dear Sir/Madam, I\'m looking for products with the following specifications:',
      other_requerment: '',
      checkboxTerms: false,
      item_id: '',
      cat_id: '',
      subcat_id: '',
      species_id: '',
      buyer_id: '',
      rfq_type: '',
      displayQuantity: true
    })
  }



  render() {
    console.log(this.state.serverData);
    const { selectedImages } = this.state
    return (
      <Container>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 17, color: '#666', paddingTop: 35, paddingBottom: 10 }}>Request For Quotation</Text></View>
                <View style={style.styles.productListHeaderContainerRight}></View>
                <View style={style.styles.productListHeaderContainerRighttwo}>


                </View>
              </View>
            </View>

          </View>
          <View style={{ flex: 0.80 }} >
            <Content>
              {
                (this.state.serverError) ? (
                  <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                ) :
                  (!this.state.connectState) ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                    </View>
                  ) :
                    (this.state.showLoginButton) ? (
                      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                          <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                          </Button>
                        </View>
                      </View>
                    ) :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :

                        <Form>
                          <View style={{ paddingLeft: 2, paddingRight: 2, marginTop: 10 }}>
                            <Item>
                              <Input placeholder="Keywords of items you are looking for" placeholderTextColor="grey" onChangeText={(rfqSearchKeyWord) => this.setState({ rfqSearchKeyWord }, () => this.rfqSearchKeyWord(this.state.rfqSearchKeyWord))} value={(this.state.displayText != '') ? this.state.displayText : this.state.rfqSearchKeyWord} />
                              <Icon active name='search' />
                            </Item>
                            <View style={{ paddingLeft: 2, paddingRight: 2, marginTop: 10 }}>
                              <FlatList
                                data={this.state.serverData}
                                keyExtractor={item => item.id}
                                renderItem={({ item }) =>
                                  <View>
                                    {
                                      item.type == 1 ?
                                        <ListItem onPress={() => this.selectRFQ(item)}>
                                          <Text style={{ color: '#000', paddingTop: 4 }}> {`${item.category_name}`} > {`${item.subcat_name}`} > {`${item.item_name}`} > {`${item.species_name}`}</Text>
                                        </ListItem>
                                        :
                                        item.type == 2 ?
                                          <ListItem onPress={() => this.selectRFQ(item)}>
                                            <Text style={{ color: '#000', paddingTop: 4 }}> {`${item.service_category_name}`} > {`${item.name}`}</Text>
                                          </ListItem>
                                          : null
                                    }

                                  </View>


                                }
                              />
                            </View>
                            <View style={{ flex: 0.8, flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingTop: 5 }}>

                              <View style={{ flex: 0.5, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Quantity:</Text></View>

                                <View style={{ flex: 0.4 }}>
                                  <Input style={{ borderWidth: 1, borderColor: '#ddd' }} placeholder='Quantity' onChangeText={(quantity) => this.setState({ quantity })} ref={'quantityClear'} keyboardType={'phone-pad'} value={this.state.quantity} />
                                </View>

                              </View>
                              <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 15 }}>
                                <View style={{ flex: 0.2, paddingBottom: 5 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Unit of Measurement:</Text></View>
                                <View style={{ flex: 0.6 }}>
                                  <Item picker>
                                    <Picker
                                      mode="dropdown"
                                      iosIcon={<Icon name="arrow-down" />}
                                      style={{ paddingLeft: 10, marginLeft: 10, marginTop: 0 }}
                                      placeholder="Select UOM"
                                      placeholderStyle={{ color: "#bfc6ea" }}
                                      placeholderIconColor="#007aff"
                                      selectedValue={this.state.uom_id}
                                      onValueChange={(itemValue, itemIndex) => this.onChangeUOMId(itemValue)}>{
                                        this.state.uomArray.map((v) => {
                                          return <Picker.Item label={v.uom_name} value={v.uom_id} />
                                        })
                                      }
                                    </Picker>
                                  </Item>
                                </View>
                              </View>
                            </View>
                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Message:</Text></View>
                                <View style={{ flex: 1 }}>
                                  <Textarea rowSpan={7} bordered placeholder="Type you message" onChangeText={(message) => this.setState({ message })} ref={'messageClear'} value={this.state.message} />
                                </View>
                                <View>
                                </View>
                              </View>
                            </View>
                            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }}>
                              <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10 }}>
                                {

                                  this.state.selectedImages.map((y, index) => {
                                    return <View style={{ flex: 0.5, paddingLeft: 5 }}>
                                      <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                        <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.delImg(index); }} />
                                      </View>
                                      <Image
                                        source={{ uri: y.uri }}
                                        style={{ width: 100, height: 100 }}
                                      /></View>
                                  })
                                }
                              </View>
                              <Button rounded title="Choose Photo" onPress={this.handleChoosePhoto} ><Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Upload attachments</Text>
                              </Button>
                            </View>
                            <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10, paddingTop: 25 }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Other Requirements:</Text></View>
                                <View style={{ flex: 1 }}>
                                  <Textarea rowSpan={7} bordered placeholder="Type you message" onChangeText={(other_requerment) => this.setState({ other_requerment })} ref={'messageClear'} value={this.state.other_requerment} />
                                </View>
                                <View>
                                </View>
                              </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                              <View style={{ flex: 0.1 }}>
                                <CheckBox style={{ marginTop: 7 }} color={"#f0ad4e"} selectedColor={"#5cb85c"} onPress={this.toggleTerms.bind(this)} checked={this.state.checkboxTerms} />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={{ paddingTop: 1, textAlign: 'left', fontSize: 14 }} > I have read, understood and agreed to abide by Terms and Conditions Governing RFQ. </Text>
                              </View>
                            </View>
                            {
                              this.state.isSubmited ?
                                <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 30 }}>
                                  <Button bordered rounded success >
                                    <Text style={{ color: '#14b351', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Sending request. Please wait...</Text>
                                  </Button>
                                </View>
                                :

                                <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 30 }}>
                                  <Button block rounded onPress={() => this.submitRfqQuoteForm()}
                                    style={{ bottom: 0, backgroundColor: '#14b351' }} >
                                    <Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Send</Text>
                                  </Button>
                                </View>
                            }
                          </View>
                        </Form>


              }
            </Content>
          </View>



          <View style={{ flex: 0.10 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('home')}  >
                  <Icon name="ios-home" style={{ color: 'white' }} />
                  <Text style={{ color: 'white' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Myakpoti')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }

}


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertBox: {
    backgroundColor: '#1C97F7',
  },
  alertText: {
    fontSize: 12,
    color: '#ffffff',
  },
  conCard: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 20,
  },
  conCardItem: {
    marginLeft: 5,
    marginTop: 5,
  },
  conDetails: {
    fontSize: 15,
    color: 'black',
    marginLeft: 5,
  },
  postCard: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
  }
});