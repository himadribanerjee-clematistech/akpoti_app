import React from 'react';
import { YellowBox, SafeAreaView, View, Text, Dimensions, TouchableOpacity, ActivityIndicator, Image, FlatList } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Footer, FooterTab, Button, Container, Content, Card, CardItem, Body, } from 'native-base';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
const regex = /(<([^>]+)>)/ig;

export default class Newslist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      serverData: [],
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',

    }

    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {

    this.getNewsListData();

  }


  getNewsListData = () => {
    const url = serverURL + "api/news/getNews";


    var data = {
      resultPerPage: '',
    };
    //console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res)  
        var data = res.result;

        if (res.status === 200) {

          this.setState({
            serverData: res.news
          });
          this.setState({
            loading: false
          });
        } else {
          this.setState({
            serverError: true,
            errorMsg: res.msg

          });
        }

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })

  }

  Item = ({ title }) => {
    return (
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }




  render() {
    //console.log(this.state.serverData); 
    const serverData = Object.values(this.state.serverData);

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 0.10 }} >
              <View style={style.styles.categoriesHeader}>
                <View style={style.styles.signinHeaderContainer}>
                  <View style={style.styles.categoriesHeaderContainerLeft}>
                    <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                  </View>
                  <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>All News</Text></View>
                  <View style={style.styles.productListHeaderContainerRight}></View>
                  <View style={style.styles.productListHeaderContainerRighttwo}>


                  </View>
                </View>
              </View>
            </View>
            {
              (this.state.serverError) ? (
                <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
              ) :
                (!this.state.connectState) ? (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                  </View>
                ) :
                  this.state.loading ? (
                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                  ) :

                    <View style={{ flex: 0.9 }} >
                      <Content>
                        <FlatList
                          data={serverData}
                          renderItem={({ item }) =>
                            <Card style={{ flex: 0 }}>
                              <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsDetails', { item: item })} >
                                <CardItem>
                                  <Body>
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                      <Text numberOfLines={2} style={{ fontWeight: 'bold', textAlign: 'left', fontSize: 20, color: '#148db3', paddingTop: 10, flex: 1 }}>{`${item.title}`}</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', paddingTop: 10 }}>
                                      <View style={{ width: 30 }}><Icon name="ios-calendar" /></View>
                                      <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', paddingTop: 5 }} >{`${item.date}`}</Text></View>
                                    </View>
                                  </Body>
                                </CardItem>
                                <CardItem>
                                  <Body>
                                    <Image source={{ uri: serverURL + 'admin/uploads/news/' + item.image[0] }} style={{ height: 200, width: 320, flex: 1 }} />
                                    <Text numberOfLines={3} style={{ textAlign: 'left', fontSize: 15, color: '#666', paddingTop: 10, flex: 1 }}>
                                      {
                                        item.description = item.description.replace(regex, '')
                                      }
                                      {`${item.description}`}
                                    </Text>
                                  </Body>
                                </CardItem>
                              </TouchableOpacity>
                            </Card>
                          }
                          keyExtractor={item => item.id}
                        />
                      </Content>
                    </View>
            }

            {
              this.state.loading ? (
                <Text style={{ color: 'white' }}></Text>
              ) :
                <View style={{ flex: 0.08, marginBottom: 0, paddingBottom: 0 }} >
                  <Footer style={{ height: '100%' }} >
                    <FooterTab style={{ backgroundColor: '#000' }}>
                      <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                        <Icon name="ios-home" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Home</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                        <Icon name="ios-person" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Myakpoti</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                        <Icon name="ios-list" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Categories</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                        <Icon name="ios-cog" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Services</Text>
                      </Button>
                      <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                        <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                        <Text style={{ color: 'grey' }}>Messenger</Text>
                      </Button>
                    </FooterTab>
                  </Footer>
                </View>
            }
          </View>
        </Container>
      </SafeAreaView>
    );
  }

}
