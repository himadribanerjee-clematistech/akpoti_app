import React from 'react';
import { YellowBox, View, Text, StatusBar, Dimensions, TouchableOpacity, Alert, KeyboardAvoidingView, Modal, Image, ScrollView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Content, Form, Item, Input, Label, Button } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;
const createFormData = (photo, body) => {
    const data = new FormData();

    photo.forEach((item, i) => {
        data.append("business_logo[]", {
            uri: Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
            type: item.type,
            name: item.fileName || `filename${i}.jpg`,
            size: item.fileSize
        });
    });

    Object.keys(body).forEach(key => {
        data.append(key, body[key]);
    });

    return data;
};

export default class UpgradeSeller extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showNameText: false,
            nameText: null,
            showCityText: false,
            cityText: null,
            showStateText: false,
            stateText: null,
            showContactNameText: false,
            contactNameText: null,
            showContactNumberText: false,
            contactNumberText: null,
            showAddressText: false,
            addressText: null,
            showImageText: false,
            imageText: null,

            bname: null,
            bcity: null,
            bstate: null,
            bcontactname: null,
            bcontactnum: null,
            baddress: null,
            selectedImages: [],
            serverError: false,
            connectState: true,
            errorMsg: 'Something went wrong. Please try later.',
        }

        this.CheckConnectivity();

    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.setState({
                        connectState: true,
                    });
                } else {
                    this.setState({
                        connectState: false,
                    });
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
                "connectionChange",
                this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.setState({
                connectState: false,
            });
        } else {
            this.setState({
                connectState: true,
            });
        }
    };


    handleChoosePhoto = () => {
        const options = {
            noData: true,
            quality: 0.3
        }

        ImagePicker.launchImageLibrary(options, response => {
            var fileSizeInGb = parseInt(Math.floor(Math.log(response.fileSize) / Math.log(1024)));
            if (fileSizeInGb > 2) {
                Alert.alert(
                    'Oops !',
                    'Image size can\'t be greater than 2 MB!', [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
                    { cancelable: false }
                )
            }
            else {
                if (response.uri) {
                    var selectedImg = [];
                    this.state.selectedImages.map((y) => {
                        selectedImg.push(y);
                    });

                    selectedImg.push(response);
                    this.setState({ selectedImages: selectedImg })
                    //console.log(this.state.selectedImages);
                }
            }
        })
    }

    delImg(ind) {
        //console.log(ind);
        var selectedImg = [];
        this.state.selectedImages.map((y, index) => {
            if (index == ind) {
                // Skip img
            } else {
                selectedImg.push(y);
            }

        })
        this.setState({ selectedImages: selectedImg })
    }

    _apply = async () => {
        let savedToken = await AsyncStorage.getItem('@token');
        let customer_id = await AsyncStorage.getItem('@customer_id');

        let BName = this.state.bname;
        let BCity = this.state.bcity;
        let BState = this.state.bstate;
        let BCName = this.state.bcontactname;
        let BCNum = this.state.bcontactnum;
        let BAddr = this.state.baddress;
        let BDocument = this.state.selectedImages;

        //console.log(this.state.selectedImages);

        if (BName == null) {
            this.setState({
                showNameText: true,
                nameText: 'Bussiness Name Should Not Be Blank',
            })
        }
        else if (BCity == null) {
            this.setState({
                showNameText: false,
                showCityText: true,
                cityText: 'Bussiness City Should Not Be Blank'
            })
        }
        else if (BState == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: true,
                stateText: 'Bussiness State Should Not Be Blank'
            })
        }
        else if (BCName == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: true,
                contactNameText: 'Bussiness Contact Name Should Not Be Blank'
            })
        }
        else if (BCNum == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: false,
                showContactNumberText: true,
                contactNumberText: 'Bussiness Contact Number Should Not Be Blank'
            })
        }
        else if (BAddr == null) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: false,
                showContactNumberText: false,
                showAddressText: true,
                addressText: 'Bussiness Address Should Not Be Blank'
            })
        }
        else if (BDocument == []) {
            this.setState({
                showNameText: false,
                showCityText: false,
                showStateText: false,
                showContactNameText: false,
                showContactNumberText: false,
                showAddressText: false,
                showImageText: true,
                imageText: 'Bussiness Document Should Not Be Blank'
            })
        }
        else {
            fetch(serverURL + 'api/seller/UpdateToSeller', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': savedToken,
                    'credentials': 'same-origin'
                },
                timeout: 0,
                compress: true,
                body: createFormData(this.state.selectedImages, {
                    customer_id: customer_id,
                    business_name: BName,
                    address: BAddr,
                    state: BState,
                    city: BCity,
                    contact_name: BCName,
                    contact_number: BCNum,
                })
            })
                .then(response => {
                    const status = response.status;
                    const data = response.json();

                    return Promise.all([status, data]);
                })
                .then((responseJson) => {
                    //console.log(responseJson);
                    if (responseJson[0] == 200) {
                        if (responseJson[1].msg !== '') {
                            Alert.alert('Success Message !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                        else {
                            Alert.alert('Success Message with Error !!', responseJson[1].msg, [{ text: 'OK', onPress: () => this.props.navigation.goBack() }], { cancelable: false });
                        }
                    }
                    else {
                        Alert.alert('Error Message !!', responseJson[1].msg);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }


    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#F7F9F9' }}>

                <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />

                <View style={{ width: Width, height: (Height * 12 / 100), backgroundColor: '#FFF', borderColor: '#ddd', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 3, elevation: 5, }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <Icon name="ios-arrow-back" style={{ fontSize: 30, color: '#000', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.9, justifyContent: 'center', alignItems: 'center', paddingTop: 40, }}>
                            <Text style={{ fontSize: 20, }}> Upgrade To Seller</Text>
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingBottom: 10, }}>
                    {/* <KeyboardAvoidingView style={{ flex: 1, top: (Height * 2 / 100), justifyContent: 'center', alignItems: 'center', marginBottom: 20, }} behavior="padding" enabled> */}
                    <Content>
                        <Form style={{ width: (Width * 90 / 100), }}>
                            <Item floatingLabel style={{ width: (Width * 95 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, marginLeft: 0 }}>
                                <Label>Bussiness Name</Label>
                                <Input
                                    style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                    returnKeyType='done'
                                    autoCorrect={false}
                                    onChangeText={bname => this.setState({ bname })}
                                />
                            </Item>

                            {
                                this.state.showNameText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.nameText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <Item floatingLabel style={{ width: (Width * 95 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, marginLeft: 0 }}>
                                <Label>City</Label>
                                <Input
                                    style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                    returnKeyType='done'
                                    autoCorrect={false}
                                    onChangeText={bcity => this.setState({ bcity })}
                                />
                            </Item>

                            {
                                this.state.showCityText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.cityText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <Item floatingLabel style={{ width: (Width * 95 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, marginLeft: 0 }}>
                                <Label>State</Label>
                                <Input
                                    style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                    returnKeyType='done'
                                    autoCorrect={false}
                                    onChangeText={bstate => this.setState({ bstate })}
                                />
                            </Item>

                            {
                                this.state.showStateText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.stateText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <Item floatingLabel style={{ width: (Width * 95 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, marginLeft: 0 }}>
                                <Label>Contact Name</Label>
                                <Input
                                    style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                    returnKeyType='done'
                                    autoCorrect={false}
                                    onChangeText={bcontactname => this.setState({ bcontactname })}
                                />
                            </Item>

                            {
                                this.state.showContactNameText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.contactNameText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <Item iconLeft floatingLabel style={{ width: (Width * 95 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, marginLeft: 0 }}>
                                <Icon name="ios-call" style={{ fontSize: 18, color: '#e68c04', borderRightColor: '#e68c04', borderRightWidth: 1, marginTop: 10, }} >
                                    <Text style={{ marginRight: 3, fontSize: 14, }}>+234</Text>
                                </Icon>

                                <Label style={{ left: (Width * 10 / 100) }}>Contact Number</Label>
                                <Input
                                    style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                    keyboardType='number-pad'
                                    returnKeyType='done'
                                    autoCorrect={false}
                                    onChangeText={bcontactnum => this.setState({ bcontactnum })}
                                />
                            </Item>

                            {
                                this.state.showContactNumberText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.contactNumberText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <Item floatingLabel style={{ width: (Width * 95 / 100), borderBottomColor: '#e68c04', borderBottomWidth: 1, marginLeft: 0 }}>
                                <Label>Bussiness Address</Label>
                                <Input
                                    style={{ height: 50, paddingHorizontal: 10, borderRadius: 5, fontSize: 18 }}
                                    returnKeyType='done'
                                    autoCorrect={false}
                                    onChangeText={baddress => this.setState({ baddress })}
                                />
                            </Item>

                            {
                                this.state.showAddressText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.addressText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <View style={{ width: (Width * 95 / 100), paddingTop: 15, flexDirection: 'row', }} >
                                <Button iconLeft transparent style={{ width: (Width * 60 / 100), height: 30, }} onPress={() => this.handleChoosePhoto()}>
                                    <Icon name='ios-attach' style={{ color: '#000' }} />
                                    <Text style={{ color: '#000', textAlign: 'center', fontSize: 14, }}> Choose Document To Be A Seller  </Text>
                                </Button>
                            </View>
                            <View style={{ paddingLeft: 10, paddingTop: 5 }}>
                                <Text style={{ fontSize: 12, color: '#E74C3C', paddingLeft: (Width * 2 / 100), }}>[ Only jpg/png allowed ]</Text>
                            </View>

                            <ScrollView horizontal >
                                <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10 }}>
                                    {
                                        this.state.selectedImages.map((y, index) => {
                                            return <View style={{ paddingLeft: 5 }}>
                                                <Image
                                                    source={{ uri: y.uri }}
                                                    style={{ width: 100, height: 100 }}
                                                />
                                                <View style={{ position: 'absolute', zIndex: 1, top: -10, left: 2 }}>
                                                    <Icon name="ios-close-circle" style={{ fontSize: 30, color: 'orange', }} onPress={() => this.delImg(index)} />
                                                </View>
                                            </View>
                                        })
                                    }
                                </View>
                            </ScrollView>


                            {
                                this.state.showImageText
                                    ?
                                    <View style={{ marginBottom: 10, marginTop: 5 }}>
                                        <Text style={{ textAlign: 'left', color: 'red', left: 5 }} > {this.state.imageText} </Text>
                                    </View>
                                    :
                                    null
                            }

                            <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, justifyContent: 'center', alignItems: 'center', marginBottom: 15 }} onPress={() => this._apply()}>
                                <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}> Apply </Text>
                            </TouchableOpacity>

                        </Form>
                    </Content>
                    {/* </KeyboardAvoidingView> */}
                </View>

            </View>
        )
    }
}