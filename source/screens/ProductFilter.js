import React from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, StatusBar, FlatList, TextInput, AsyncStorage, YellowBox } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Container, Content, ListItem, Radio, Footer, FooterTab, Button } from 'native-base';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

export default class ProductFilterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [],
      type: 2,
      minorder: null,
      minPrice: null,
      maxprice: null,
      category_id: null,
      subcat_id: null,
      typep: this.props.navigation.state.params.type,
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',
    }

    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  setsubcat(item, index) {
    this.setState({
      subcat_id: item.subcat_id
    });
  }
  toggle(item, index1) {
    if (this.state.typep != 'service') {


      index = 0;
      var categories = [];
      var temp = this.state.category;
      while (parseInt(index) < parseInt(temp.length)) {
        var category = {
          'category_id': temp[index].category_id,
          'category_name': temp[index].category_name,
          'image': temp[index].image,
          'subcategory': temp[index].subcategory,
        };
        if (item.category_id == temp[index].category_id) {
          category.icon = (temp[index].icon == 'minus') ? 'plus' : 'minus';
          if (temp[index].icon == 'minus') {
            item.category_id = null;
          }
        } else {
          category.icon = 'plus';
        }
        categories.push(category)
        index++;
      }
      this.setState({
        category: categories,
        category_id: item.category_id
      });
    } else {
      this.setState({
        category_id: item.category_id
      });
    }

  }
  async componentDidMount() {
    /* StatusBar.setBackgroundColor('transparent');
      StatusBar.setBarStyle("dark-content")
     StatusBar.setTranslucent(false)*/
    if (this.props.navigation.state.params.type == 'service') {
      this.setState({
        category: this.props.navigation.state.params.serviceCategorylist
      });

    } else {

      this.getAllCategory();

    }


    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);

    var suppliertype = (data && data != null) ? data.suppliertype : null;
    var Minorder = (data && data != null) ? data.minorder : null;
    var Minprice = (data && data != null) ? data.minPrice : null;
    var maxprice = (data && data != null) ? data.maxPrice : null;
    var category_id = (data && data != null) ? data.category_id : null;
    var subcat_id = (data && data != null) ? data.subcat_id : null;

    this.setState({
      type: suppliertype,
      minorder: Minorder,
      minPrice: Minprice,
      maxPrice: maxprice,
      category_id: category_id,
      subcat_id: subcat_id
    });
    //console.log("test1",this.state,)
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setTranslucent(true);

    });

  }

  componentWillUnmount() {
    this._navListener.remove();
  }

  getAllCategory = () => {
    const url = serverURL + "api/main";
    var method = [];
    method.push('getCategoryItem');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var categories = [];
        index = 0;
        var temp = res.result.item;
        while (parseInt(index) < parseInt(temp.length)) {
          var category = {
            'category_id': temp[index].category_id,
            'category_name': temp[index].category_name,
            'image': temp[index].image,
            'icon': (temp[index].category_id == this.state.category_id) ? 'minus' : 'plus',
            'subcategory': temp[index].subcategory,
          };
          categories.push(category)
          index++;
        }
        this.setState({
          category: categories
        });

      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }

  typeset = (type) => {
    this.setState({
      type: type
    });


  };

  onChangeMinOrder = (minorder) => {
    this.setState({
      minorder: minorder
    });
  }
  onChangeMinPrice = (price) => {
    this.setState({
      minPrice: price
    });




  }
  onChangeMaxPrice = (price) => {
    this.setState({
      maxPrice: price
    });


  }
  setSearchFilter = async () => {
    try {


      if (parseFloat(this.state.minPrice) > parseFloat(this.state.maxPrice)) {
        //  console.log(this.state.minPrice,this.state.maxPrice)
        Toast.show('Minimum Price Should Be Less Than OR Equal To Maximum Price .', Toast.LONG, Toast.TOP);
      } else {
        var data = {
          suppliertype: this.state.type,
          maxPrice: this.state.maxPrice,
          minPrice: this.state.minPrice,
          minorder: this.state.minorder,
          category_id: this.state.category_id,
          subcat_id: this.state.subcat_id
        }

        await AsyncStorage.setItem('data', JSON.stringify(data));
        this.props.navigation.navigate('ProductList', {
          data: this.props.navigation.state.params.param.data,
          type: this.props.navigation.state.params.param.type,
          item: this.props.navigation.state.params.param.item,
          filter: 1
        })

      }



    } catch (error) {
      Toast.show('Please Try After Sometime.', Toast.LONG, Toast.TOP);
    }
  }
  removeFilter() {


    this.removeItemValue('data');

  }
  async removeItemValue(favi) {

    await AsyncStorage.removeItem(favi);
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    var suppliertype = (data && data.suppliertype) ? data.suppliertype : null;
    var Minorder = (data && data.Minorder) ? data.Minorder : null;
    var Minprice = (data && data.Minprice) ? data.Minprice : null;
    var maxprice = (data && data.maxprice) ? data.maxprice : null;
    var category_id = (data && data.category_id) ? data.category_id : null;
    var subcat_id = (data && data.subcat_id) ? data.subcat_id : null;

    this.setState({
      type: suppliertype,
      minorder: Minorder,
      minPrice: Minprice,
      maxPrice: maxprice,
      category_id: category_id,
      subcat_id: subcat_id
    });

    if (this.props.navigation.state.params.type == 'service') {
      this.setState({
        category: this.props.navigation.state.params.category
      });
    } else {
      this.getAllCategory();
    }
    this.props.navigation.navigate('ProductList', {
      data: this.props.navigation.state.params.param.data,
      type: this.props.navigation.state.params.param.type,
      item: this.props.navigation.state.params.param.item,
      filter: 1
    })
  }


  render() {

    return (
      (!this.state.connectState) ? (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
        </View>
      ) :
        <Container>
          <View style={style.styles.categoriesHeader}>
            <View style={style.styles.signinHeaderContainer}>
              <View style={style.styles.categoriesHeaderContainerLeft}>
                <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
              </View>
              <View style={style.styles.productfilterHeaderContainerMiddle}>
                <Text style={style.styles.productfilterHeaderContainerMiddleText}> Filters</Text>
              </View>
              <View style={style.styles.productfilterHeaderContainerRight}>
                <TouchableOpacity onPress={() => this.removeFilter()}
                  style={{ right: Platform.OS === 'ios' ? Dimensions.get("window").height < 667 ? '10%' : '5%' : '25%', backgroundColor: 'transparent', paddingLeft: 15 }}>
                  <Text style={{ fontSize: 15, color: 'grey', right: -20 }}> Clear Filters</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <Content>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <View style={{ flex: .2 }}>
                <ListItem selected={false} >
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: .3 }}><Text>Supplier Types:</Text></View>
                    <View style={{ flex: .1 }}>
                      <Radio
                        color={"#f0ad4e"}
                        selectedColor={"#5cb85c"}
                        selected={this.state.type == 1}
                        onPress={() => this.typeset(1)}
                      />
                    </View>
                    <View style={{ flex: .2 }}><Text>Agent</Text></View>
                    <View style={{ flex: .1 }}>
                      <Radio
                        color={"#f0ad4e"}
                        selectedColor={"#5cb85c"}
                        selected={this.state.type == 0}
                        onPress={() => this.typeset(0)}
                      />
                    </View>
                    <View style={{ flex: .2 }}><Text>Supplier</Text></View>
                  </View>
                </ListItem>
              </View>
              {
                (this.state.typep == 'product') ?
                  <View style={{ flex: .2 }}>
                    <ListItem selected={false} >
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: .3 }}><Text>Min. Order:</Text></View>

                        <View style={{ flex: .3 }}>

                          <TextInput
                            style={{ height: 35, borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => this.onChangeMinOrder(text)}
                            placeholder="0"
                            keyboardType={'numeric'}
                            value={this.state.minorder}
                          />
                        </View>
                      </View>

                    </ListItem>
                  </View>

                  : null
              }
              {
                (this.state.typep == 'product') ?
                  <View style={{ flex: .2 }}>
                    <ListItem selected={false} >
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: .3 }}><Text>Price:</Text></View>

                        <View style={{ flex: .3 }}>

                          <TextInput
                            style={{ height: 35, borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => this.onChangeMinPrice(text)}
                            placeholder="0.00"
                            keyboardType={'numeric'}
                            value={this.state.minPrice}
                          />
                        </View>
                        <View style={{ flex: .1 }}><Text style={{ top: 3, left: 3 }}> <Icon name="remove" /> </Text>
                        </View>
                        <View style={{ flex: .3 }}>

                          <TextInput
                            style={{ height: 35, borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => this.onChangeMaxPrice(text)}
                            placeholder="0.00"
                            keyboardType={'numeric'}
                            value={this.state.maxPrice}
                          />
                        </View>
                      </View>

                    </ListItem>
                  </View>
                  : null
              }
              <View style={{ flex: .2 }}>
                <ListItem selected={false} >
                  <View style={{ flex: 1 }}><Text style={{ top: 3, left: 3, fontSize: 20 }}>Category </Text>
                  </View>
                </ListItem>
              </View>
              <View style={{ flex: .2 }}>

                <FlatList
                  data={this.state.category}
                  keyExtractor={(item, index1) => item.category_id}
                  renderItem={({ item: item, index }) =>
                    <TouchableOpacity
                      onPress={() => this.toggle(item, index)}
                    >
                      <View style={styles.container} >
                        <View style={styles.titleContainer}>
                          {
                            (this.state.typep != 'service') ?
                              <Text style={(this.state.category_id != item.category_id) ? styles.Header : styles.HeaderSelected}>{`${item.category_name}`}</Text>
                              :
                              <Text style={(this.state.category_id != item.category_id) ? styles.Header : styles.HeaderSelected}>{`${item.categoryname}`}</Text>
                          }

                          <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.toggle(item, index)}
                            underlayColor="#f1f1f1">
                            <Icon1
                              style={styles.FAIcon}
                              name={item.icon}
                            />
                          </TouchableOpacity>
                        </View>

                        {
                          item.icon == 'minus' && (

                            <View style={styles.childBody}>

                              {
                                _.values(item.subcategory).map((item3, i) => (


                                  <ListItem onPress={() => this.setsubcat(item3, i)}>

                                    <Text style={(this.state.subcat_id != item3.subcat_id) ? styles.subcattext : styles.subcatActivetext}> {`${item3.subcategory_name}`}</Text>
                                  </ListItem>

                                ))
                              }
                            </View>


                          )
                        }

                      </View>


                    </TouchableOpacity>


                  }
                />
              </View>
            </View>
          </Content>
          <View style={{ flex: 0.10 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#fff', flex: 1, flexDirection: 'row-reverse' }}>
                <View style={{ justifyContent: 'center', flex: .5 }}>
                  <Button vertical onPress={() => this.setSearchFilter()} style={{ backgroundColor: '#f48024', width: 120, marginTop: 10, marginBottom: 5, textAlign: 'center' }}>
                    <Text style={{ color: '#fff', fontSize: 18 }}>Apply</Text>
                  </Button>

                </View>

              </FooterTab>
            </Footer>
          </View>


        </Container>
    );
  }
}


var styles = StyleSheet.create({
  container: {
    backgroundColor: '#D3D3D3',
    margin: 10,
    overflow: 'hidden'
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Header: {
    flex: 1,
    padding: 10,
    color: '#000'
  },
  HeaderSelected: {
    flex: 1,
    padding: 10,
    color: '#ff8308'
  },
  FAIcon: {
    fontSize: 15,
    color: "#000",
    textAlign: 'center'

  },
  button: {
    textAlign: 'center',
    justifyContent: 'center',
    width: 30
  },
  childBody: {
    backgroundColor: '#fff',
    padding: 10,
    paddingTop: 0,

  },
  subcattext: {
    color: '#000',
    paddingTop: 4
  },
  subcatActivetext: {
    color: '#ff8308',
    paddingTop: 4
  }
});
