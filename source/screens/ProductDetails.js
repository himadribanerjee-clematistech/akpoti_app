import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, Dimensions, Image, ActivityIndicator } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import { Footer, FooterTab, Button, Container, Content, Tab, Tabs } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
var images = [];
const MIN_HEIGHT = 100;
const MAX_HEIGHT = 250;
const SLIDER_1_FIRST_ITEM = 1;
const serverURL = 'http://agro.clematistech.com/';

export default class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedtab: 0,
      sellerItemImages: [],
      loading: true,
      serverData: [],
      sellerCustomerName: 'init',
      sellerCustomerId: 'init',
      uomId: '',
      uomName: '',
      sellerItemId: '',
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',

    }
    /*
    var parameter=this.props.navigation.state.params;
    //console.log(parameter.item);

    this.state.productDetailsId = parameter.item.id;
    //alert(this.state.productDetailsId);
    */
    this.state.productDetailsId = this.props.navigation.getParam('productDetailsId', null);
    //alert(this.state.productDetailsId);
    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {
    this.getProductDetailsData();

  }

  getProductDetailsData = () => {
    const url = serverURL + "api/productDetails";


    if (!this.state.productDetailsId) {
      this.state.productDetailsId = '';
    }
    var data = {
      id: this.state.productDetailsId,
    };
    //console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res)  
        var data = res.result;

        if (res.status === 200) {
          // console.log(data.seller_item);
          this.setState({
            serverData: data.seller_item[0],
            sellerCustomerName: data.seller_item[0].customer_name,
            sellerCustomerId: data.seller_item[0].customer_id,

            uomId: data.seller_item[0].uom_id,
            uomName: data.seller_item[0].uom_name,
            sellerItemId: data.seller_item[0].id
          });
          var selleritemImg = [];
          if (this.state.serverData.seller_item_images.length > 0) {
            this.state.serverData.seller_item_images.map((y) => {
              //alert(y.image);
              selleritemImg.push(serverURL + 'admin/uploads/selleritem/' + y.image);

            })
          } else {
            if (this.state.serverData.item_image) {
              selleritemImg.push(serverURL + 'admin/uploads/item/' + this.state.serverData.item_image);
            } else {
              selleritemImg.push(serverURL + 'admin/uploads/category/' + this.state.serverData.category_image);
            }
          }

          this.setState({
            sellerItemImages: selleritemImg
          });
          //console.log('HHH');
          //console.log(this.state.sellerItemImages);   
          this.setState({
            loading: false
          });
        } else {
          this.setState({
            serverError: true,
            errorMsg: res.msg

          });
        }

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })

  }


  _renderProductImage({ item, index }) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', top: 0, margin: 10, borderRadius: 10, backgroundColor: '#eff0f1', shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center' }} >
        <Image style={{ width: '100%', height: '100%', resizeMode: 'contain', opacity: 1 }} source={{ uri: item }} />
        <View>

        </View>
      </View>
    );
  }

  render() {
    const { slider1ActiveSlide } = this.state;
    //console.log('-----------------------------------------------------------------------------------------------------------------'); 
    //console.log(this.state.sellerItemImages);  
    return (
      <Container>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={style.styles.productListHeaderContainerMiddle}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 5 }} >{this.state.serverData.item_name}</Text></View>
                <View style={style.styles.productListHeaderContainerRight}></View>
                <View style={style.styles.productListHeaderContainerRighttwo}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchItem')} >
                    <Icon name="search" />
                  </TouchableOpacity>

                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.80 }} >
            <Content>
              {
                (this.state.serverError) ? (
                  <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                ) :
                  (!this.state.connectState) ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                    </View>
                  ) :
                    this.state.loading ? (
                      <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                    ) :

                      <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 0.8 }}>
                          <Carousel
                            data={this.state.sellerItemImages}
                            loop={true}
                            hasParallaxImages={true}
                            renderItem={this._renderProductImage}
                            sliderWidth={BannerWidth - 5}
                            itemWidth={BannerWidth - 5}
                            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                          />
                          <Pagination
                            dotsLength={this.state.sellerItemImages.length}
                            dotColor={'#000'}
                            dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                            inactiveDotColor={'#fff'}
                            containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                            activeDotIndex={slider1ActiveSlide}
                            inactiveDotOpacity={0.9}
                            inactiveDotScale={0.9}
                            carouselRef={this._slider1Ref}
                            tappableDots={!!this._slider1Ref}
                          />
                        </View>
                        <View style={{ flex: 0.8, top: 10, paddingBottom: 10 }}>
                          <View style={{ flex: 0.8, flexDirection: 'column', paddingLeft: 10, paddingRight: 10 }}>
                            <View style={{ flex: 0.5, flexDirection: 'row' }}>
                              <View style={{ flex: 0.2 }}><Text style={{ fontSize: 17, color: '#666', fontWeight: 'bold' }} >Price:</Text></View>
                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 19, color: '#666', fontWeight: 'bold' }} >₦ {this.state.serverData.min_price} - ₦ {this.state.serverData.max_price} </Text></View>
                            </View>
                            <View style={{ flex: 0.5, flexDirection: 'row', paddingTop: 15 }}>
                              <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Minimum Order:</Text></View>
                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.min_quantity} {this.state.serverData.uom_name} </Text></View>
                            </View>
                            <View style={{ flex: 0.5, flexDirection: 'column', paddingTop: 15 }}>
                              <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Description:</Text></View>
                              <View style={{ flex: 0.7, paddingTop: 5 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.item_description}  </Text></View>
                            </View>

                            <View style={{ flex: 1, top: 10, paddingBottom: 10 }}>
                              <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 2, borderColor: 'orange' }} onChangeTab={(obj) => {
                                this.setState({
                                  selectedtab: obj.i
                                });
                              }}>
                                <Tab heading="Product Details" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                                  <Content>
                                    <View style={{ flex: 1, flexDirection: 'column', paddingTop: 10 }}>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}><Text style={{ fontSize: 17, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} > Quick Details  </Text></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, height: 1, backgroundColor: '#000' }}></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Category:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.category_name} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Sub Category:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.subcat_name} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Species:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.species_name} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Variety:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.item_name} </Text></View>
                                        </View>
                                      </View>
                                    </View>

                                    <View style={{ flex: 1, flexDirection: 'column', paddingTop: 10 }}>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}><Text style={{ fontSize: 17, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} > Supply Ability  </Text></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, height: 1, backgroundColor: '#000' }}></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Organic:</Text></View>
                                          {
                                            (this.state.serverData.organic_YN == 1) ?
                                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > Yes </Text></View>
                                              :
                                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > No </Text></View>
                                          }

                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Min Order:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.min_quantity} {this.state.serverData.uom_name} </Text></View>
                                        </View>
                                      </View>
                                    </View>

                                    <View style={{ flex: 1, flexDirection: 'column', paddingTop: 10 }}>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}><Text style={{ fontSize: 17, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} > Information  </Text></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, height: 1, backgroundColor: '#000' }}></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}>
                                        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 15 }}>
                                          <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} >Packing:</Text></View>
                                          <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.packing_information} </Text></View>
                                        </View>
                                      </View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}>
                                        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 15 }}>
                                          <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} >Shipping:</Text></View>
                                          <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.shipping_information} </Text></View>
                                        </View>
                                      </View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}>
                                        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 15 }}>
                                          <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} >Special:</Text></View>
                                          <View style={{ flex: 1 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.special_information} </Text></View>
                                        </View>
                                      </View>
                                    </View>


                                  </Content>
                                </Tab>

                                <Tab heading="Company Profile" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                                  <Content>
                                    <View style={{ flex: 1, flexDirection: 'column', paddingTop: 10 }}>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}><Text style={{ fontSize: 17, color: '#666', fontWeight: 'bold', paddingBottom: 5 }} > Quick Details  </Text></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, height: 1, backgroundColor: '#000' }}></View>
                                      <View style={{ flex: 1, flexDirection: 'column', left: 10, paddingTop: 10 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Business Name:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.business_name} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Contact Name:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.contact_name} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Agent:</Text></View>
                                          {
                                            (this.state.serverData.agent_YN == 1) ?
                                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > Yes </Text></View>
                                              :
                                              <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > No </Text></View>
                                          }

                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Address:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.address} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >State:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.state} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >City:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.city} </Text></View>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: 15 }}>
                                          <View style={{ flex: 0.4 }}><Text style={{ fontSize: 15, color: '#666', fontWeight: 'bold' }} >Contact Number:</Text></View>
                                          <View style={{ flex: 0.7 }}><Text style={{ fontSize: 15, color: '#666' }} > {this.state.serverData.contact_number} </Text></View>
                                        </View>
                                      </View>
                                    </View>
                                  </Content>
                                </Tab>
                              </Tabs>
                            </View>
                          </View>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 10, }}>
                          <Button rounded onPress={() => this.props.navigation.navigate('ContactSupplier', { sellerCustomerName: this.state.sellerCustomerName, sellerCustomerId: this.state.sellerCustomerId, uomId: this.state.uomId, uomName: this.state.uomName, sellerItemId: this.state.sellerItemId })}
                            style={{ bottom: 0, backgroundColor: '#ff7519' }} >
                            <Text style={{ color: '#fff', paddingRight: 20, paddingLeft: 20, textAlign: 'center' }}>Contact Supplier</Text>
                          </Button>
                        </View>


                      </View>

              }
            </Content>
          </View>



          <View style={{ flex: 0.10 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('home')}  >
                  <Icon name="ios-home" style={{ color: 'white' }} />
                  <Text style={{ color: 'white' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Myakpoti')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }

}