import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, StyleSheet, TextInput, ActivityIndicator, Alert, ScrollView, FlatList, Image, Modal, Dimensions, TouchableHighlight } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import { Footer, FooterTab, Container, Content, Button, Textarea, Thumbnail } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import { NavigationEvents } from 'react-navigation';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);

const Height = Dimensions.get('window').height;
const Width = Dimensions.get('window').width;

const createFormData = (photo, body) => {
  const data = new FormData();

  photo.forEach((item, i) => {
    data.append("image[]", {
      uri: Platform.OS === "android" ? item.uri : item.uri.replace("file://", ""),
      type: item.type,
      name: item.filename || `filename${i}.jpg`,
      //data: item.data
    });
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};

export default class MyMessageDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      loading: false,
      from_id: null,
      to_id: '',
      message: '',
      serverData: [],
      reportincidentImages: [],
      reltype: '',
      relpic: '',
      relname: '',
      relid: '',
      relitemid: '',
      quatation_details: [],
      modalVisible: false,
      reportIncidentModalVisible: false,
      report_incident_msg: '',
      isSubmited: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',
      showLoginButton: false,
      serverError: false,

    }
    this.CheckConnectivity();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
      quality: 0.3
    }
    ImagePicker.launchImageLibrary(options, response => {
      var fileSizeInGb = parseInt(Math.floor(Math.log(response.fileSize) / Math.log(1024)));
      //alert(fileSizeInGb);
      if (fileSizeInGb > 2) {
        Alert.alert(
          'Oops !',
          'Image size can\'t be greater than 2 MB!', [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
          { cancelable: false })
      } else {
        if (response.uri) {
          var selectedImg = [];
          this.state.reportincidentImages.map((y) => {
            selectedImg.push(y);
          })
          //alert(response.fileSize);
          selectedImg.push(response);
          this.setState({ reportincidentImages: selectedImg })

          //console.log(this.state.reportincidentImages)
        }
      }

    })
  }

  componentDidMount() {
    if (global_isLoggedIn) {
      this.setState({
        reltype: this.props.navigation.getParam('reltype', ''),
        //from_id: localStorage.getItem("customer_id"),
        relpic: this.props.navigation.getParam('relpic', ''),
        relname: this.props.navigation.getParam('relname', ''),
        relid: this.props.navigation.getParam('relid', ''),
        relitemid: this.props.navigation.getParam('relitemid', ''),
      }, () => {
        this.getMessageDetails();
      });
    } else {
      this.setState({
        errorMsg: 'Please login to view this section.',
        showLoginButton: true

      });
    }
    //alert(this.props.navigation.getParam('reltype', 'Not specified'));




  }


  getMessageDetails = async () => {
    let url = serverURL + "api/messagedetails";
    let savedToken = await AsyncStorage.getItem('@token');

    var data = {
      type: this.state.reltype,
      rfq_id: this.state.relid,
      itemid: this.state.relitemid
    };
    //console.log(url,data,savedToken)
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': savedToken,
        'credentials': 'same-origin'
      },
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //console.log(res)  
        var data = res.result;

        if (res.status === 200) {
          //console.log(res.response_data);    
          var sellerName = '';
          var sellerProfileImage = '';
          if (res.response_data.length) {
            if (res.response_data[0].from_customer_id == res.to_id) {
              sellerName = res.response_data[0].fromcust_name;
              if (res.response_data[0].fromcust_profile_image) {
                if (res.response_data[0].fromcust_profile_image.length > 2) {
                  sellerProfileImage = serverURL + 'admin/uploads/bussiness_logo/' + res.response_data[0].fromcust_profile_image;
                }
              } else {
                sellerProfileImage = serverURL + 'assets/images/user.png';
              }

            } else if (res.response_data[0].to_customer_id == res.to_id) {
              sellerName = res.response_data[0].tocust_name;
              if (res.response_data[0].tocust_profile_image) {
                if (res.response_data[0].tocust_profile_image.length > 2) {
                  sellerProfileImage = serverURL + 'admin/uploads/bussiness_logo/' + res.response_data[0].tocust_profile_image;
                }
              } else {
                sellerProfileImage = serverURL + 'assets/images/user.png';
              }

            }
          }


          //console.log(sellerName+'='+sellerProfileImage);

          if (sellerName != '' && sellerProfileImage != '') {
            this.setState({
              relname: sellerName,
              relpic: sellerProfileImage
            });
          }


          this.setState({
            quatation_details: res.quatation_details,
            serverData: res.response_data,
            to_id: res.to_id,
            loading: false,
            noInternet: false,

          });
        } else {
          this.setState({
            serverError: true,
            errorMsg: res.msg,
            modalVisible: false,
            reportIncidentModalVisible: false,

          });
        }

      })
      .catch(error => {
        this.setState({
          serverError: true,
        });
        console.log(error)

      })
  }

  renderDate = (fromcust_name, date, timeStyle) => {
    return (
      <Text style={[styles.time, timeStyle]} numberOfLines={1}>
        {fromcust_name}  {date}
      </Text>
    );
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setReportIncidentModalVisible(visible) {
    this.setState({ reportIncidentModalVisible: visible });
  }

  reportincidentForm = async () => {
    var submit = true;
    let savedToken = await AsyncStorage.getItem('@token');
    if (this.state.report_incident_msg == '') {
      Alert.alert(
        'Oops !',
        'Message is empty!', [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
        { cancelable: false })
      submit = false;
      return false;
    }
    if (submit) {
      this.setState({ isSubmited: true });
      let url = serverURL + "api/messagecenter/reportIncident";

      fetch(url, {
        method: "POST",
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: savedToken,
          'credentials': 'same-origin'
        },
        timeout: 0,
        compress: true,
        body: createFormData(this.state.reportincidentImages, {
          from_customer_id: global_LoggedInCustomerId,
          to_customer_id: this.state.to_id,
          ref_id: this.state.relid,
          type: this.state.reltype,
          message: this.state.report_incident_msg
        })
      }).then(res => res.json())
        .then(res => {
          //console.log(res);
          if (res.status == 200) {
            Alert.alert(
              'Success !',
              res.msg, [
              { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
              { cancelable: false })
          } else {
            Alert.alert(
              'Success !',
              res.msg, [
              { text: 'OK', onPress: () => this.nextAction(), style: 'cancel' },],
              { cancelable: false })
            this.setState({
              isSubmited: false,
            })
          }

        })
        .catch(err => {
          console.error("error uploading images: ", err);
        });

    }

  }

  nextAction() {
    this.setState({
      reportincidentImages: [],
      reportIncidentModalVisible: false,
      report_incident_msg: '',
      isSubmited: false
    })
  }

  sendMessage = async () => {
    //item.from_customer_id === item.user_id
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds

    if (this.state.message != '') {
      var nc = {
        "from_customer_id": 1,
        'user_id': 1,
        "date": date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
        "message": this.state.message,
        fromcust_name: global_LoggedInCustomerName
      };
      let newServerData = this.state.serverData.concat(nc);


      let url = serverURL + "api/messageSend";
      let savedToken = await AsyncStorage.getItem('@token');

      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          var data = {
            type: this.state.reltype,
            rfq_id: this.state.relid,
            message: this.state.message,
            to_customer_id: this.state.to_id
          };
          //console.log(url,data,savedToken)
          fetch(url, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': savedToken,
              'credentials': 'same-origin'
            },
            body: JSON.stringify(data),
          })
            .then(res => res.json())
            .then(res => {
              //console.log(res)  

              if (res.status === 200) {

                this.setState({
                  serverData: newServerData,
                  message: ''

                });
              }

            })
            .catch(error => {
              this.setState({
                noInternet: true,
              });
              console.log(error)

            })
        } else {
          Alert.alert(
            'Oops !',
            'Seems like you don\'t have an active internet connection!', [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },],
            { cancelable: false })
        }
      });

    }



  }

  delImg(ind) {
    //console.log(ind);
    var selectedImg = [];
    this.state.reportincidentImages.map((y, index) => {
      if (index == ind) {
        // Skip img
      } else {
        selectedImg.push(y);
      }

    })
    this.setState({ reportincidentImages: selectedImg })
  }

  processQuotationImages(quotationImages) {

    if (quotationImages != undefined) {
      //console.log('AA-'+quotationImages);
      quotationImages.map((y, index) => {
        //console.log(y.image);  
        //console.log(serverURL+'admin/uploads/rfq/'+y.image);                     
        return <View style={{ flex: 0.5, paddingLeft: 5 }}><Image source={{ uri: serverURL + 'admin/uploads/rfq/' + y.image }} style={{ width: 100, height: 100 }} /></View>
      })
    }
  }

  _listEmptyComponent = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 200 }}>
        <Text style={{ fontSize: 24, color: '#a94442' }}>No Chat Found!!</Text>
      </View>
    )
  }

  render() {
    <NavigationEvents onDidFocus={() => this.getMessageDetails()} />

    var displayLi = '';
    var quantity = '';
    var rfq_message = '';
    var other_requerment = '';

    if (this.state.quatation_details.category_name) {
      displayLi += '' + this.state.quatation_details.category_name + ' > ';
    }
    if (this.state.quatation_details.item_name) {
      displayLi += '' + this.state.quatation_details.item_name + '';
    }
    if (this.state.quatation_details.species_name) {
      displayLi += ' > ' + this.state.quatation_details.species_name + ' ';
    }

    if (this.state.quatation_details.service_category_name) {
      displayLi += '' + quotationDetails.service_category_name + ' > ';
    }
    if (this.state.quatation_details.name) {
      displayLi += ' > ' + this.state.quatation_details.name + '';
    }

    if (this.state.quatation_details.rfq_message) {
      if (this.state.quatation_details.uom_name) {
        quantity = this.state.quatation_details.quantity + ' ' + this.state.quatation_details.uom_name;
      } else {
        quantity = '';
      }
      rfq_message = this.state.quatation_details.rfq_message;
    }



    if (this.state.quatation_details.other_requerment) {
      other_requerment = quotationDetails.other_requerment;
    }

    return (
      <Container>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={{ flex: 0.7, flexDirection: 'row', top: (Height * 3 / 100) }}>
                  <View style={{ flex: 0.2 }}><Thumbnail style={{ width: 40, height: 40 }} rounded source={{ uri: this.state.relpic }} /></View>
                  <View style={{ flex: 0.6, paddingTop: 10, paddingLeft: 5 }}><Text style={{ fontSize: 16, fontWeight: 'bold', color: '#5f5b5b' }}>{this.state.relname}</Text></View>
                </View>
                <View style={{ flex: 0.1, flexDirection: 'row', top: (Height * 4.5 / 100) }}>
                  <TouchableHighlight onPress={() => { this.setModalVisible(true); }}><Icon name="ios-information-circle" style={{ fontSize: 26, color: '#e68c04', }} /></TouchableHighlight>
                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.90 }} >
            <Content>
              {
                (this.state.serverError) ? (
                  <View style={{ flex: 1, flexDirection: 'column' }}><Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text></View>
                ) :
                  (!this.state.connectState) ? (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                    </View>
                  ) :
                    (this.state.showLoginButton) ? (
                      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', marginBottom: 5, marginTop: 40 }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 35, paddingBottom: 10 }}>{this.state.errorMsg}</Text>
                        <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                          <Button style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: '#FFA500', borderRadius: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }} onPress={() => this.props.navigation.navigate('Signin')}>
                            <Text style={{ color: 'white', fontSize: 15, textAlign: 'center' }}>Login</Text>
                          </Button>
                        </View>
                      </View>

                    ) :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :


                        <View style={{ paddingLeft: 2, paddingRight: 2, marginTop: 10 }}>
                          <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => { this.setState({ modalVisible: false }); }}
                          >
                            <View style={{ zIndex: 1, backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute', width: Width, height: Height + 30, }}>
                              <View style={{ backgroundColor: 'rgb(255, 255, 255)', margin: 10, borderRadius: 4, padding: 10, top: (Height * 7 / 100) }}>

                                <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                  <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.setState({ modalVisible: false }); }} />
                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, }}>
                                  <Text style={{ color: '#000', fontSize: 20, lineHeight: 30, }}>Information</Text>
                                </View>

                                <View style={{ padding: 10, }}>
                                  <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.8, left: (Width * 2 / 100) }}>
                                      <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#5f5b5b' }}>
                                        {displayLi}
                                      </Text>
                                    </View>
                                  </View>
                                </View>

                                <View style={{ flexDirection: 'row', padding: 10 }}>
                                  <View style={{ flex: 0.30, left: (Width * 2 / 100) }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#5f5b5b' }}>
                                      Quantity :
</Text>
                                  </View>
                                  <View style={{ flex: 0.70 }}>
                                    <Text style={{ fontSize: 15, left: (Width * 2 / 100), color: '#5f5b5b' }}>
                                      {quantity}
                                    </Text>
                                  </View>
                                </View>

                                <View style={{ flexDirection: 'row', padding: 10 }}>
                                  <View style={{ flex: 0.30, left: (Width * 2 / 100) }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#5f5b5b' }}>
                                      Message :
</Text>
                                  </View>
                                  <View style={{ flex: 0.70 }}>
                                    <Text style={{ fontSize: 15, left: (Width * 2 / 100), color: '#5f5b5b' }}>
                                      {this.state.quatation_details.rfq_message}
                                    </Text>
                                  </View>
                                </View>

                                <View style={{ flexDirection: 'row', padding: 10 }}>
                                  <View style={{ flex: 0.40, left: (Width * 2 / 100) }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#5f5b5b' }}>
                                      Other requirement :
</Text>
                                  </View>
                                  <View style={{ flex: 0.70 }}>
                                    <Text style={{ fontSize: 15, left: (Width * 2 / 100), color: '#5f5b5b' }}>
                                      {this.state.quatation_details.other_requerment}
                                    </Text>
                                  </View>
                                </View>

                                <View style={{ flexDirection: 'row', padding: 10, height: 130 }}>
                                  <View style={{ flex: 0.30, left: (Width * 2 / 100) }}>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#5f5b5b' }}>
                                      Image :
</Text>
                                  </View>
                                  <View style={{ flex: 0.70, flexDirection: 'row' }}>
                                    {
                                      (this.state.quatation_details.images != undefined) ?
                                        _.values(this.state.quatation_details.images).map((y, index) => (

                                          <View style={{ flex: 0.5, paddingLeft: 5, marginBottom: 10 }}><Image source={{ uri: serverURL + 'admin/uploads/rfq/' + y.image }} style={{ width: 100, height: 100 }} /></View>

                                        ))
                                        : null
                                    }
                                  </View>
                                </View>


                              </View>
                            </View>
                          </Modal>

                          <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.reportIncidentModalVisible}
                            onRequestClose={() => { this.setState({ reportIncidentModalVisible: false }); }}
                          >
                            <View style={{ zIndex: 1, backgroundColor: 'rgba(0,0,0,0.6)', position: 'absolute', width: Width, height: Height + 30, }}>
                              <View style={{ backgroundColor: 'rgb(255, 255, 255)', margin: 10, borderRadius: 4, top: (Height * 7 / 100) }}>

                                <View style={{ position: 'absolute', zIndex: 1, top: -17, left: -4 }}>
                                  <Icon name="ios-close-circle" style={{ fontSize: 45, color: '#e68c04', }} onPress={() => { this.setState({ reportIncidentModalVisible: false }); }} />
                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', padding: 12, }}>
                                  <Text style={{ color: '#000', fontSize: 20, lineHeight: 30, }}>Report Incident</Text>
                                </View>

                                <View style={{ padding: 10, }}>
                                  <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                      <Textarea rowSpan={10} bordered placeholder="Report message" onChangeText={(report_incident_msg) => this.setState({ report_incident_msg })} ref={'report_incident_msgClear'} />
                                    </View>
                                  </View>
                                </View>

                                <View style={{ padding: 10, }}>
                                  <ScrollView horizontal >
                                    <View style={{ flexDirection: 'row' }}>
                                      {

                                        this.state.reportincidentImages.map((y, index) => {
                                          return <View style={{ flex: 0.5, paddingLeft: 5 }}>
                                            <Image
                                              source={{ uri: y.uri }}
                                              style={{ width: 100, height: 100 }}
                                            /><View style={{ position: 'absolute', zIndex: 1, top: 0, left: 5 }}>
                                              <Icon name="ios-close-circle" style={{ fontSize: 30, color: '#e68c04', }} onPress={() => { this.delImg(index); }} />
                                            </View></View>
                                        })
                                      }
                                    </View>
                                  </ScrollView>
                                  <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 10, justifyContent: 'center', alignItems: 'center' }}><Text style={{ color: '#0f62ba', paddingRight: 0, paddingLeft: 0, textAlign: 'center' }} onPress={this.handleChoosePhoto}>Upload Image</Text></View>

                                </View>

                                <View style={{ justifyContent: 'center', marginBottom: 20, alignItems: 'center' }}>
                                  <TouchableOpacity style={{ height: 40, borderRadius: 30, backgroundColor: '#ffa500', paddingVertical: 10, top: 15, marginBottom: 15, width: (Width * 40 / 100), }} onPress={() => this.reportincidentForm()}>
                                    <Text style={{ color: '#F5FCFF', textAlign: 'center', fontSize: 18, }}> Report </Text>
                                  </TouchableOpacity>
                                </View>


                              </View>
                            </View>
                          </Modal>

                          <View style={styles.container}>
                            <FlatList style={styles.list}
                              data={this.state.serverData}
                              ref="flatList"
                              onContentSizeChange={() => this.refs.flatList.scrollToEnd()}
                              ListEmptyComponent={this._listEmptyComponent}
                              keyExtractor={(item) => {
                                return item.id;
                              }}
                              renderItem={(message) => {
                                const item = message.item;
                                //console.log(item);
                                let inMessage = 'out';
                                let itemStyle = '';
                                let fontStyle = '';
                                let timeStyle = '';

                                if (item.from_customer_id === item.user_id) {
                                  inMessage = 'out';
                                  itemStyle = styles.itemOut;
                                  fontStyle = styles.fontOut;
                                  timeStyle = styles.timeOut;
                                } else {
                                  inMessage = 'in';
                                  itemStyle = styles.itemIn;
                                  fontStyle = styles.fontIn;
                                  timeStyle = styles.timeIn;
                                }

                                //let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
                                //let fontStyle = inMessage ? styles.fontIn : styles.fontOut;

                                //let fontStyle = item.type === 'in' ? styles.fontIn : styles.fontOut;
                                //alert(fontStyle);

                                // {inMessage && this.renderDate(item.fromcust_name,item.date)}

                                return (
                                  <View>
                                    <View style={[styles.item, itemStyle]}>
                                      <View style={[styles.balloon]}>
                                        <Text style={[fontStyle]}>{item.message}</Text>
                                      </View>
                                    </View>
                                    {inMessage && this.renderDate(item.fromcust_name, item.date, timeStyle)}
                                  </View>
                                )
                              }} />

                          </View>
                          {
                            (this.state.quatation_details.is_expiry_date == 1) ? <View><View style={{ paddingBottom: 5, paddingLeft: 10, paddingRight: 10, flex: 1, justifyContent: 'flex-end' }}><TouchableHighlight onPress={() => { this.setReportIncidentModalVisible(true); }}><Text style={{ textAlign: 'left', color: '#d9271a' }}>Report Incident</Text></TouchableHighlight></View>
                              <View style={styles.footer}>
                                <View style={styles.inputContainer}>
                                  <TextInput style={styles.inputs}
                                    placeholder="Write a message..."
                                    underlineColorAndroid='transparent'
                                    onChangeText={(message) => this.setState({ message })} value={this.state.message} multiline={true}
                                  />
                                </View>

                                <TouchableOpacity style={styles.btnSend} onPress={() => { this.sendMessage(); }}>
                                  <Icon name="ios-send" style={styles.iconSend} />
                                </TouchableOpacity>
                              </View></View> :
                              <View style={styles.footer}>
                                <View>
                                  <Text>You can't send messages because this RFQ has expired. </Text>
                                </View>

                              </View>

                          }


                        </View>



              }
            </Content>
          </View>



          <View style={{ flex: 0.08 }} >
            <Footer style={{ height: 50 }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                  <Icon name="ios-home" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'white' }} />
                  <Text style={{ color: 'white' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }

}


const styles = StyleSheet.create({
  loading: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertBox: {
    backgroundColor: '#1C97F7',
  },
  alertText: {
    fontSize: 12,
    color: '#ffffff',
  },
  conCard: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 20,
  },
  conCardItem: {
    marginLeft: 5,
    marginTop: 5,
  },
  conDetails: {
    fontSize: 15,
    color: 'black',
    marginLeft: 5,
  },
  postCard: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
  },
  list: {
    paddingHorizontal: 17,
  },
  footer: {
    flexDirection: 'row',
    height: 80,
    backgroundColor: '#d1d1d1',
    paddingHorizontal: 10,
    padding: 10,


  },
  btnSend: {
    backgroundColor: "#00BFFF",
    width: 40,
    height: 40,
    borderRadius: 360,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  iconSend: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    color: '#fff',
    marginLeft: 5
  },
  container: {
    flex: 1,
    height: 483
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginRight: 10,

  },
  inputs: {
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
    width: '90%',

  },
  balloon: {
    maxWidth: 250,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 15,
    paddingLeft: 15,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start',
    backgroundColor: "#eeeeee",
    justifyContent: 'flex-start'
  },
  itemOut: {
    alignSelf: 'flex-end',
    backgroundColor: "#00b5ec",
    justifyContent: 'flex-end'

  },
  fontIn: {
    color: '#808080'
  },
  fontOut: {
    color: '#fff'
  },
  time: {
    margin: 1,
    fontSize: 10,
    color: "#808080",
  },
  timeIn: {
    alignSelf: 'flex-start',
    paddingLeft: 15
  },
  timeOut: {
    alignSelf: 'flex-end',
    paddingRight: 15
  },
  item: {
    marginVertical: 16,
    flex: 1,
    flexDirection: 'column',
    borderRadius: 20,
    padding: 5,
    width: '80%',
    marginBottom: 0
  },
});