import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, Image, StatusBar, FlatList, AsyncStorage } from 'react-native';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import { Container, Content, ListItem, Item, Input, Tab, Tabs, Separator } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);
export default class SearchItemScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [],
      selectedtab: 0,
      productitem: [],
      supplieritem: [],
      serviceitem: [],
      connectState: true,
    }
    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setTranslucent(true);

    });
    //this.product._root.focus()

  }

  componentWillUnmount() {
    this._navListener.remove();
  }
  async poroductSearchKeyWord(text) {
    await AsyncStorage.removeItem('data');


    if (text != '') {


      var url = '';
      if (parseInt(this.state.selectedtab) == 2) {
        url = "http://agro.clematistech.com/api//services/servicesearchbykeyword";
      } else {
        url = "http://agro.clematistech.com/api/poroductSearchKeyWord";
      }

      var data = {
        keyword: text
      };
      fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
        }),
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(res => {
          var item = res.result;

          this.setState({
            productitem: [],
            supplieritem: [],
            serviceitem: [],
          });

          if (parseInt(this.state.selectedtab) == 0) {
            this.setState({
              productitem: item
            });
          } else if (parseInt(this.state.selectedtab) == 1) {
            this.setState({
              supplieritem: item
            });
          } else if (parseInt(this.state.selectedtab) == 2) {
            this.setState({
              serviceitem: item
            });
          }


        })
        .catch(error => {
          // alert(JSON.stringify(error))
        })
    } else {
      this.setState({
        item: []
      });
    }

  }

  render() {

    return (
      (!this.state.connectState) ? (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
        </View>
      ) :
        <Container>
          <View style={style.styles.categoriesHeader}>
            <View style={style.styles.signinHeaderContainer}>
              <View style={style.styles.categoriesHeaderContainerLeft}>
                <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
              </View>
              <View style={style.styles.categoriesHeaderContainerMiddle}>


                <Item stackedLabel style={style.styles.itemsHeaderContainerMiddleText}>
                  {
                    this.state.selectedtab == 0 ?
                      <Input placeholder="Enter Product Name" placeholderTextColor="grey" onChangeText={this.poroductSearchKeyWord.bind(this)} />
                      :
                      this.state.selectedtab == 1 ?
                        <Input placeholder="Enter Supplier Name" placeholderTextColor="grey" onChangeText={this.poroductSearchKeyWord.bind(this)} />
                        :
                        <Input placeholder="Enter Service Name" placeholderTextColor="grey" onChangeText={this.poroductSearchKeyWord.bind(this)} />
                  }

                </Item>
              </View>

            </View>
          </View>
          <Content>
            <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 2, borderColor: 'orange' }} onChangeTab={(obj) => {
              this.setState({
                selectedtab: obj.i
              }); this.setState({
                productitem: [],
                supplieritem: [],
                serviceitem: [],
              });
            }}>
              <Tab heading="Products" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                <Content>
                  <FlatList
                    data={this.state.productitem}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) =>
                      <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'product', 'data': item, 'item': 1 })}>
                          <Separator bordered>
                            <Text style={{ fontWeight: 'bold' }}> {`${item.name}`}</Text>
                          </Separator>
                        </TouchableOpacity>
                        {
                          _.values(item.species).map((item3, i) => (

                            <ListItem onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'product', 'data': item3, 'item': 0 })}>
                              <Text style={{ color: '#000', paddingTop: 4 }}> {`${item3.species_name}`}</Text>
                            </ListItem>

                          ))
                        }
                      </View>


                    }
                  />
                </Content>
              </Tab>
              <Tab heading="Suppliers" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                <Content>
                  <FlatList
                    data={this.state.supplieritem}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) =>
                      <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'suppliers', 'data': item, 'item': 1 })}>
                          <Separator bordered>
                            <Text style={{ fontWeight: 'bold' }}> {`${item.name}`}</Text>
                          </Separator>
                        </TouchableOpacity>
                        {
                          _.values(item.species).map((item3, i) => (
                            <ListItem onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'suppliers', 'data': item3, 'item': 0, })}>
                              <Text style={{ color: '#000', paddingTop: 4 }}> {`${item3.species_name}`}</Text>
                            </ListItem>

                          ))
                        }
                      </View>


                    }
                  />
                </Content>
              </Tab>
              <Tab heading="Service" tabStyle={{ backgroundColor: 'white', borderColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                <FlatList
                  data={this.state.serviceitem}
                  keyExtractor={item => item.service_category_id}
                  renderItem={({ item }) =>
                    <View>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'service', 'data': item, 'item': 1 })}>
                        <Separator bordered>
                          <Text style={{ fontWeight: 'bold' }}> {`${item.service_category_name}`}</Text>
                        </Separator>
                      </TouchableOpacity>
                      {
                        _.values(item.item).map((item3, i) => (

                          <ListItem onPress={() => this.props.navigation.navigate('ProductList', { 'type': 'service', 'data': item3, 'item': 0 })}>
                            <Text style={{ color: '#000', paddingTop: 4 }}> {`${item3.name}`}</Text>
                          </ListItem>

                        ))
                      }
                    </View>


                  }
                />
              </Tab>

            </Tabs>

          </Content>
        </Container>
    );
  }
}