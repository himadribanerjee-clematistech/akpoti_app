import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, StatusBar, FlatList, ActivityIndicator, AsyncStorage } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { HeaderBackButton } from 'react-navigation-stack';
import { Footer, FooterTab, Button, Container, Content, Tab, Tabs } from 'native-base';
import _ from 'lodash'
import * as style from '../config';
import { Icon } from 'native-base';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
var images = [];
const MIN_HEIGHT = 100;
const MAX_HEIGHT = 250;
const SLIDER_1_FIRST_ITEM = 1;
export default class ProductListcreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [],
      selectedtab: 0,
      item: [],
      itemsearch: '',
      itemslug: '',
      type: 'product',
      loading: true,
      //Loading state used while loading the data for the first time
      serverData: [],
      sellerData: [],
      //Data Source for the FlatList
      fetching_from_server: false,
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
      totalfilter: 0,
      nextService: 0,
      nextServiceProvider: 0,
      nextProduct: 0,
      nextsupplier: 0,
      category_id: null,
      subcat_id: null,
      serviceCategorylist: [],
      serverError: false,
      connectState: true,
      errorMsg: 'Something went wrong. Please try later.',

    }
    this.CheckConnectivity();
    this.parameterSet();

  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  parameterSet() {
    var parameter = this.props.navigation.state.params;

    if (parameter.type == 'product') {
      this.offset = 1;
      this.state.type = 'product';
      if (parameter.item == 1) {

        if (parameter.data.item_slug) {
          this.state.itemsearch = parameter.data.name;
          this.state.itemslug = parameter.data.item_slug;
        } else {
          console.log(parameter, "aaa")
          if (parameter.subcat_id) {
            this.state.itemsearch = parameter.subcat_name;
            this.state.subcat_id = parameter.subcat_id;
          } else {
            this.state.itemsearch = parameter.data.category_name;
            this.state.category_id = parameter.data.category_id;
          }

        }
      } else {
        this.state.itemsearch = parameter.data.species_name;
        this.state.itemslug = parameter.data.species_slug;
      }
    } else if (parameter.type == 'service') {
      this.offset = 1;
      this.offsetprovider = 1;
      this.state.type = 'service';
      if (parameter.item == 1) {

        if (parameter.data.category_slug) {
          this.state.itemsearch = parameter.data.service_category_name;
          this.state.itemslug = parameter.data.category_slug;
        } else {
          console.log(parameter.data)
          if (parameter.data.service_id) {
            this.state.itemsearch = parameter.data.service_name;
            this.state.category_id = parameter.data.service_id;
          } else {
            this.state.itemsearch = parameter.data.service_category_name;
            this.state.category_id = parameter.data.id;
          }

        }
      } else {
        this.state.itemsearch = parameter.data.name;
        this.state.itemslug = parameter.data.item_slug;
      }

    } else if (parameter.type == 'suppliers') {
      this.offset = 1;
      this.state.type = 'suppliers';
      if (parameter.item == 1) {

        if (parameter.data.item_slug) {
          this.state.itemsearch = parameter.data.name;
          this.state.itemslug = parameter.data.item_slug;
        } else {
          this.state.itemsearch = parameter.data.category_name;
          this.state.category_id = parameter.data.category_id;
        }
      } else {
        this.state.itemsearch = parameter.data.species_name;
        this.state.itemslug = parameter.data.species_slug;
      }
    }
  }
  componentDidMount() {
    this.getAllCategory();;

    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setTranslucent(true);

    });
    //this.product._root.focus()
    if (this.state.type == 'product') {
      this.getProductData();
    } else if (this.state.type == 'suppliers') {
      this.getSupplierData();
    } else if (this.state.type == 'service') {
      this.getServiceData();
      this.getServiceProviderData();
    }



  }

  componentWillUnmount() {
    this._navListener.remove();
  }
  async getServiceData() {
    const url = serverURL + "api/services/getServiceList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;

    var category_id = (data && data != null) ? data.category_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    var data = { pageno: 1 };
    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }
    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    //console.log(url,data);
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {

        var data = res.result;
        this.offset = this.offset + 1;
        this.setState({
          serverData: data.seller_item,
          nextService: data.next,
          loading: false,
          serviceCategorylist: data.category
        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  async loadMoreServiceProviderData() {

    const url = serverURL + "api/services/getProviderList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;

    var category_id = (data && data != null) ? data.category_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    var data = { pageno: this.offset };

    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }
    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {

        var data = res.result;
        var sellerData = this.state.sellerData.concat(data.seller);
        this.offsetprovider = this.offsetprovider + 1;
        this.setState({
          sellerData: serverData,
          nextService: data.next,
          loading: false,
          serviceCategorylist: data.category
        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  };
  async getServiceProviderData() {
    const url = serverURL + "api/services/getProviderList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;

    var category_id = (data && data != null) ? data.category_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    var data = { pageno: 1 };
    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }
    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    console.log(url, data);
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {

        var data = res.result;
        this.offsetprovider = this.offsetprovider + 1;
        this.setState({
          sellerData: data.seller,
          nextServiceProvider: data.next,
          loading: false,
          serviceCategorylist: data.category
        });
        console.log(this.state.sellerData);
      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  async loadMoreServiceData() {

    const url = serverURL + "api/services/getServiceList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;

    var category_id = (data && data != null) ? data.category_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    var data = { pageno: this.offset };

    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }
    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {

        var data = res.result;
        var serverData = this.state.serverData.concat(data.seller_item);
        this.offset = this.offset + 1;
        this.setState({
          serverData: serverData,
          nextService: data.next,
          loading: false,
          serviceCategorylist: data.category
        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  };
  async getSupplierData() {
    const url = serverURL + "api/supplierList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;
    var Minorder = (data && data != null) ? data.minorder : null;
    var Minprice = (data && data != null) ? data.minPrice : null;
    var maxprice = (data && data != null) ? data.maxPrice : null;
    var category_id = (data && data != null) ? data.category_id : null;
    var subcat_id = (data && data != null) ? data.subcat_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    if (subcat_id == null) {
      subcat_id = this.state.subcat_id;
      cat_search = 1;
    }
    var data = { pageno: 1 };
    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (subcat_id != null) {
      data.subcategory = subcat_id;

      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    } else if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }

    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }

    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var data = res.result;
        this.offset = this.offset + 1;
        this.setState({
          serverData: data.seller,
          loading: false,
          nextsupplier: data.next

        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  async loadMoreSupplierData() {
    const url = serverURL + "api/supplierList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);
    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;
    var Minorder = (data && data != null) ? data.minorder : null;
    var Minprice = (data && data != null) ? data.minPrice : null;
    var maxprice = (data && data != null) ? data.maxPrice : null;
    var category_id = (data && data != null) ? data.category_id : null;
    var subcat_id = (data && data != null) ? data.subcat_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    if (subcat_id == null) {
      subcat_id = this.state.subcat_id;
      cat_search = 1;
    }
    var data = { pageno: this.offset };
    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (subcat_id != null) {
      data.subcategory = subcat_id;

      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    } else if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }

    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }

    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var data = res.result;
        this.offset = this.offset + 1;
        this.setState({
          serverData: [...this.state.serverData, ...data.seller],
          loading: false,
          nextsupplier: data.next

        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  async  getProductData() {
    const url = serverURL + "api/productList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);

    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;
    var Minorder = (data && data != null) ? data.minorder : null;
    var Minprice = (data && data != null) ? data.minPrice : null;
    var maxprice = (data && data != null) ? data.maxPrice : null;
    var category_id = (data && data != null) ? data.category_id : null;
    var subcat_id = (data && data != null) ? data.subcat_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    var data = { pageno: 1 };
    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (subcat_id == null) {
      subcat_id = this.state.subcat_id;
      cat_search = 1;
    }
    if (subcat_id != null) {
      data.subcategory = subcat_id;

      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }
    } else if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }

    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }

    if (Minorder != null) {
      data.min_quantity = Minorder;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    if (Minprice != null) {
      data.min_price = Minprice;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    if (maxprice != null) {
      data.max_price = maxprice;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }



    console.log(url, data)
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = res.result;
        this.offset = this.offset + 1;
        this.setState({
          serverData: data.seller_item,
          loading: false,
          nextProduct: data.next

        });
      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  async  loadMoreProductData() {
    const url = serverURL + "api/productList";
    var data = await AsyncStorage.getItem('data');
    data = JSON.parse(data);

    this.setState({
      totalfilter: 0
    });
    var suppliertype = (data && data != null) ? data.suppliertype : null;
    var Minorder = (data && data != null) ? data.minorder : null;
    var Minprice = (data && data != null) ? data.minPrice : null;
    var maxprice = (data && data != null) ? data.maxPrice : null;
    var category_id = (data && data != null) ? data.category_id : null;
    var subcat_id = (data && data != null) ? data.subcat_id : null;
    var cat_search = 0;
    if (category_id == null) {
      category_id = this.state.category_id;
      cat_search = 1;
    }
    var data = { pageno: this.offset };
    if (!this.state.itemslug) {
      this.state.itemslug = '';
    }
    if (subcat_id != null) {
      data.subcategory = subcat_id;

      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    } else if (category_id != null) {
      data.category = category_id;
      if (cat_search == 0) {
        this.setState({
          totalfilter: parseInt(this.state.totalfilter) + 1
        });
      }

    } else if (this.state.itemslug != null) {
      data.keyword = this.state.itemslug;
    }

    if (suppliertype != null) {
      data.is_agent = suppliertype;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }

    if (Minorder != null) {
      data.min_quantity = Minorder;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    if (Minprice != null) {
      data.min_price = Minprice;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }
    if (maxprice != null) {
      data.max_price = maxprice;
      this.setState({
        totalfilter: parseInt(this.state.totalfilter) + 1
      });
    }



    // console.log(url,data)
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = res.result;
        this.offset = this.offset + 1;
        this.setState({
          serverData: [...this.state.serverData, ...data.seller_item],
          loading: false,
          nextProduct: data.next

        });
      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  };


  getAllCategory = () => {
    const url = serverURL + "api/main";
    console.log(url);
    var method = [];
    method.push('getCategoryItem');
    var data = {
      method: method
    };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        var categories = [];
        index = 0;
        var temp = res.result.item;
        while (parseInt(index) < parseInt(temp.length)) {
          var category = {
            'category_id': temp[index].category_id,
            'category_name': temp[index].category_name,
            'image': temp[index].image
          };
          categories.push(category)
          index++;
        }
        this.setState({
          category: categories
        });
      })
      .catch(error => {
        // alert(JSON.stringify(error))
      })
  }
  _renderProductImage({ item, index }) {

    return (
      <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#eff0f1', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center' }} >
        <Image style={{ width: '100%', height: '100%', resizeMode: 'contain', borderRadius: 10, opacity: 1, backgroundColor: '#eff0f1', }} source={{ uri: serverURL + 'admin/uploads/selleritem/' + item.image }} />
        <View>

        </View>
      </View>
    );
  }
  _renderServiceImage({ item, index }) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#eff0f1', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center' }} >
        <Image style={{ width: '100%', height: '100%', resizeMode: 'contain', borderRadius: 10, opacity: 1, backgroundColor: '#eff0f1', }} source={{ uri: serverURL + 'admin/uploads/selleritem/' + item.image }} />
        <View>

        </View>
      </View>
    );
  }

  _renderSellerProviderImage({ item, index }) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ServiceDetails', { productDetailsId: item.provider_service_item_id })} >
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }}  >
          <Text style={{ position: 'absolute', zIndex: 9, color: '#fff', fontSize: 20, fontWeight: 'bold', paddingTop: 4, justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%' }}>{`${item.service_item_name}`} ({`${item.service_category_name}`})</Text>
          <Image style={{ width: '100%', height: 200, borderRadius: 10, opacity: 0.5, backgroundColor: 'black', }} source={{ uri: serverURL + 'admin/uploads/selleritem/' + item.seller_item_images }} />
          <View>

          </View>
        </View>
      </TouchableOpacity>
    );
  }
  _renderSellerImage({ item, index }) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { productDetailsId: item.seller_item_id })} >
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }}  >
          <Text style={{ position: 'absolute', zIndex: 9, color: '#fff', fontSize: 20, fontWeight: 'bold', paddingTop: 4, justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%' }}>{`${item.item_name}`}({`${item.species_name}`})</Text>
          <Image style={{ width: '100%', height: 200, borderRadius: 10, opacity: 0.5, backgroundColor: 'black', }} source={{ uri: serverURL + 'admin/uploads/selleritem/' + item.seller_item_images }} />
          <View>

          </View>
        </View>
      </TouchableOpacity>
    );
  }


  filterApply() {
    this.parameterSet();
    this.props.navigation.state.params.filter = 0;
    if (this.state.type == 'product') {
      this.getProductData();

    } else if (this.state.type == 'suppliers') {
      this.getSupplierData();
    } else if (this.state.type == 'service') {
      this.getServiceData();
      this.getServiceProviderData();
    }

  }

  renderServiceFooter() {
    return (
      //Footer View with Load More button

      (this.state.nextService == 1) ?

        <View style={styles.footer}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={this.loadMoreServiceData.bind(this)}
            //On Click of button calling loadMoreData function to load more data
            style={styles.loadMoreBtn}>
            <Text style={styles.btnText}>Load More</Text>
            {this.state.fetching_from_server ? (
              <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
            ) : null}
          </TouchableOpacity>
        </View>
        : null
    );
  }
  renderServiceProviderFooter() {
    return (
      //Footer View with Load More button

      (this.state.nextService == 1) ?

        <View style={styles.footer}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={this.loadMoreServiceProviderData.bind(this)}
            //On Click of button calling loadMoreData function to load more data
            style={styles.loadMoreBtn}>
            <Text style={styles.btnText}>Load More</Text>
            {this.state.fetching_from_server ? (
              <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
            ) : null}
          </TouchableOpacity>
        </View>
        : null
    );
  }
  renderSupplierFooter() {
    return (
      //Footer View with Load More button

      (this.state.nextsupplier == 1) ?

        <View style={styles.footer}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={this.loadMoreSupplierData.bind(this)}
            //On Click of button calling loadMoreData function to load more data
            style={styles.loadMoreBtn}>
            <Text style={styles.btnText}>Load More</Text>
            {this.state.fetching_from_server ? (
              <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
            ) : null}
          </TouchableOpacity>
        </View>
        : null
    );
  }
  renderProductFooter() {
    return (
      //Footer View with Load More button

      (this.state.nextProduct == 1) ?

        <View style={styles.footer}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={this.loadMoreProductData.bind(this)}
            //On Click of button calling loadMoreData function to load more data
            style={styles.loadMoreBtn}>
            <Text style={styles.btnText}>Load More</Text>
            {this.state.fetching_from_server ? (
              <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
            ) : null}
          </TouchableOpacity>
        </View>
        : null
    );
  }
  render() {
    if (this.props.navigation.state.params.filter && this.props.navigation.state.params.filter == 1) {
      this.filterApply();
    } else {
      if (this.props.navigation.state.params.subcat && this.props.navigation.state.params.subcat == 1) {
        this.parameterSet();
        this.getProductData();
        this.props.navigation.state.params.subcat = 0;
      }
    }

    const { slider1ActiveSlide } = this.state;
    return (
      <Container>

        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 0.10 }} >
            <View style={style.styles.categoriesHeader}>
              <View style={style.styles.signinHeaderContainer}>
                <View style={style.styles.categoriesHeaderContainerLeft}>
                  <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                </View>
                <View style={style.styles.productListHeaderContainerMiddle}>


                  <Text style={style.styles.productListHeaderContainerMiddleText}> {`${this.state.itemsearch}`}</Text>
                </View>
                <View style={style.styles.productListHeaderContainerRight}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchItem')} >
                    <Icon name="search" style={{ fontSize: 28, top: 2, left: 10 }} />
                  </TouchableOpacity>

                </View>
                <View style={style.styles.productListHeaderContainerRighttwo}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductFilter', { param: this.props.navigation.state.params, type: this.state.type, serviceCategorylist: this.state.serviceCategorylist })} >

                    {
                      parseInt(this.state.totalfilter) > 0 ?
                        <Text style={{ position: 'absolute', zIndex: 1, backgroundColor: 'red', borderRadius: 100, width: 18, height: 18, textAlign: 'center', justifyContent: 'center', left: 0, top: -6, color: '#fff' }}> {`${this.state.totalfilter}`}</Text>
                        : null
                    }
                    <Icon type="Feather" name="filter" style={{ fontSize: 28, top: 2 }} />
                  </TouchableOpacity>

                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.80 }} >
            <Content>
              {
                (this.state.type == 'product' || this.state.type == 'suppliers') ?

                  (this.state.type == 'product') ?

                    (!this.state.connectState) ? (
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                      </View>
                    ) :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :
                        (!this.state.loading && this.state.serverData.length == 0) ?
                          <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#f2dede', marginTop: 40, height: 50, left: 10, marginRight: 20, bottom: 10 }}><Text style={{ textAlign: 'center', justifyContent: 'center', top: 10, fontSize: 20, color: '#a94442' }}>No Data Found</Text></View>
                          :
                          <FlatList
                            data={this.state.serverData}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) =>
                              <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#eff0f1', marginTop: 10, left: 10, marginRight: 20, bottom: 10 }}>
                                <View style={{ flex: .4 }}>
                                  <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { productDetailsId: item.id })} >
                                    <Carousel
                                      ref={(c) => { this._carousel = c; }}
                                      data={item.seller_item_images}
                                      loop={true}
                                      hasParallaxImages={true}
                                      renderItem={this._renderProductImage}
                                      sliderWidth={BannerWidth - 30}
                                      itemWidth={BannerWidth - 30}
                                      onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                    />
                                    <Pagination
                                      dotsLength={(item.seller_item_images) ? item.seller_item_images.length : 0}
                                      dotColor={'#000'}
                                      dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                      inactiveDotColor={'#fff'}
                                      containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                      activeDotIndex={slider1ActiveSlide}
                                      inactiveDotOpacity={0.9}
                                      inactiveDotScale={0.9}
                                      carouselRef={this._slider1Ref}
                                      tappableDots={!!this._slider1Ref}
                                    />
                                  </TouchableOpacity>

                                </View>
                                <View style={{ flex: .6, backgroundColor: '#fff', paddingBottom: 10, borderColor: '#eff0f1', borderRadius: 10, borderWidth: 1 }}>
                                  <View style={{ flex: 1, flexDirection: 'column', }}>
                                    <View style={{ flex: .2 }}>
                                      <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 16, color: '#666' }} >{`${item.species_name}`} in {`${item.item_name}`}</Text>
                                    </View>

                                    <View style={{ flex: .5, top: 10, paddingBottom: 10 }}>
                                      <View style={{ flex: 1, flexDirection: 'row', }}>
                                        <View style={{ flex: .6 }}>

                                          <View style={{ flex: 1, flexDirection: 'column', left: 10 }}>
                                            <View style={{ flex: .2 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >₦ {`${item.min_price}`} - ₦ {`${item.max_price}`} </Text>
                                            </View>
                                            <View style={{ flex: .2 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >{`${item.min_quantity}`} {`${item.uom_name}`} Min. Order </Text>
                                            </View>
                                            <View style={{ flex: .2 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >Category: {`${item.category_name}`} </Text>
                                            </View>
                                            <View style={{ flex: .1 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >Sub Category: {`${item.subcat_name}`} </Text>
                                            </View>
                                            <View style={{ flex: .1 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >Item: {`${item.item_name}`} </Text>
                                            </View>
                                            <View style={{ flex: .1 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >Species: {`${item.species_name}`} </Text>
                                            </View>
                                            <View style={{ flex: .1 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >Cultivation Type: {(item.organic_YN == 1) ? ' Organic ' : ' Non  Organic '} </Text>
                                            </View>
                                          </View>
                                        </View>

                                        <View style={{ flex: .4 }}>
                                          <View style={{ flex: 1, flexDirection: 'column', }}>
                                            <View style={{ flex: .1 }}>
                                              <Text style={{ fontSize: 15, color: '#666' }} >{`${item.customer_name}`}</Text>
                                            </View>
                                            <View style={{ flex: .9, top: -10 }}>
                                              <Text style={{ fontSize: 10, color: '#666' }} >{`${item.email}`}</Text>
                                            </View>
                                          </View>
                                        </View>

                                      </View>
                                    </View>

                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactSupplier', { sellerCustomerName: item.customer_name, sellerCustomerId: item.customer_id, uomId: item.uom_id, uomName: item.uom_name, sellerItemId: item.id })} >
                                      <View style={{ flex: .3, flexDirection: 'row-reverse', right: 10, top: 10 }}>
                                        <Text style={{ color: '#ff8308' }}> Contact Supplier</Text><Icon name="chatboxes" style={{ color: '#ff8308' }} />
                                      </View>
                                    </TouchableOpacity>

                                  </View>
                                </View>
                              </View>


                            }
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            ListFooterComponent={this.renderSupplierFooter.bind(this)}
                          />
                    :
                    (!this.state.connectState) ? (
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                      </View>
                    ) :
                      this.state.loading ? (
                        <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                      ) :
                        (this.state.serverData.length == 0) ?
                          <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#f2dede', marginTop: 40, height: 50, left: 10, marginRight: 20, bottom: 10 }}><Text style={{ textAlign: 'center', justifyContent: 'center', top: 10, fontSize: 20, color: '#a94442' }}>No Data Found</Text></View>
                          :
                          <FlatList
                            data={this.state.serverData}
                            keyExtractor={item => item.id}
                            renderItem={({ item }) =>
                              <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#eff0f1', marginTop: 20, left: 10, marginRight: 20, bottom: 10 }}>

                                <View style={{ flex: .4 }}>

                                  <Carousel
                                    ref={(c) => { this._carousel = c; }}
                                    data={item.seller_item}
                                    loop={true}
                                    hasParallaxImages={true}
                                    renderItem={this._renderSellerImage.bind(this)}
                                    sliderWidth={BannerWidth - 30}
                                    itemWidth={BannerWidth - 30}
                                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                  />
                                  <Pagination
                                    dotsLength={(item.seller_item) ? item.seller_item.length : 0}
                                    dotColor={'#000'}
                                    dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                    inactiveDotColor={'#fff'}
                                    containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                    activeDotIndex={slider1ActiveSlide}
                                    inactiveDotOpacity={0.9}
                                    inactiveDotScale={0.9}
                                    carouselRef={this._slider1Ref}
                                    tappableDots={!!this._slider1Ref}
                                  />

                                </View>
                                <View style={{ flex: .6, backgroundColor: '#fff', paddingBottom: 10, borderColor: '#eff0f1', borderRadius: 10, borderWidth: 1 }}>
                                  <View style={{ flex: 1, flexDirection: 'column', }}>
                                    <View style={{ flex: .2 }}>
                                      <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 18, color: '#666' }} >{`${item.customer_name}`} </Text>
                                    </View>

                                    {
                                      (item.Main_product != '') ?
                                        <View style={{ flex: .2, left: 10, top: 5 }}>

                                          <Text style={{ fontSize: 15, color: 'grey' }} >Main Products: {`${item.Main_product}`} </Text>

                                        </View>
                                        : null
                                    }
                                    {
                                      (item.city != '') ?
                                        <View style={{ flex: .2, left: 10, top: 5 }}>
                                          <Text style={{ fontSize: 15, color: 'grey' }} >City: {`${item.city}`} </Text>
                                        </View>
                                        : null
                                    }

                                    {
                                      (item.state != '') ?
                                        <View style={{ flex: .2, left: 10, top: 5 }}>
                                          <Text style={{ fontSize: 15, color: 'grey' }} >State: {`${item.state}`} </Text>
                                        </View>
                                        : null
                                    }


                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactSupplier', { sellerCustomerName: item.customer_name, sellerCustomerId: item.customer_id, uomId: item.uom_id, uomName: item.uom_name, sellerItemId: '' })} >
                                      <View style={{ flex: .2, flexDirection: 'row-reverse', right: 10, top: 10 }}>
                                        <Text style={{ color: '#ff8308' }}> Contact Supplier</Text><Icon name="chatboxes" style={{ color: '#ff8308' }} />
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </View>


                            }
                            ItemSeparatorComponent={() => <View style={styles.separator} />}
                            ListFooterComponent={this.renderSupplierFooter.bind(this)}
                          />

                  :
                  <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 2, borderColor: 'orange' }} onChangeTab={(obj) => {
                    this.setState({
                      selectedtab: obj.i
                    });
                  }}>
                    <Tab heading="Services" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                      <Content>
                        {
                          (!this.state.connectState) ? (
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                              <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                            </View>
                          ) :
                            this.state.loading ? (
                              <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                            ) :
                              (!this.state.loading && this.state.serverData.length == 0) ?
                                <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#f2dede', marginTop: 40, height: 50, left: 10, marginRight: 20, bottom: 10 }}><Text style={{ textAlign: 'center', justifyContent: 'center', top: 10, fontSize: 20, color: '#a94442' }}>No Data Found</Text></View>
                                :
                                <FlatList
                                  data={this.state.serverData}
                                  keyExtractor={item => item.provider_service_item_id}
                                  renderItem={({ item }) =>
                                    <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#eff0f1', marginTop: 10, left: 10, marginRight: 20, bottom: 10 }}>
                                      <View style={{ flex: .4 }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ServiceDetails', { productDetailsId: item.provider_service_item_id })} >
                                          <Carousel
                                            ref={(c) => { this._carousel = c; }}
                                            data={item.seller_item_images}
                                            loop={true}
                                            hasParallaxImages={true}
                                            renderItem={this._renderServiceImage.bind(this)}
                                            sliderWidth={BannerWidth - 30}
                                            itemWidth={BannerWidth - 30}
                                            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                          />
                                          <Pagination
                                            dotsLength={(item.seller_item_images) ? item.seller_item_images.length : 0}
                                            dotColor={'#000'}
                                            dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                            inactiveDotColor={'#fff'}
                                            containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                            activeDotIndex={slider1ActiveSlide}
                                            inactiveDotOpacity={0.9}
                                            inactiveDotScale={0.9}
                                            carouselRef={this._slider1Ref}
                                            tappableDots={!!this._slider1Ref}
                                          />
                                        </TouchableOpacity>

                                      </View>
                                      <View style={{ flex: .6, backgroundColor: '#fff', paddingBottom: 10, borderColor: '#eff0f1', borderRadius: 10, borderWidth: 1 }}>
                                        <View style={{ flex: 1, flexDirection: 'column', }}>
                                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ServiceDetails', { productDetailsId: item.provider_service_item_id })} >
                                            <View style={{ flex: .2 }}>
                                              <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 16, color: '#666' }} >{`${item.service_item_name}`} in {`${item.service_category_name}`}</Text>
                                            </View>

                                            <View style={{ flex: .5, top: 10, paddingBottom: 10 }}>
                                              <View style={{ flex: 1, flexDirection: 'row', }}>
                                                <View style={{ flex: .6 }}>

                                                  <View style={{ flex: 1, flexDirection: 'column', left: 10 }}>
                                                    <View style={{ flex: .2 }}>
                                                      <Text style={{ fontSize: 13, color: '#666' }} >₦ {`${item.min_rate}`} - ₦ {`${item.max_rate}`} /{`${item.uom_name}`}</Text>
                                                    </View>

                                                    <View style={{ flex: .2 }}>
                                                      <Text style={{ fontSize: 13, color: '#666' }} >Category: {`${item.service_category_name}`} </Text>
                                                    </View>

                                                    <View style={{ flex: .6 }}>
                                                      <Text style={{ fontSize: 13, color: '#666' }} >Item: {`${item.service_item_name}`} </Text>
                                                    </View>


                                                  </View>
                                                </View>

                                                <View style={{ flex: .4 }}>
                                                  <View style={{ flex: 1, flexDirection: 'column', }}>
                                                    <View style={{ flex: .1 }}>
                                                      <Text style={{ fontSize: 15, color: '#666' }} >{`${item.customer_name}`}</Text>
                                                    </View>
                                                    <View style={{ flex: .9, top: 0 }}>
                                                      <Text style={{ fontSize: 10, color: '#666' }} >{`${item.email}`}</Text>
                                                    </View>
                                                  </View>
                                                </View>

                                              </View>
                                            </View>
                                          </TouchableOpacity>
                                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactSupplier', { sellerCustomerName: item.customer_name, sellerCustomerId: item.customer_id, uomId: item.uom_id, uomName: item.uom_name, sellerItemId: item.service_item_id })} >
                                            <View style={{ flex: .3, flexDirection: 'row-reverse', right: 10, top: 10 }}>
                                              <Text style={{ color: '#ff8308' }}> Contact Supplier</Text><Icon name="chatboxes" style={{ color: '#ff8308' }} />
                                            </View>
                                          </TouchableOpacity>

                                        </View>
                                      </View>
                                    </View>


                                  }
                                  ItemSeparatorComponent={() => <View style={styles.separator} />}
                                  ListFooterComponent={this.renderServiceFooter.bind(this)}
                                />
                        }
                      </Content>
                    </Tab>
                    <Tab heading="Providers" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#000' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} >
                      <Content>
                        {
                          (!this.state.connectState) ? (
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                              <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
                            </View>
                          ) :
                            this.state.loading ? (
                              <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                            ) :

                              (!this.state.loading && this.state.sellerData.length == 0) ?
                                <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#f2dede', marginTop: 40, height: 50, left: 10, marginRight: 20, bottom: 10 }}><Text style={{ textAlign: 'center', justifyContent: 'center', top: 10, fontSize: 20, color: '#a94442' }}>No Data Found</Text></View>

                                : <FlatList
                                  data={this.state.sellerData}
                                  keyExtractor={item => item.seller_id}
                                  renderItem={({ item }) =>
                                    <View style={{ flex: 1, flexDirection: 'column', borderRadius: 10, backgroundColor: '#eff0f1', marginTop: 20, left: 10, marginRight: 20, bottom: 10 }}>

                                      <View style={{ flex: .4 }}>

                                        <Carousel
                                          ref={(c) => { this._carousel = c; }}
                                          data={item.service_item}
                                          loop={true}
                                          hasParallaxImages={true}
                                          renderItem={this._renderSellerProviderImage.bind(this)}
                                          sliderWidth={BannerWidth - 30}
                                          itemWidth={BannerWidth - 30}
                                          onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                        />
                                        <Pagination
                                          dotsLength={(item.service_item) ? item.service_item.length : 0}
                                          dotColor={'#000'}
                                          dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                          inactiveDotColor={'#fff'}
                                          containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                          activeDotIndex={slider1ActiveSlide}
                                          inactiveDotOpacity={0.9}
                                          inactiveDotScale={0.9}
                                          carouselRef={this._slider1Ref}
                                          tappableDots={!!this._slider1Ref}
                                        />

                                      </View>
                                      <View style={{ flex: .6, backgroundColor: '#fff', paddingBottom: 10, borderColor: '#eff0f1', borderRadius: 10, borderWidth: 1 }}>
                                        <View style={{ flex: 1, flexDirection: 'column', }}>
                                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProviderDetails', { sellerId: item.seller_id })} >
                                            <View style={{ flex: .2 }}>
                                              <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 18, color: '#666' }} >{`${item.customer_name}`} </Text>
                                            </View>

                                            {
                                              (item.Main_product != '') ?
                                                <View style={{ flex: .2, left: 10, top: 5 }}>

                                                  <Text style={{ fontSize: 15, color: 'grey' }} >Main Services: {`${item.Main_Services}`} </Text>

                                                </View>
                                                : null
                                            }
                                            {
                                              (item.city != '') ?
                                                <View style={{ flex: .2, left: 10, top: 5 }}>
                                                  <Text style={{ fontSize: 15, color: 'grey' }} >City: {`${item.city}`} </Text>
                                                </View>
                                                : null
                                            }

                                            {
                                              (item.state != '') ?
                                                <View style={{ flex: .2, left: 10, top: 5 }}>
                                                  <Text style={{ fontSize: 15, color: 'grey' }} >State: {`${item.state}`} </Text>
                                                </View>
                                                : null
                                            }
                                          </TouchableOpacity>

                                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactSupplier', { sellerCustomerName: item.customer_name, sellerCustomerId: item.customer_id, uomId: item.uom_id, uomName: item.uom_name, sellerItemId: '' })} >
                                            <View style={{ flex: .2, flexDirection: 'row-reverse', right: 10, top: 10 }}>
                                              <Text style={{ color: '#ff8308' }}> Contact Supplier</Text><Icon name="chatboxes" style={{ color: '#ff8308' }} />
                                            </View>
                                          </TouchableOpacity>
                                        </View>
                                      </View>
                                    </View>


                                  }
                                  ItemSeparatorComponent={() => <View style={styles.separator} />}
                                  ListFooterComponent={this.renderServiceProviderFooter.bind(this)}
                                />
                        }
                      </Content>
                    </Tab>


                  </Tabs>
              }


            </Content>
          </View>
          <View style={{ flex: 0.10 }} >
            <Footer style={{ height: '100%' }} >
              <FooterTab style={{ backgroundColor: '#000' }}>
                <Button vertical onPress={() => this.props.navigation.navigate('Home')}  >
                  <Icon name="ios-home" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Home</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Profile')}>
                  <Icon name="ios-person" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Myakpoti</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                  <Icon name="ios-list" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Categories</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                  <Icon name="ios-cog" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Services</Text>
                </Button>
                <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                  <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                  <Text style={{ color: 'grey' }}>Messenger</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  item: {
    padding: 10,
  },
  separator: {
    height: 0.5,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#FFA500',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});