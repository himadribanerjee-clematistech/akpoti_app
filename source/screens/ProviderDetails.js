import React from 'react';
import { YellowBox, View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { HeaderBackButton } from 'react-navigation-stack';
import { Footer, FooterTab, Button, Container, Content, ListItem, } from 'native-base';
import _ from 'lodash';
import * as style from '../config';
import { Icon } from 'native-base';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import NetInfo from "@react-native-community/netinfo";

YellowBox.ignoreWarnings(["Warning:"]);
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
var images = [];
const MIN_HEIGHT = 100;
const MAX_HEIGHT = 250;
const SLIDER_1_FIRST_ITEM = 1;

export default class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      supplierDetail: [],
      ProviderServiceCategory: [],
      catdata: [],
      category: [],
      category_id: null,
      service_category_id: null,
      SupplierItem: [],
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
      seller_services: [],
      connectState: true,



    }

    this.state.sellerId = this.props.navigation.getParam('sellerId', null);

    this.CheckConnectivity();
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === "android") {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          this.setState({
            connectState: true,
          });
        } else {
          this.setState({
            connectState: false,
          });
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      this.setState({
        connectState: false,
      });
    } else {
      this.setState({
        connectState: true,
      });
    }
  };

  componentDidMount() {
    this.getSupplierDetail();
    this.getProviderServiceCategory();
    this.getSellerCategory();
    this.getSupplierItemBySellerId();
    this.getProviderServiceByProviderId();
    //console.log(this.state.ProviderServiceCategory,this.state.catdata)
  }

  getSupplierDetail() {
    const url = serverURL + "api/product/getSupplierDetailById";
    this.state.loading = true;
    var data = { seller_id: this.state.sellerId };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = { 'seller_image': [] };
        if (res.result.seller && res.result.seller.length == 1) {
          data = res.result.seller[0]
        }
        if (data.seller_image.length == 0) {
          data.seller_image = [{ image: "no-image.jpg" }]
        }

        // console.log(data)
        this.setState({
          supplierDetail: data,
          loading: false

        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  getSupplierItemBySellerId() {
    const url = serverURL + "api/product/getSupplierItemBySellerId";
    this.state.loading = true;
    var data = { seller_id: this.state.sellerId };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = res.result.seller;
        this.setState({
          SupplierItem: data,
          loading: false

        });
        // console.log(data)  
      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  getProviderServiceByProviderId() {
    const url = serverURL + "api/services/getProviderServiceByProviderId";
    this.state.loading = true;
    var data = { seller_id: this.state.sellerId };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = res.result.seller_services;
        this.setState({
          seller_services: data,
          loading: false

        });
        // console.log(data)  
      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  getProviderServiceCategory() {
    const url = serverURL + "api/main";
    this.state.loading = true;
    var method = ['getProviderServiceCategory']
    var data = { seller_id: this.state.sellerId, method: method };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = res.result.servicecatdata;
        this.setState({
          ProviderServiceCategory: data,
          loading: false

        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  getSellerCategory() {
    const url = serverURL + "api/main";
    this.state.loading = true;
    var method = ['getSellerCategory']
    var data = { seller_id: this.state.sellerId, method: method };
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(data),
    })
      .then(res => res.json())
      .then(res => {
        //  console.log(res)  
        var data = res.result.catdata;
        index = 0;
        var categories = [];
        var temp = res.result.catdata;
        while (parseInt(index) < parseInt(temp.length)) {
          var category = {
            'category_id': temp[index].category_id,
            'category_name': temp[index].category_name,
            'image': temp[index].image,
            'icon': (temp[index].category_id == this.state.category_id) ? 'minus' : 'plus',
            'subcategory': temp[index].subcategory,
          };
          categories.push(category)
          index++;
        }

        this.setState({
          catdata: categories,
          loading: false

        });

      })
      .catch(error => {
        // console.log("aa",JSON.stringify(error))
      })
  }
  _renderSellerImage({ item, index }) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', top: 0, margin: 10, borderRadius: 10, backgroundColor: '#eff0f1', shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center' }} >
        <Image style={{ width: '100%', height: '100%', resizeMode: 'contain', opacity: 1 }} source={{ uri: serverURL + 'admin/uploads/seller/' + item.image }} />
        <View>

        </View>
      </View>
    );
  }
  _renderSellerService({ item, index }) {
    var provider_service_images = item.provider_service_images;
    var image = '/seller/no-image.jpg';
    if (item.service_category_image != '') {
      image = '/category/' + item.service_category_image;
    }
    if (provider_service_images.length > 0) {
      image = '/selleritem/' + provider_service_images[0].image;
    }
    return (

      <TouchableOpacity onPress={() => this.props.navigation.navigate('ServiceDetails', { productDetailsId: item.provider_service_item_id })} >
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }}  >
          <Text style={{ position: 'absolute', zIndex: 9, color: '#fff', fontSize: 20, fontWeight: 'bold', paddingTop: 4, justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%' }}>{`${item.service_item_name}`} ({`${item.service_category_name}`})</Text>
          <Image style={{ width: '100%', height: 200, borderRadius: 10, opacity: 0.5, backgroundColor: 'black', }} source={{ uri: serverURL + 'admin/uploads' + image }} />
          <View>

          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderSellerProduct({ item, index }) {
    var seller_item_images = item.seller_item_images;
    var image = '/seller/no-image.jpg';
    if (item.category_image != '') {
      image = '/category/' + item.category_image;
    }


    if (seller_item_images.length > 0) {
      image = '/selleritem/' + seller_item_images[0].image;
    }
    return (

      <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetails', { productDetailsId: item.seller_item_id })} >
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#fff', borderColor: '#000', top: 0, margin: 10, shadowColor: '#000', height: 200, justifyContent: 'flex-start', alignItems: 'center', shadowOffset: { width: 0, height: 20 }, elevation: 3, shadowRadius: 52, borderRadius: 10 }}  >
          <Text style={{ position: 'absolute', zIndex: 9, color: '#fff', fontSize: 20, fontWeight: 'bold', paddingTop: 4, justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%' }}>{`${item.category_name}`}->{`${item.item_name}`}->{`${item.species_name}`}</Text>
          <Image style={{ width: '100%', height: 200, borderRadius: 10, opacity: 0.5, backgroundColor: 'black', }} source={{ uri: serverURL + 'admin/uploads' + image }} />
          <View>

          </View>
        </View>
      </TouchableOpacity>
    );
  }

  toggle(item, index1) {


    index = 0;
    var categories = [];
    var temp = this.state.catdata;
    while (parseInt(index) < parseInt(temp.length)) {
      var category = {
        'category_id': temp[index].category_id,
        'category_name': temp[index].category_name,
        'image': temp[index].image,
        'subcategory': temp[index].subcategory,
      };
      if (item.category_id == temp[index].category_id) {
        category.icon = (temp[index].icon == 'minus') ? 'plus' : 'minus';
        if (temp[index].icon == 'minus') {
          item.category_id = null;
        }
      } else {
        category.icon = 'plus';
      }
      categories.push(category)
      index++;
    }

    this.setState({
      catdata: categories,
      category_id: item.category_id
    });


  }
  render() {

    const { slider1ActiveSlide } = this.state;
    return (

      <Container>
        {
          (!this.state.connectState) ? (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../assets/images/no_internet.png')} style={{ width: 300, height: 200, borderRadius: 10 }} />
            </View>
          ) :
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <View style={{ flex: 0.10 }} >
                <View style={style.styles.categoriesHeader}>
                  <View style={style.styles.signinHeaderContainer}>
                    <View style={style.styles.categoriesHeaderContainerLeft}>
                      <HeaderBackButton onPress={() => this.props.navigation.goBack(null)} />
                    </View>
                    <View style={style.styles.productListHeaderContainerMiddle}></View>
                    <View style={style.styles.productListHeaderContainerRight}></View>
                    <View style={style.styles.productListHeaderContainerRighttwo}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('SearchItem')} >
                        <Icon name="search" />
                      </TouchableOpacity>

                    </View>
                  </View>
                </View>
              </View>
              <View style={{ flex: 0.80 }} >
                <Content>
                  {
                    this.state.loading ? (
                      <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" style={{ marginTop: 20 }} /></View>
                    ) :

                      <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: .2, flexDirection: 'column', borderRadius: 10, marginTop: 10, left: 10, marginRight: 20, bottom: 5 }}>
                          <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: '#666', paddingTop: 10, paddingBottom: 10 }} >{this.state.supplierDetail.customer_name}</Text>
                        </View>
                        <View style={{ flex: 0.2, backgroundColor: '#eff0f1' }}>
                          <Carousel
                            data={this.state.supplierDetail.seller_image}
                            loop={true}
                            hasParallaxImages={true}
                            renderItem={this._renderSellerImage}
                            sliderWidth={BannerWidth - 5}
                            itemWidth={BannerWidth - 5}
                            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                          />
                          <Pagination
                            dotsLength={(this.state.supplierDetail.seller_image) ? this.state.supplierDetail.seller_image.length : 0}
                            dotColor={'#000'}
                            dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                            inactiveDotColor={'#fff'}
                            containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                            activeDotIndex={slider1ActiveSlide}
                            inactiveDotOpacity={0.9}
                            inactiveDotScale={0.9}
                            carouselRef={this._slider1Ref}
                            tappableDots={!!this._slider1Ref}
                          />
                        </View>
                        <View style={{ flex: 0.2, backgroundColor: '#fff', paddingBottom: 10, borderColor: '#eff0f1', borderRadius: 10, borderWidth: 1 }}>
                          <View style={{ flex: 1, flexDirection: 'column', }}>
                            {(this.state.supplierDetail.address && this.state.supplierDetail.address != '') ?
                              <View style={{ flex: .3, top: 5 }}>
                                <View style={{ flex: 1, flexDirection: 'row', }}>
                                  <View style={{ flex: .5 }}>
                                    <Text style={{ left: 10, fontSize: 16, color: 'grey' }} >Address: </Text>
                                  </View>
                                  <View style={{ flex: .5, flexDirection: 'row' }}>
                                    <Text style={{ left: 10, fontSize: 16, color: '#000' }} >{`${this.state.supplierDetail.address}`}</Text>
                                  </View>
                                </View>

                              </View> : null
                            }


                            {(this.state.supplierDetail.city && this.state.supplierDetail.city != '') ?
                              <View style={{ flex: .3, top: 5 }}>
                                <View style={{ flex: 1, flexDirection: 'row', }}>
                                  <View style={{ flex: .5 }}>
                                    <Text style={{ left: 10, fontSize: 16, color: 'grey' }} >City: </Text>
                                  </View>
                                  <View style={{ flex: .5, flexDirection: 'row' }}>
                                    <Text style={{ left: 10, fontSize: 16, color: '#000' }} >{`${this.state.supplierDetail.city}`}</Text>
                                  </View>
                                </View>

                              </View> : null
                            }
                            {(this.state.supplierDetail.state && this.state.supplierDetail.state != '') ?
                              <View style={{ flex: .3, top: 5 }}>
                                <View style={{ flex: 1, flexDirection: 'row', }}>
                                  <View style={{ flex: .5 }}>
                                    <Text style={{ left: 10, fontSize: 16, color: 'grey' }} >State: </Text>
                                  </View>
                                  <View style={{ flex: .5, flexDirection: 'row' }}>
                                    <Text style={{ left: 10, fontSize: 16, color: '#000' }} >{`${this.state.supplierDetail.state}`}</Text>
                                  </View>
                                </View>

                              </View> : null
                            }

                          </View>

                        </View>
                        <View style={{ flex: 0.2, backgroundColor: '#eff0f1', backgroundColor: '#fff', }}>
                          <View style={{ flex: 1, flexDirection: 'column' }}>
                            <View style={{ flex: .1, flexDirection: 'column', borderRadius: 10, marginTop: 10, left: 10, marginRight: 20, bottom: 5 }}>
                              <Text style={{ fontWeight: 'bold', textAlign: 'left', fontSize: 20, color: '#000', paddingTop: 10, paddingBottom: 10, left: 10 }} >Main Product</Text>
                            </View>
                            <View style={{ flex: .4, flexDirection: 'column' }}>
                              <FlatList
                                data={this.state.catdata}
                                keyExtractor={(item, index1) => item.category_id}
                                renderItem={({ item: item, index }) =>
                                  <TouchableOpacity
                                    onPress={() => this.toggle(item, index)}
                                  >
                                    <View style={styles.container} >
                                      <View style={styles.titleContainer}>

                                        <Text style={(this.state.category_id != item.category_id) ? styles.Header : styles.HeaderSelected}>{`${item.category_name}`}</Text>

                                        <TouchableOpacity
                                          style={styles.button}
                                          onPress={() => this.toggle(item, index)}
                                          underlayColor="#f1f1f1">
                                          <Icon1
                                            style={styles.FAIcon}
                                            name={item.icon}
                                          />
                                        </TouchableOpacity>
                                      </View>
                                      {
                                        item.icon == 'minus' && (

                                          <View style={styles.childBody}>

                                            {
                                              _.values(item.subcategory).map((item3, i) => (


                                                <ListItem >

                                                  <Text style={(this.state.subcat_id != item3.subcat_id) ? styles.subcattext : styles.subcatActivetext}> {`${item3.subcat_name}`}</Text>
                                                </ListItem>

                                              ))
                                            }
                                          </View>


                                        )
                                      }


                                    </View>


                                  </TouchableOpacity>


                                }
                              />

                            </View>
                            <View style={{ flex: .5, flexDirection: 'column' }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: .2, flexDirection: 'column', borderRadius: 10, marginTop: 10, left: 10, marginRight: 20, bottom: 5 }}>
                                  <Text style={{ fontWeight: 'bold', textAlign: 'left', fontSize: 20, color: '#000', paddingTop: 10, paddingBottom: 10, left: 10 }} >Main Services</Text>
                                </View>
                                <View style={{ flex: .8, flexDirection: 'column' }}>
                                  <FlatList
                                    data={this.state.ProviderServiceCategory}
                                    keyExtractor={(item, index1) => item.service_category_id}
                                    renderItem={({ item: item, index }) =>

                                      <View style={styles.container} >
                                        <View style={{ flexDirection: 'row', padding: 10 }}>

                                          <Text >{`${item.service_category_name}`}</Text>
                                        </View>
                                      </View>

                                    }
                                  />

                                </View>

                              </View>

                            </View>
                          </View>

                        </View>
                        <View style={{ flex: 0.2 }}>
                          <View style={{ flex: 1, flexDirection: 'column' }}>
                            <View style={{ flex: 0.5 }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: .2, flexDirection: 'column', borderRadius: 10, marginTop: 10, left: 10, marginRight: 20, bottom: 5 }}>
                                  <Text style={{ fontWeight: 'bold', textAlign: 'left', fontSize: 20, color: '#000', paddingTop: 10, paddingBottom: 10, left: 10 }} >Products</Text>
                                </View>
                                <View style={{ flex: .2, backgroundColor: '#eff0f1' }}>
                                  <Carousel
                                    data={this.state.SupplierItem}
                                    loop={true}
                                    hasParallaxImages={true}
                                    renderItem={this._renderSellerProduct.bind(this)}
                                    sliderWidth={BannerWidth - 5}
                                    itemWidth={BannerWidth - 5}
                                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                  />
                                  <Pagination
                                    dotsLength={(this.state.SupplierItem) ? this.state.SupplierItem.length : 0}
                                    dotColor={'#000'}
                                    dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                    inactiveDotColor={'#fff'}
                                    containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                    activeDotIndex={slider1ActiveSlide}
                                    inactiveDotOpacity={0.9}
                                    inactiveDotScale={0.9}
                                    carouselRef={this._slider1Ref}
                                    tappableDots={!!this._slider1Ref}
                                  />
                                </View>
                              </View>

                            </View>
                            <View style={{ flex: 0.5, backgroundColor: '#eff0f1' }}>
                              <View style={{ flex: 1, flexDirection: 'column' }}>
                                <View style={{ flex: .2, flexDirection: 'column', borderRadius: 10, marginTop: 10, left: 10, marginRight: 20, bottom: 5 }}>
                                  <Text style={{ fontWeight: 'bold', textAlign: 'left', fontSize: 20, color: '#000', paddingTop: 10, paddingBottom: 10, left: 10 }} >Service</Text>
                                </View>
                                <View style={{ flex: .2, backgroundColor: '#eff0f1' }}>
                                  <Carousel
                                    data={this.state.seller_services}
                                    loop={true}
                                    hasParallaxImages={true}
                                    renderItem={this._renderSellerService.bind(this)}
                                    sliderWidth={BannerWidth - 5}
                                    itemWidth={BannerWidth - 5}
                                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                  />
                                  <Pagination
                                    dotsLength={(this.state.seller_services) ? this.state.seller_services.length : 0}
                                    dotColor={'#000'}
                                    dotStyle={{ width: 8, height: 8, borderRadius: 10, marginHorizontal: 0 }}
                                    inactiveDotColor={'#fff'}
                                    containerStyle={{ bottom: 0, paddingBottom: 5, paddingTop: 2 }}
                                    activeDotIndex={slider1ActiveSlide}
                                    inactiveDotOpacity={0.9}
                                    inactiveDotScale={0.9}
                                    carouselRef={this._slider1Ref}
                                    tappableDots={!!this._slider1Ref}
                                  />
                                </View>
                              </View>

                            </View>
                          </View>
                        </View>
                      </View>

                  }
                </Content>
              </View>



              <View style={{ flex: 0.10 }} >
                <Footer style={{ height: '100%' }} >
                  <FooterTab style={{ backgroundColor: '#000' }}>
                    <Button vertical onPress={() => this.props.navigation.navigate('home')}  >
                      <Icon name="ios-home" style={{ color: 'white' }} />
                      <Text style={{ color: 'white' }}>Home</Text>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('Myakpoti')}>
                      <Icon name="ios-person" style={{ color: 'grey' }} />
                      <Text style={{ color: 'grey' }}>Myakpoti</Text>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('Categories')}>
                      <Icon name="ios-list" style={{ color: 'grey' }} />
                      <Text style={{ color: 'grey' }}>Categories</Text>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('Services')}>
                      <Icon name="ios-cog" style={{ color: 'grey' }} />
                      <Text style={{ color: 'grey' }}>Services</Text>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('Messenger')}>
                      <Icon name="ios-chatboxes" style={{ color: 'grey' }} />
                      <Text style={{ color: 'grey' }}>Messenger</Text>
                    </Button>
                  </FooterTab>
                </Footer>
              </View>
            </View>}

      </Container>
    );
  }

}


var styles = StyleSheet.create({
  container: {
    backgroundColor: '#D3D3D3',
    margin: 10,
    overflow: 'hidden'
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Header: {
    flex: 1,
    padding: 10,
    color: '#000'
  },
  HeaderSelected: {
    flex: 1,
    padding: 10,
    color: '#ff8308'
  },
  FAIcon: {
    fontSize: 15,
    color: "#000",
    textAlign: 'center'

  },
  button: {
    textAlign: 'center',
    justifyContent: 'center',
    width: 30
  },
  childBody: {
    backgroundColor: '#fff',
    padding: 10,
    paddingTop: 0,

  },
  subcattext: {
    color: '#000',
    paddingTop: 4
  },
  subcatActivetext: {
    color: '#ff8308',
    paddingTop: 4
  }
});