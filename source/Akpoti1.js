import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  Dimensions,
  Image,
  Button,
  TouchableOpacity,
  YellowBox,
} from 'react-native';

import {
  Splash,
  Home,
  Settings,
  Signin,
  Register,
  Categories,
  Services,
  Messenger,
  SearchItem,
  ProductList,
  ProductFilter,
  ContactSupplier,
  Newslist,
  NewsDetails,
  ProductDetails,
  Profile,
  ServiceDetails,
  ProviderDetails,
  EditBuyer,
  UpgradeSeller,
  UpgradeAgent,
  SellerNewDoc,
  Enquiry,
  MyItems,
  AddSellerItem,
  EditSellerItem,
  MySellerServices,
  AddSellerServiceItem,
  EditSellerServiceItem,
  SendRfq,
  MyNotifications,
  MyRFQs,
  MyMessageList,
  MyMessageDetails,
  EditSeller,
  Test,
} from './screens';
import { Icon } from 'native-base';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';
import {
  createDrawerNavigator,
  DrawerNavigatorItems,
} from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';

YellowBox.ignoreWarnings(["Warning:"]);

global.API_KEY = 'AIzaSyCCWnp0Wk-h48SAo3NepYcz_GLjEPSTS2M';
global.serverURL = 'http://agro.clematistech.com/';
global.global_profileImage = 'http://agro.clematistech.com/assets/images/user.png';


const BottomStack = createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-home" style={{ color: tintColor, fontSize: 25 }}></Icon>
      ),
      tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: 'grey',
        style: {
          backgroundColor: '#000',
        },
      },
    }),
  },
  MyAkpoti: {
    //screen: Signin,
    screen: Profile,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-person" style={{ color: tintColor, fontSize: 25 }}></Icon>
      ),
      tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: 'grey',
        style: {
          backgroundColor: '#000',
        },
      },
    }),
  },
  Categories: {
    screen: Categories,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-list" style={{ color: tintColor, fontSize: 25 }}></Icon>
      ),
      tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: 'grey',
        style: {
          backgroundColor: '#000',
        },
      },
    }),
  },
  Services: {
    screen: Services,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon
          name="ios-construct"
          style={{ color: tintColor, fontSize: 25 }}></Icon>
      ),
      tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: 'grey',
        style: {
          backgroundColor: '#000',
        },
      },
    }),
  },

  Messenger: {
    screen: MyMessageList,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-list" style={{ color: tintColor, fontSize: 25 }}></Icon>
      ),
      tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: 'grey',
        style: {
          backgroundColor: '#000',
        },
      },
    }),
  },
});
const CustomDrawerContentComponent = props => (
  <ScrollView>
    <SafeAreaView
      style={{ flex: 1, flexDirection: 'column' }}
      forceInset={{ top: 'always', horizontal: 'never' }}>
      <View
        style={{
          height: 150,
          justifyContent: 'center',
          backgroundColor: 'orange',
          flex: 1,
          flexDirection: 'column',
        }}>
        <View style={{ flex: 0.6 }}>
          <Image
            source={{ uri: global_profileImage }}
            style={{ left: 10, top: 30, borderRadius: 60, height: 70, width: 70 }}
          />
        </View>

        <View style={{ flex: 0.2, left: 10, paddingTop: 30 }}>
          {global_isLoggedIn ? (
            <View
              style={{ justifyContent: 'center', flex: 1, flexDirection: 'row' }}>
              <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 17 }}>
                Hi! {global_LoggedInCustomerName}
              </Text>
            </View>
          ) : (
              <View
                style={{ justifyContent: 'center', flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 0.25 }}>
                  <TouchableOpacity
                    onPress={() => props.navigation.navigate('MyAkpoti')}>
                    <Text
                      style={{ color: 'white', fontWeight: 'bold', fontSize: 17 }}>
                      Sign In |
                  </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 0.75, left: 0 }}>
                  <TouchableOpacity
                    onPress={() => props.navigation.navigate('MyAkpoti')}>
                    <Text
                      style={{ color: 'white', fontWeight: 'bold', fontSize: 17 }}>
                      Join Free
                  </Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
        </View>
      </View>

      <DrawerNavigatorItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: BottomStack,
      navigationOptions: () => ({
        drawerIcon: <Icon name="ios-home" size={20} />,
      }),
    },
    Categories: {
      screen: Categories,
      navigationOptions: {
        drawerIcon: <Icon name="ios-list" />,
      },
    },
    Services: {
      screen: Services,
      navigationOptions: {
        drawerIcon: <Icon name="ios-cog" />,
      },
    },
    // SignIn: {
    //   screen: Signin,
    //   navigationOptions: () => ({
    //     drawerIcon: <Icon name="ios-person" size={20} />,
    //   }),
    // },
    Enquiry: {
      screen: Enquiry,
      navigationOptions: () => ({
        drawerIcon: <Icon type="FontAwesome" name="file-text" size={20} />,
      }),
    },
    'My Items': {
      screen: MyItems,
      navigationOptions: () => ({
        drawerIcon: <Icon name="ios-keypad" size={20} />,
      }),
    },
    //MySellerServices
    'My Services': {
      screen: MySellerServices,
      navigationOptions: () => ({
        drawerIcon: <Icon name="ios-keypad" size={20} />,
      }),
    },
    'My Notifications': {
      screen: MyNotifications,
      navigationOptions: () => ({
        drawerIcon: <Icon name="ios-notifications" size={20} />,
      }),
    },
    'My RFQs': {
      screen: MyRFQs,
      navigationOptions: () => ({
        drawerIcon: (
          <Image
            source={require('./assets/images/rfq.png')}
            style={{ width: 20, height: 20, borderRadius: 100, top: 5 }}
          />
        ),
      }),
    },


  },
  {
    contentComponent: CustomDrawerContentComponent,
  },
);

const StackNavigator = createStackNavigator(
  {
    Home: {
      screen: DrawerNavigator,
    },
    Register: {
      screen: Register,
    },
    SearchItem: {
      screen: SearchItem,
    },
    ProductList: {
      screen: ProductList,
    },
    ProductFilter: {
      screen: ProductFilter,
    },
    ContactSupplier: {
      screen: ContactSupplier,
    },
    Newslist: {
      screen: Newslist,
    },
    NewsDetails: {
      screen: NewsDetails,
    },
    ProductDetails: {
      screen: ProductDetails,
    },
    Test: {
      screen: Test,
    },
    ServiceDetails: {
      screen: ServiceDetails,
    },
    ProviderDetails: {
      screen: ProviderDetails,
    },
    Register: {
      screen: Register,
    },
    Signin: {
      screen: Signin,
    },
    Profile: {
      screen: Profile,
    },
    AddSellerItem: {
      screen: AddSellerItem,
    },
    EditSellerItem: {
      screen: EditSellerItem,
    },
    AddSellerServiceItem: {
      screen: AddSellerServiceItem,
    },
    EditSellerServiceItem: {
      screen: EditSellerServiceItem,
    },
    SendRfq: {
      screen: SendRfq,
    },
    MyMessageDetails: {
      screen: MyMessageDetails,
    },
    EditBuyer: EditBuyer,
    UpgradeSeller: UpgradeSeller,
    UpgradeAgent: UpgradeAgent,
    SellerNewDoc: SellerNewDoc,
    EditSeller: EditSeller,
  },
  {
    defaultNavigationOptions: ({ }) => ({
      header: null,
    }),
  },
);

const SwitchStack = createSwitchNavigator({
  splash: Splash,
  main: StackNavigator,
});

const AppContainer = createAppContainer(SwitchStack);

export default class Akpoti extends React.Component {
  render() {
    return <AppContainer />;
  }
}
